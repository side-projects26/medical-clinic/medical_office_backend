package com.anbara.medicaloffice.dto;

import com.anbara.medicaloffice.entities.Patient;
import com.anbara.medicaloffice.enums.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE;
import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE_TIME;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Jacksonized
public class PatientDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String phone;
    private String address;
    private String profession;
    private MedicalHistoryDto medicalHistoryDto;
    @JsonFormat(pattern = PATTERN_DATE)
    private LocalDate dateOfBirth;
    private Gender gender;
    @JsonFormat(pattern = PATTERN_DATE_TIME)
    private LocalDateTime dateCreation ;

   /* {
        dateCreation = LocalDateTime.now();
    }*/

    private Collection<PatientAppointmentDto> appointmentsDto;
    private Collection<ConsultationDto> consultationsDto;

    public static PatientDto fromEntity(Patient patient) {
        if (patient == null) {
            // TODO throw exception
            return null;
        }
        return PatientDto.builder()
                .id(patient.getId())
                .firstName(patient.getFirstName())
                .lastName(patient.getLastName())
                .phone(patient.getPhone())
                .gender(patient.getGender())
                .address(patient.getAddress())
                .profession(patient.getProfession())
                .medicalHistoryDto(
                        MedicalHistoryDto.fromEntity(patient.getMedicalHistory())
                )
                .dateOfBirth(patient.getDateOfBirth())
                .dateCreation(patient.getDateCreation())
               // .appointmentsDto(AppointmentPatientDto.fromEntities(patient.getAppointments()))
               // .consultationsDto(ConsultationDto.fromEntities(patient.getConsultations()))
                .build();
    }


    public static Patient toEntity(PatientDto patientDto) {
        if (patientDto == null) {
            // TODO throw exception
            return null;
        }
        System.out.println(patientDto.getDateCreation());
        return
                Patient.builder()
                        .id(patientDto.getId())
                        .firstName(patientDto.getFirstName())
                        .lastName(patientDto.getLastName())
                        .phone(patientDto.getPhone())
                        .gender(patientDto.getGender())
                        .address(patientDto.getAddress())
                        .profession(patientDto.getProfession())
                        .medicalHistory(
                                MedicalHistoryDto.toEntity(patientDto.getMedicalHistoryDto())
                        )
                        .dateOfBirth(patientDto.getDateOfBirth())
                        .dateCreation(patientDto.getDateCreation())
                      //  .appointments(AppointmentPatientDto.toEntities(patientDto.getAppointmentsDto()))
                      //  .consultations(ConsultationDto.toEntities(patientDto.getConsultationsDto()))
                        .build();
    }
}


