package com.anbara.medicaloffice.dto;

import com.anbara.medicaloffice.entities.AppRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppRoleDto  {

    private Long id;
    private String roleName;

    public static AppRoleDto fromEntity(AppRole role) {
        if (role == null) {
            // TODO throw exception
            return null;
        }
        return AppRoleDto.builder()
                .id(role.getId())
                .roleName(role.getRoleName())
                .build();
    }

    public static AppRole toEntity(AppRoleDto roleDto) {
        if (roleDto == null) {
            // TODO throw exception
            return null;
        }
        return new AppRole(roleDto.id,roleDto.roleName);
    }



}
