package com.anbara.medicaloffice.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

 @Data @NoArgsConstructor @ToString
@Builder @AllArgsConstructor
public class UserRequest {
    private Long id;
    private String username;
    private String password;
    private String confirmPassword;
    private boolean active;
    private String roleName;
}
