package com.anbara.medicaloffice.dto;

import com.anbara.medicaloffice.entities.AppRole;
import com.anbara.medicaloffice.entities.AppUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data @NoArgsConstructor @AllArgsConstructor
public class UserDto {
    private Long id;
    private String username;
    private boolean active;
    private String roleName;
    @JsonIgnore
    private String password;

    public static List<UserDto> convertAppUsersToUserResponses(List<AppUser> appUserList){
        List<UserDto> userDtoList =new ArrayList<>();
        appUserList.forEach(appUser -> {
            UserDto userDto = toUserDto(appUser);
            userDtoList.add(userDto);
        });
        return userDtoList;
    }

    public static UserDto toUserDto(AppUser appUser){
        UserDto userDto =new UserDto();
        userDto.setId(appUser.getId());
        userDto.setUsername(appUser.getUsername());
        userDto.setActive(appUser.isActive());
        userDto.setPassword(appUser.getPassword());
        List<AppRole> appRoles= (List<AppRole>) appUser.getRoles();
        userDto.setRoleName(appRoles.get(0).getRoleName());
        return userDto;
    }
}
