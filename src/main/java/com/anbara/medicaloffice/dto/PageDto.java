package com.anbara.medicaloffice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageDto<T> {
    private List<T> content;
    private long totalElements;
    // TODO change name of attribut number and size
    private long number; // current page
    private long size; // page size


    public static <T> PageDto<T> fromPage(Page<T> page) {
        if (page == null) {
            // TODO throw exception
            return null;
        }
        return new PageDto<T>(page.getContent(), page.getTotalElements()
                , page.getNumber(), page.getSize());
    }
}
