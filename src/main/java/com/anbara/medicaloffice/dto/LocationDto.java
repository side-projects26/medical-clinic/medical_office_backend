package com.anbara.medicaloffice.dto;

import com.anbara.medicaloffice.entities.Location;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@AllArgsConstructor @NoArgsConstructor
@Jacksonized
public class LocationDto {
    private String  address;
    private double longitude;
    private double latitude ;
    private double zoomLevel;

    public static LocationDto fromEntity(Location location) {
        if (location == null) {
            // TODO throw exception
            return null;
        }
        return LocationDto.builder()
                .address(location.getAddress())
                .latitude(location.getLatitude())
                .zoomLevel(location.getZoomLevel())
                .longitude(location.getLongitude())
                .build();
    }

    public static Location toEntity(LocationDto locationDto) {
        if (locationDto == null) {
            // TODO throw exception
            return null;
        }
        return Location.builder()
                .address(locationDto.getAddress())
                .latitude(locationDto.getLatitude())
                .zoomLevel(locationDto.getZoomLevel())
                .longitude(locationDto.getLongitude())
                .build();

    }
}
