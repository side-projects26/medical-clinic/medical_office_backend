package com.anbara.medicaloffice.dto;

import com.anbara.medicaloffice.entities.Location;
import com.anbara.medicaloffice.entities.MedicalHistory;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Jacksonized
public class MedicalHistoryDto {
    private String allergies;
    private String medications;
    private String oldDiseases;
    private String currentDiseases;

    public static MedicalHistoryDto fromEntity(MedicalHistory medicalHistory) {
        if (medicalHistory == null) {
            // TODO throw exception
            return null;
        }
        return MedicalHistoryDto.builder()
                .allergies(medicalHistory.getAllergies())
                .medications(medicalHistory.getMedications())
                .oldDiseases(medicalHistory.getOldDiseases())
                .currentDiseases(medicalHistory.getCurrentDiseases())
                .build();
    }

    public static MedicalHistory toEntity(MedicalHistoryDto medicalHistoryDto) {
        if (medicalHistoryDto == null) {
            // TODO throw exception
            return null;
        }
        return MedicalHistory.builder()
                .allergies(medicalHistoryDto.getAllergies())
                .medications(medicalHistoryDto.getMedications())
                .oldDiseases(medicalHistoryDto.getOldDiseases())
                .currentDiseases(medicalHistoryDto.getCurrentDiseases())
                .build();

    }
}
