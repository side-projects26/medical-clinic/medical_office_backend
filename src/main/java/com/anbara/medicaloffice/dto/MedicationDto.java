package com.anbara.medicaloffice.dto;

import com.anbara.medicaloffice.entities.Medication;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;

import java.io.Serializable;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Jacksonized
public class MedicationDto implements Serializable {


    private Long id;
    private String name;


    public static MedicationDto fromEntity(Medication medication) {
        if (medication == null) {
            // TODO throw exception
            return null;
        }
        return MedicationDto.builder()
                .id(medication.getId())
                .name(medication.getName())
                .build();
    }

    public static Medication toEntity(MedicationDto medicationDto) {
        if (medicationDto == null) {
            // TODO throw exception
            return null;
        }
        return Medication.builder()
                .id(medicationDto.getId())
                .id(medicationDto.getId())
                .name(medicationDto.getName())
                .build();
    }

}
