package com.anbara.medicaloffice.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
public class StatisticResponse {

    private long appointmentsToday;
    private Long newPatients;
    private BigDecimal malePercentage;
    private BigDecimal femalePercentage;
    private List<DataConsultation> dataConsultation;

    @Data
    @NoArgsConstructor
   public static class DataConsultation {
        private long year;
        private long[] data;
    }

}

