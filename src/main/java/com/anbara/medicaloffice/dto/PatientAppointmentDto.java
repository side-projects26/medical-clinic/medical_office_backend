package com.anbara.medicaloffice.dto;

import com.anbara.medicaloffice.entities.PatientAppointment;
import com.anbara.medicaloffice.enums.AppointmentStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;

import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE;
import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_TIME;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Jacksonized
public class PatientAppointmentDto {

    private Long id;
    @JsonFormat(pattern = PATTERN_DATE)
    private LocalDate date;
    @JsonFormat(pattern = PATTERN_TIME)
    private LocalTime time;
    private AppointmentStatus status;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private PatientDto patientDto;


    public static PatientAppointmentDto fromEntity(PatientAppointment appointment){
        if (appointment == null) {
            // TODO throw exception
            return null;
        }
        return PatientAppointmentDto.builder()
                .id(appointment.getId())
                .date(appointment.getDate())
                .time(appointment.getTime())
                .status(appointment.getStatus())
                .patientDto(PatientDto.fromEntity(appointment.getPatient()))
                .build();
    }

    public static PatientAppointment toEntity(PatientAppointmentDto appointmentDto){
        if (appointmentDto == null) {
            // TODO throw exception
            return null;
        }
        return PatientAppointment.builder()
                .id(appointmentDto.getId())
                .date(appointmentDto.getDate())
                .time(appointmentDto.getTime())
                .status(appointmentDto.getStatus())
                .patient(PatientDto.toEntity(appointmentDto.getPatientDto()))
                .build();
    }
    public static Collection<PatientAppointmentDto> fromEntities(
            Collection<PatientAppointment> input) {
        final Collection<PatientAppointmentDto> output = new ArrayList<>();
        input.forEach(item -> output.add(PatientAppointmentDto.fromEntity(item)));
        return output;
    }

    public static Collection<PatientAppointment> toEntities(
            Collection<PatientAppointmentDto> input) {
        final Collection<PatientAppointment> output = new ArrayList<>();
        input.forEach(item -> output.add(PatientAppointmentDto.toEntity(item)));
        return output;
    }

}
