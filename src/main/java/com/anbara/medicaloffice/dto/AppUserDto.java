package com.anbara.medicaloffice.dto;

import com.anbara.medicaloffice.entities.AppRole;
import com.anbara.medicaloffice.entities.AppUser;
import com.anbara.medicaloffice.entities.Consultation;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@Builder
public class AppUserDto {
    private Long id;
    private String username;
    private String password;
    private boolean active;
    private Collection<AppRoleDto> roles = new ArrayList<>();


    public static AppUserDto fromEntity(AppUser user) {
        if (user == null) {
            // TODO throw exception
            return null;
        }
        return AppUserDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .active(user.isActive())
                //.roles(AppRoleDto.fromEntity(user.getRoles()))
                .build();
    }

    public static AppUser toEntity(AppUserDto userDto) {
        if (userDto == null) {
            // TODO throw exception
            return null;
        }
        final List<AppRole> roles = new ArrayList<>();
        userDto.roles.forEach(roleDto -> roles.add(AppRoleDto.toEntity(roleDto)));
        return
                new AppUser(userDto.getUsername(), userDto.getPassword(),
                        roles, userDto.isActive());
    }


}
