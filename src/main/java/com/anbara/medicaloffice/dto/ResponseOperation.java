package com.anbara.medicaloffice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

 @Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseOperation {
    private boolean success;
    private String message;
}