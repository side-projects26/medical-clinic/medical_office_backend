package com.anbara.medicaloffice.dto;

import com.anbara.medicaloffice.entities.MedicalOfficeInfos;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Jacksonized
public class MedicalOfficeInfosDto {

    private Long id;
    private String medicalOfficeName;
    private String phone;
    private DoctorDto doctorDto;
    private LocationDto locationDto;
    private String logoBase64;

    public static MedicalOfficeInfosDto fromEntity(MedicalOfficeInfos infos) {
        if (infos == null) {
            // TODO throw exception
            return null;
        }
        return MedicalOfficeInfosDto.builder()
                .id(infos.getId())
                .medicalOfficeName(infos.getMedicalOfficeName())
                .phone(infos.getPhone())
                .logoBase64(infos.getLogoBase64())
                .doctorDto(
                        DoctorDto.fromEntity(infos.getDoctor())
                )
                .locationDto(
                        LocationDto.fromEntity(infos.getLocation())
                )
                .build();
    }

    public static MedicalOfficeInfos toEntity(MedicalOfficeInfosDto infosDto) {
        if (infosDto == null) {
            // TODO throw exception
            return null;
        }
        return MedicalOfficeInfos.builder()
                .id(infosDto.getId())

                .medicalOfficeName(infosDto.getMedicalOfficeName())
                .phone(infosDto.getPhone())
                .doctor(DoctorDto.toEntity(infosDto.getDoctorDto()))
                .logoBase64(infosDto.getLogoBase64())
                .location(
                        LocationDto.toEntity(infosDto.getLocationDto())
                )
                .build();
    }
}
