package com.anbara.medicaloffice.dto;

import com.anbara.medicaloffice.entities.VisitorAppointment;
import com.anbara.medicaloffice.enums.AppointmentStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE;
import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_TIME;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Jacksonized
public class VisitorAppointmentDto implements Serializable {

    private Long id;
    private String firstName;
    private String lastName;
    private String phone;
    @JsonFormat(pattern = PATTERN_DATE)
    private LocalDate date;
    @JsonFormat(pattern = PATTERN_TIME)
    private LocalTime time;
    private AppointmentStatus status;

    public static VisitorAppointmentDto fromEntity(VisitorAppointment appointment){
        if (appointment == null) {
            // TODO throw exception
            return null;
        }
        return VisitorAppointmentDto.builder()
                .id(appointment.getId())
                .firstName(appointment.getFirstName())
                .lastName(appointment.getLastName())
                .date(appointment.getDate())
                .time(appointment.getTime())
                .status(appointment.getStatus())
                .phone(appointment.getPhone())
                .build();
    }
    public static VisitorAppointment toEntity(VisitorAppointmentDto appointmentDto){
        if (appointmentDto == null) {
            // TODO throw exception
            return null;
        }
        return VisitorAppointment.builder()
                .id(appointmentDto.getId())
                .firstName(appointmentDto.getFirstName())
                .lastName(appointmentDto.getLastName())
                .date(appointmentDto.getDate())
                .time(appointmentDto.getTime())
                .status(appointmentDto.getStatus())
                .phone(appointmentDto.getPhone())
                .build();
    }


}
