package com.anbara.medicaloffice.dto;

import com.anbara.medicaloffice.entities.Consultation;
import com.anbara.medicaloffice.entities.Payment;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Jacksonized
public class ConsultationDto {
    private Long id;
    @JsonFormat(pattern = PATTERN_DATE)
    private LocalDate date;
    private String reason;

    @JsonIgnore
    private PatientDto patientDto;

    private Payment  payment;

    public static ConsultationDto fromEntity(Consultation consultation) {
        if (consultation == null) {
            // TODO throw exception
            return null;
        }
        return ConsultationDto.builder()
                .id(consultation.getId())
                .date(consultation.getDate())
                .reason(consultation.getReason())
                .payment(consultation.getPayment())
                .patientDto(PatientDto.fromEntity(consultation.getPatient()))
                .build();
    }

    public static Consultation toEntity(ConsultationDto consultationDto) {
        if (consultationDto == null) {
            // TODO throw exception
            return null;
        }
        return Consultation.builder()
                .id(consultationDto.getId())
                .date(consultationDto.getDate())
                .reason(consultationDto.getReason())
                .payment(consultationDto.getPayment())
                .patient(PatientDto.toEntity(consultationDto.getPatientDto()))
                .build();
    }

    public static Collection<ConsultationDto> fromEntities(
            Collection<Consultation> input) {
        final Collection<ConsultationDto> output = new ArrayList<>();
        input.forEach(item -> output.add(ConsultationDto.fromEntity(item)));
        return output;
    }


    public static Collection<Consultation> toEntities(
            Collection<ConsultationDto> input) {
        final Collection<Consultation> output = new ArrayList<>();
        input.forEach(item -> output.add(ConsultationDto.toEntity(item)));
        return output;
    }
}
