package com.anbara.medicaloffice.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResponseAllAppointmentsPendings {

    private Long patient_id;
    private String first_name;
    private String last_name;

    private String date;
    private String time;


}