package com.anbara.medicaloffice.dto;

import com.anbara.medicaloffice.entities.Doctor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Jacksonized
public class DoctorDto {

    private String name;
    private String specialty;

    public static DoctorDto fromEntity(Doctor doctor) {
        if (doctor == null) {
            // TODO throw exception
            return null;
        }
        return DoctorDto.builder()
                .name(doctor.getName())
                .specialty(doctor.getSpecialty())
                .build();
    }

    public static Doctor toEntity(DoctorDto doctorDto) {
        if (doctorDto == null) {
            // TODO throw exception
            return null;
        }
        return Doctor.builder()
                .name(doctorDto.getName())
                .specialty(doctorDto.getSpecialty())
                .build();

    }
}
