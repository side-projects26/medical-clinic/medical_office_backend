package com.anbara.medicaloffice.init;

public interface IMedicalUpdateService {
    void updatePatients();
    void updateAppointmentsPatient();
    void updateConsultations();




}
