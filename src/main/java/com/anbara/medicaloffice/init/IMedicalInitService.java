package com.anbara.medicaloffice.init;

import java.text.ParseException;

public interface IMedicalInitService {
    void initPatients() throws ParseException;

    void initAppointmentsPatient();

    void initConsultations();

    void initUsers();

    void initInfos();

    void initOthers();
    void initMedications();

}
