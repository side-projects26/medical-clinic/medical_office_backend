package com.anbara.medicaloffice.init;

import com.anbara.medicaloffice.dao.AppointmentFirstVisitRepo;
import com.anbara.medicaloffice.dao.ConsultationRepository;
import com.anbara.medicaloffice.dao.PatientRepository;
import com.anbara.medicaloffice.entities.*;
import com.anbara.medicaloffice.utils.Helpers;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;

import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE;

@Service
public class MedicalUpdateServiceImpl implements IMedicalUpdateService {

    private final PatientRepository patientRepository;
    private final AppointmentFirstVisitRepo appointmentFirstVisitRepo;
    private final ConsultationRepository consultationRepository;

    public MedicalUpdateServiceImpl(PatientRepository patientRepository, AppointmentFirstVisitRepo appointmentFirstVisitRepo, ConsultationRepository consultationRepository) {
        this.patientRepository = patientRepository;
        this.appointmentFirstVisitRepo = appointmentFirstVisitRepo;
        this.consultationRepository = consultationRepository;

    }


    @Override
    public void updatePatients() {
         /*  table patientFile
            id  firstName lastName   telephone
             1    amine     manih      074358228
             2    mostafa    akim      002126334444
         */
        /* table patient_file_rdv_patients
          patient_file_id        rdv_patients
            1                 2020-04-13 14:46:36
            1                 2020-04-13 14:46:36

        * */


        Optional<Patient> optional = patientRepository.findById(1L);
        if (optional.isPresent()) {
            Patient pp = optional.get();
            pp.setPhone("06468799");
            patientRepository.save(pp);

        }
    }

    LocalDate now = LocalDate.parse(Helpers.getTodayDate(), DateTimeFormatter.ofPattern(PATTERN_DATE));

    DateTimeFormatter formatters = DateTimeFormatter.ofPattern(PATTERN_DATE);
    String text = now.format(formatters);
    LocalDate parsedDateNow = LocalDate.parse(text, formatters);

    @Override
    public void updateAppointmentsPatient() {


        // create patientFile from AppointmentGeneral
        Optional<VisitorAppointment> optional = appointmentFirstVisitRepo.findById(3L);
        if (optional.isPresent()) {
            VisitorAppointment ag = optional.get();
            ag.setDate(parsedDateNow.plusDays(45));

            appointmentFirstVisitRepo.save(ag);
        }

    }

    @Override
    public void updateConsultations() {
        // get Patient then add consultaion
        Optional<Patient> optional = patientRepository.findById(1L);
        if (optional.isPresent()) {
            Consultation consultation = new Consultation();

            consultation.setPatient(optional.get());
            consultation.setReason("fievre");
            consultationRepository.save(consultation);

        }

    }


    public void updatePrescriptions() {
        // get consulation then add
        Optional<Consultation> optional = consultationRepository.findById(1L);
        if (optional.isPresent()) {
            Consultation consultation = optional.get();

            Prescription prescription = consultation.getPrescription();
            Map<String, String> map = prescription.getMedicamentMap();
            map.put("medica eeee", "form177");
            map.put("medicam wwt", "form8");
            prescription.setMedicamentMap(map);
            consultation.setPrescription(prescription);
            consultationRepository.save(consultation);
        }
    }

    public void updateCertificates() {
// get consulation then add
        Optional<Consultation> optional = consultationRepository.findById(1L);
        if (optional.isPresent()) {
            Consultation consultation = optional.get();
//firstName,  lastName,  commentCertificate, CertificateType certificateType) {
            Certificate certificate = new Certificate(Certificate.CertificateType.Medical
                    , "comment Certificate....");

            consultation.setCertificate(certificate);
            consultationRepository.save(consultation);
//            certificateRepository.save(certificate);

        }
    }



   /* public void updateMedicalExams() {
        // get consulation then add
        Optional<Consultation> optional = consultationRepository.findById(1L);
        if (optional.isPresent()) {
            Consultation consultation = optional.get();
            // firstName,  lastName,  commentCertificate
            MedicalExam exam = new MedicalExam("description of exam ...");
            consultation.setMedicalExams(exam);
            consultationRepository.save(consultation);
        }
    }*/


}
