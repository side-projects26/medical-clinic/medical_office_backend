package com.anbara.medicaloffice.init;

import com.anbara.medicaloffice.dao.*;
import com.anbara.medicaloffice.dto.AppRoleDto;
import com.anbara.medicaloffice.dto.UserRequest;
import com.anbara.medicaloffice.entities.*;
import com.anbara.medicaloffice.enums.AppointmentStatus;
import com.anbara.medicaloffice.enums.Gender;
import com.anbara.medicaloffice.enums.PaymentStatus;
import com.anbara.medicaloffice.services.UserService;
import com.anbara.medicaloffice.utils.Helpers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

import static com.anbara.medicaloffice.utils.AppConstants.*;
import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE;

@Service
public class MedicalInitServiceImpl implements IMedicalInitService {

    private final PatientRepository patientRepository;
    private final AppointmentFirstVisitRepo appointmentFirstVisitRepo;
    private final ConsultationRepository consultationRepository;

    private final AppointmentPatientRepo appointmentPatientRepo;

    private final UserService userService;
    private final InfosMedOfficeRepo infosMedOfficeRepo;
    private final MedicationRepo medicationRepo;

    LocalDate today = LocalDate.parse(Helpers.getTodayDate(), DateTimeFormatter.ofPattern(PATTERN_DATE));


    public MedicalInitServiceImpl(PatientRepository patientRepository,
                                  AppointmentFirstVisitRepo appointmentFirstVisitRepo, ConsultationRepository consultationRepository,

                                  UserService userService,
                                  InfosMedOfficeRepo infosMedOfficeRepo,
                                  AppointmentPatientRepo appointmentPatientRepo,
                                  MedicationRepo medicationRepo) {
        this.patientRepository = patientRepository;
        this.appointmentFirstVisitRepo = appointmentFirstVisitRepo;
        this.consultationRepository = consultationRepository;

        this.appointmentPatientRepo = appointmentPatientRepo;
        this.userService = userService;
        this.infosMedOfficeRepo = infosMedOfficeRepo;
        this.medicationRepo = medicationRepo;

    }


//    LocalDate today =LocalDate.parse(Helpers.getTodayDate(), DateTimeFormatter.ofPattern(PATTERN_DATE));
//    ;
//    DateTimeFormatter formatters = DateTimeFormatter.ofPattern(PATTERN_DATE);
//    String text = now.format(formatters);
//     LocalDate today = LocalDate.parse(text, formatters);


    @Override
    public void initPatients() {
        Stream.of(
                Patient.builder()
                        .firstName("sina").lastName("Jake")
                        .phone("074358228")
                        .dateOfBirth(today.minusYears(11))
                        .gender(Gender.Male)
                        .dateCreation(LocalDateTime.now())
                        .build(),
                Patient.builder()
                        .firstName("Jack").lastName("Connor")
                        .phone("0100000")
                        .dateOfBirth(today.minusYears(8))
                        .gender(Gender.Female)
                        .dateCreation(LocalDateTime.now().plusDays(9))
                        .build()
                ,
                Patient.builder()
                        .firstName("Olivia").lastName("poter")
                        .phone("0642345")
                        .dateOfBirth(today.minusYears(31))
                        .gender(Gender.Female)
                        .dateCreation(LocalDateTime.now().plusDays(45))
                        .build()
                ,
                Patient.builder()
                        .firstName("Jacob").lastName("Thomas")
                        .phone("05966679")
                        .dateOfBirth(today.minusYears(6))
                        .gender(Gender.Male)
                        .dateCreation(LocalDateTime.now().plusDays(90))
                        .build(),
                Patient.builder()
                        .firstName("farid").lastName("farid name")
                        .phone("0590007944")
                        .dateOfBirth(today.minusYears(5))
                        .gender(Gender.Male)
                        .dateCreation(LocalDateTime.now().plusDays(3))
                        .build(),
                Patient.builder()
                        .firstName("Emily").lastName("Elizabeth")
                        .phone("014567879")
                        .dateOfBirth(today.minusYears(4))
                        .gender(Gender.Female)
                        .dateCreation(LocalDateTime.now().plusDays(10))
                        .build(),
                Patient.builder()
                        .firstName("Isabella").lastName("alex")
                        .phone("01234")
                        .dateOfBirth(today.minusYears(3))
                        .gender(Gender.Female)
                        .dateCreation(LocalDateTime.now().plusDays(7))
                        .build(),
                Patient.builder()
                        .firstName("john").lastName("alex")
                        .phone("01234")
                        .dateOfBirth(today.minusYears(2))
                        .gender(Gender.Male)
                        .dateCreation(LocalDateTime.now().plusDays(55))
                        .build()

        )
                .forEach(patientRepository::save);


    }

    @Override
    public void initAppointmentsPatient() {

        Stream.of(
                VisitorAppointment.builder()
                        .firstName("Jones").lastName("Johnson").phone("002116334444")
                        .date(today)
                        .time(LocalTime.now())
                        .status(AppointmentStatus.Done)
                        .build(),
                VisitorAppointment.builder()
                        .firstName("Williams").lastName("Sullivan").phone("23455")
                        .date(today.plusDays(2))
                        .time(LocalTime.now().plusHours(5))
                        .status(AppointmentStatus.Pending)
                        .build(),
                VisitorAppointment.builder()
                        .firstName("Brown").lastName("Walsh").phone("755554")
                        .date(today.plusDays(33))
                        .time(LocalTime.now().plusHours(7))
                        .status(AppointmentStatus.Pending)
                        .build(),
                VisitorAppointment.builder()
                        .firstName("Taylor").lastName("Smith").phone("99987")
                        .date(today.plusDays(2))
                        .time(LocalTime.now().plusHours(6))
                        .status(AppointmentStatus.Pending)
                        .build(),
                VisitorAppointment.builder()
                        .firstName("Evan").lastName("Garcia").phone("438776")
                        .date(today.plusDays(3))
                        .time(LocalTime.now().plusHours(12))
                        .status(AppointmentStatus.Pending)
                        .build(),
                VisitorAppointment.builder()
                        .firstName("Roberts").lastName("Wilson").phone("65443")
                        .date(today.plusDays(11))
                        .time(LocalTime.now().plusHours(9))
                        .status(AppointmentStatus.Pending)
                        .build()

        )
                .forEach(appointmentFirstVisitRepo::save);

        // create patientFile from AppointmentGeneral
        Optional<VisitorAppointment> optional = appointmentFirstVisitRepo.findById(1L);
        if (optional.isPresent()) {
            VisitorAppointment ag = optional.get();
            Patient patient = new Patient();
            patient.setFirstName(ag.getFirstName());
            patient.setLastName(ag.getLastName());
            patient.setPhone(ag.getPhone());

            appointmentFirstVisitRepo.save(ag);
            appointmentFirstVisitRepo.deleteById(1L);
        }

    }


    @Override
    public void initConsultations() {
        // get Patient then add consultaion
        final String[] maladies = {"fever", "diabetes A...", "high blood pressure ... ", "diabetes B...", " drop in blood pressure .."};

        final Random random = new Random();
        patientRepository.findAll().forEach(p -> {
            for (int i = 0; i < 4; i++) {
                Consultation consultation = new Consultation();
                consultation.setDate(LocalDate.of(today.getYear(), 1, 1).plusDays(random.nextInt(100)));

                consultation.setPatient(p);
                consultation.setReason(maladies[i]);

                Payment payment = Payment.builder()
                        .total(new BigDecimal(100))
                        .amountPaid(new BigDecimal(50))
                        .status(PaymentStatus.Incomplete)
                        .build();
                consultation.setPayment(payment);
                consultationRepository.save(consultation);
            }

        });


    }


    @Override
    @Transactional
    //i have wrapper this method in transaction,if there will exception RuntimeException(not checked exception)
    // between {} then the transaction rollback
    // you can specify type of exception to rollback by using attribute rollbackFor
    public void initUsers() {
        long nbUsers = userService.numberCurrentUser();
        if (0 < nbUsers) return;

        userService.saveRole(new AppRoleDto(null, ROLE_ADMIN));
        userService.saveRole(new AppRoleDto(null, ROLE_DOCTOR));
        userService.saveRole(new AppRoleDto(null, ROLE_SECRETARY));

        userService.saveUser(UserRequest.builder()
                .username("admin")
                .password("12345678")
                .confirmPassword("12345678")
                .active(true)
                .roleName(ROLE_ADMIN)
                .build()
        );
        userService.saveUser(UserRequest.builder()
                .username("doctor")
                .password("12345678")
                .confirmPassword("12345678")
                .active(true)
                .roleName(ROLE_DOCTOR)
                .build()
        );
        userService.saveUser(UserRequest.builder()
                .username("secretary")
                .password("12345678")
                .confirmPassword("12345678")
                .active(true)
                .roleName(ROLE_SECRETARY)
                .build()
        );


        userService.addRoleToUser("admin", ROLE_ADMIN);
        userService.addRoleToUser("doctor", ROLE_DOCTOR);
        userService.addRoleToUser("secretary", ROLE_SECRETARY);

    }

    @Override
    public void initInfos() {


        final String logo = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAP0AAAEsCAYAAAAIILXCAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAGo/SURBVHhe7Z0HWJVXtvdn7jdz5947d8r9vjszmZk0pRd774klMZYYa2I0JsaSxFhiNLEkGRKld6SrCIqICqIggiC9F+m99957EVzfXvtsEDjvQcpBUfbvedZjoue85y37/+619157rV9xXm4Uly37va2t/TwfH//NYWFR5tHRMd4xsfF+cQkJ4bFx8aExMfF+4RFRbiFhYadc3NxWqalpTWJf5XA4LxC/d3G5tzoq6qFdUXFJZk1NbTcMgfb2DqipratKSkp+EBQUdPDAgeN/Y8fjcDjjlH974Ou7u6CgKKKxsZFJeeSUlJZlhIRFHF+6YcOf2PE5HM544fr1W/Ny8/K9Ojo6mWSlR0FRccTt27cXs5/icDjPmV8TV/y76praBqbRMaG2tq4pMDhsP/tNDofznPj3lLR0687OR0yakmlva4XcnFK4czsSrl8LgRuOIfRP55th8DA6C1qamtknJdPY2NQVExOnxn6bw+E8S+bN2/wfKSnpdkyPEsnMKARz0wfw0YfnYelSI1BSNQBFZX1QVCHG/pwz1xDeX28GWpp3ISoiHR4/7mLfFqehsbHbNyB4NzsNDofzrIiKeniW6VCQ2poG0NNxh8VLjEFGTgfkFFHwJqA6De1cP1OeYgLyyvg5XZgxQxeOfXsd0tMK2ZHEqampbXJ2vrOEnQqHwxlrPLy8PmhubpY4Y/cwKgu2bj4Pk4mIFVSMYcr0czB1xtBMibwAJsvpwRLysnBxjmBHFCcvLz/+yJEjfFafwxlrdu3a9afcvPwMpj0x7rnHwqKFRiCjYDAssQ80eSUjUFbRhSt2QezI4gSFhJxmp8XhcMYKf//A40xzYoQEp8FcMj6XVTQUFPJwTVHFBJSUdcHRIZT9Qn+KS0uLz+rrv8pOjcPhSJvT35x+pbCwKJtprh+V5TWwbq05yCpIR/A9pkDG+jNn6kFUeDr7pf74+vofZ6fH4XCkjbun58aOjg4mt/78onYb3pTRFRTuaE1GXh927rCB+jrxKL/s3NzILVv2/ZGdIofDkSYJCYmCS3RJCbkwZ64BnZ0XEu1oTXX6OZCV04arV8TH93X19Y8uXLi6kJ0ih8ORJmnp6bFMa/1QP+MKMnJ6goKVlskqGsHHH12Azs7+nsbjx48hNDz8EDtFDocjLfT09P5CxvMVTGu91FTXw9o1ZnTsLSRWaZnyVBOYMdMAgoOS2S8/ISEh2YWdJofDkRbm5tZLm5qa2pnOevF5EA9TpxmAChGlkFilZbj8R138y4Hsl5+QkZEZT07x16Iz5XA4UsHC4vzbLS0tYqK/djUY5BV16LhbSKzSNBxCnDrhDF1d/WP9iQdSpaury/feczjSxNzc6q3m5mYx0Vtb+MBkGa1RBeIM1TCU96PtNtDS3MR+XQSeF54fO1UOhyMNJPX0lmYPnpnoMT7/gw+soK62nv26CBT9uXMWb7NT5XA40sDS8sIyoTH9sxS9nJIRbN58Hhrq+2/dJ6LvMDOzWs5OlcPhSAN1dcNXCouKK5nOerG96A+y8trPRPQY7ffpZ5ehva2F/bqIhobGFnNz6wXsVDkcjrRIS88UW6e/5RQGisp6dIuskFClZfhSkZHVBhOj+3Rtvi+5efllBw+q/YGdJofDkRaJicnOTGe9xMVkw7z5RnRLrJBYpWUoenlFXfqSGUhmZlYCOb1/E50lh8N5Gv92+rT6K4aG5xYSW3zmjI4s/p3on/oTEhJ2sHtAL/u4uwt2fWIjtZ11kkxRxRiWLTtHU24NJCUlzYudohB4LX2Nr+dzJhZaBgaq/v5BO6JjYq1TUlL90jMy00pLy0oaGxtbyNi4taKysio9Mys7LS09LCYmztY/KGivmoaGEn73zBltxdq6uv4DagLGxNO1+jF08SfJ6sP3x2+wX+xPVNRDLTw/PM/7Pj4bomNiTBOTUjzSyXAkKysnm1wOtazsnGxyvYnJyWkPYmJiLwUGB++/ePHy/EmTJvGhAefl4vTp0694+fh8npae4VVWXl47cEz8NMiLoDIpKeXmpUuXt5Ne9SH7615qa+phw/uWY9bb40aeWbP1ISoik/3iE3DXn69/0N3U1LTbFeREh5uCu6KioqOopDQ2ODj0Z01NAxl2yzicF489e/b8zw0npx2kWzufl5dfzNr4qGhobOwqKCgUTHXtdicaVFR1ab47IeGO1HAsj2m3tDTusl/qz3BfYINRUlJWQbwbs8sODkvZbeRwxjffnDw5ycPDa11sbPwVIs7c1rY21pzHHsxg+4vaHZCRlW5I7iQ5ffhw63moId7EswKHOUkpKUFBoaGHdHQMZ7Hby+E8f1QXLfqz3bVrU3x9/b9JTEx2It5tYUtLK2u6z56mxmb4Yr89TaYx2vE97eHJOP79960gM10qjsqIKCsrb0zPyLjv7x/48zUnp1kbd+78C7v9HM4z4be2trYLvbwefJWQkGiek5ObSBrliOpItZKXQ3RUBnh5xICLczg43wyHW8w878VASnL+oPnnJYFbbY8euU4Ddka63RaHCPji2Lb1AmRnDl/wj7sfQWxsFri7RfdeExbVuHMrArzvx0JcXDZddRguZPzflZ2Tm5Wann7V28fvsM3lyyu+Onnyz+zZcDijQ1V10Z/1TUxmenh4vBcZHa0WH594JysrOxWjzwYumw2HnOxiMDbyhM2bLGHWHCNQnaIPiko6oNDHVMjfLVpkAp9+YgMXz/tDYX4Z+/bQ6GjvAFubAFi02IimusJc90LiHmhTiMkqGMC06Xrw4+lbUF1Vx444NDLSC0FX9x7s2H4e5swzAmVVvX7XpahMPJCpBuTfjOGTHRfA7lIgFBeWs28Pn6ampo58Mo5KTk4NeBgbZ+EfGPKpg8ONpT/+eEb5jTfe4Om5OUL88Q/Hjx//m9mFCzKu9+6tJD348bCIKOOEhCSXktLSpIqKyobmZrEVsmHT0twMAf5JREjOsOwtE9IL69I4duxRVQSKT6gQU1I1prPxsvI6sHz5OZqRtlV8v82gJCflwcnvrsP8BeR3nrLfnobxTjeEL/ZdhuDAFPLtIVW3pjTUN8M5Yy+aehtz5suR88ZrG3hd9NrIeeC/4bVhsM+qd8xAT9cdUpLyyctq9HMgVVXVXZWVVZXk+SVico/IyGhTP7+A0+7unjtv3LitamRk+cbhw4f/+oc/vIl5/X4rageclwEMAPnNu++++++GhmbKFhbWK5xcXLd4evrsCQoJ+S4iIkorMzPbJTc3N7qgsDCvqKi4pbll9OLuB3HNU1Py4dIFX9i+/SLpPfVJr6tHg1ywRxUSnpChGOWUjEFOXlMwgcXT6Oxohp2f2JIe3Ejw+D2mPPUczJ6tA/m54sE3T8PYwANef1OdFtYQOrYkw2tTUEZvRA/mzzeAPZ9fpvX1RAFAQ3/pDAVcZSgtK4eioqLagoIiMjLLjyHP/0FSUop1aHikemBw6DEvL799t+7c/fDKFYd1uGsQ2w62IUVFRXw59AQXvfjg5Mi9e16rb91yXSnJ7txxX+Hj4785MDBkm19g4Db808vL5wP8e6HPS9PwN5ydb7/j6em7pef3Hzzw2xYWFnYwOvqh1cOHsecexsZaJSamOCUlp7qkpKX7p6dnhWN8S05uXgF589ehG9ja2gqPHg1/HDkcGhubICEuC+yvBJMGfAkWL8ZeHUtKGZIeTrjhD8XwJfGmjA71FEjrZb82NFpbm2HHMESfkzU80dfV1sGmzRdAZhSpt1H82PtjgQ45BeLZvH0ODnxlDy7OkcQDyIWOjuF5OCOlq6sb2traoKm5uaupqbkd205OTl5BdnZOdkZGViRaamqaT2IyaWtJybcTkpKux8TEmcfExJpGRT20DA+P/AHbJrZRqpOQ8EF14uLiscrqot0SHUPTGYamz8asrW0W/eqGk8uOmpo6LEU8qKFgsLlhm8M/28m4UehzY2F1dfWAlVl7fr+rW7q9wEhBt728tBo878WCxtk78PF2G5g9R582XBQZuujUbRZo6MM1rDizYYMV1JFnNRww4cVwRJ+dWcK+OTRwTI6FMRWllIkXlxvRG8IdfTjXMW++IXy+2w4M9NwhKCAFKsqrpTIMGCu6ux+LNIKNlTCYTrBdkyFkJxmO1OELZqytpqa20dc/yPNXS3bt+l1+fuHw/caJBnHXO9rb6Yy7i3MYGBt6wo4dl2DFcmNQmaJHJ8zkFA1Ij8XGxwINejSGosSouJjoLHZCQ2OsRX/bJZJO0I3FNeMLAKMBcXJRVkGfDpFWrTSBfXvt4aK1H7i7RUFSYh4ZwmCG3rH14l4GOjs74e5dz2XUxbe9enVhTW3ds/GhXgAaGhohPS0fHhKB+z5IhF/O3KHu5pbNVjBnLunVSCPHSrCYFhp7OJygEmq00jZs+KdOOLGzHBpjKfq2tnbYtXPsN/ug4UsFJzix1Ba+BGTktEFZVR8WLjSGD7daweGDV0FXxwMC/ZMhPjablujGZVHOE+LiEi5SwfdAxsWW7N9eWrq7O6GpqZGme8IyUMVFlZCcmA8ODsFgeu4+aKm7wonvrsPWbRdhIRmPz5ipR1xMLdKLk54GZ5vZjDuKfCx6tqcZ/vbs2QYQEoSz60NjOKKfNUsHsjKGLnr7y0GghHv5n8O9wPuPz0FpijF9LjgckCXelrKKNsyZYwBLlp6DnR9fgtMnboC2phtYmHuBs1M4ZJGXWmlxFVRW1EB9XQN5MTSTK3n5vYTyiqoSAwOz/nsbtLSM3igtK69in+mHKL6aDVLGHByv40MQt+6uR9D1qJNmY8X/xsILuCaNmz5wxjc4OAnCQlPAyzOGun5O10NptlibC35gaeYNhw5dhw8/uggbN1rBu++YwpLFBkRERNjKBsQ116c9Kc4k48QbupUoBNXppoKN7nmZPDm3tWssID93aOvbQxU97sdfsEAf8nKGFhMQFZFOPm9Mx99Cx3tehi8CfDmiB4bDLXye+Fzxv5VUDGDeXD146y0jWP2uKWzaZE3vzbFvb4KV+QOwtfGHa/bBcPtWBNy7Gw0PvGJpewoOToay0mrazrC9PSJuMm2LrD1iMJJQexXZ+Jh/wsKnTOr9waULIWm3t3eSN6UnnDrtDD//fAfUxsh+/uUOfHf8Bhw66ACHD10Ts8932xJ38iINWvl0lw1s3WIFG943h40bzOHtt8/B9JlGMGO2EUzBPPAY9ELccNxyiq7gJBlNNu42Ij2DMV1ewoaOs+rYa/SYUEMab4YNeNvWi8RLyWNPSDJDFT2K5K23jKG4UKyehhiB/kn0fmMPK3Ss8WY9zxY9EnyR4wsdn7/IQxAFMGGeQYxixPaipEK8l6n6MHW6AW1P02cZwTurTGk7w/b20TZr2v6wHWJ73LfHDo4cFm+vh8iQ48T3N8nw0FWwvUvDUI8nTjrB9WvB7OmIk52TFzdjxrr/ZDLvz84vj/2loLAon322H2bnvOGfr2nAJDl8e46hoZsmwbAHxqAWOSX8U+RuyyuLTFHVuDfwA//sDXphD/xFEfRQDXuvxUuMaf34mkEi56Qp+tKSSjDU9yTekeg5CB3nRbW+LwZsN9h++rYn9Gh62hq2u542SNsjaZdC7RUNlyEnC7VzKdmbssQma4Dr7Sj2lPpT39Dw6LabxzomcWECgkK/Z5/vB+7ZXr/e8qV72C+yYW+F3sz75LngS9n9bhQkxOfQCayemISRih73xTc0NENUVAbccYkAfd17sHIl+U1FvTFPzcVt6IbC37vnMh12CJGYlHKDSVsyJ0+e/L+FhUVJ7Dv9sLMNAHmFsVme4TYyw54Jex8MAlImQxoMuV3xlg543xfl2hiu6EtLRNM6mhousPwtQ5gzlwyDVPTpaoWCigl/9uPIcGiqOkUXfLzj6TMbSGlpWb21tbUCk/bgeD3w2yeUHKGutoH09ha8tx+HRl1T4pIqqp6DV18/C3ecRe7eSEV/5Oh1ePUNUVKO57VawW1ww7mIA1/aw6MBlYR7CA2NVGOSfjo7dhz7r+ycHMGyyA72wcTF06E9jNCJcHu+NmW6KcjKaYLb7Wj6vEYq+uPf3aSuo9BnuT1/o0Os6boQFZlBn9dA8gsK044ePfo/TNJD486du+tbW1tFA8M+NDe1wCc7bejkhNDJcHu+xkU/MWySrC789OMttpzen9bWtm5vb++PmJSHxa+TklJvs+P0Iz4uB+bONRj2ripuY29c9C+/4QrX6tVmUJgvHKuRnJJ+l+h3ZDsCLS0tp1ZVV9eyY/UDd5Khm4+bSoROjNvzMS76l9swMhQjDsND0+hzGkhdXX2jnYPDHCbhkREQFHQId7iJ0w2m5x7QDKy4TjmcPeLcxs646F9Ow4nUyXIGdK+Bu6tYFvRewiOjDZl0R8Wvk1JSbrFjDqAbbjlF0GglHGNgTjY+0/t8jYv+5THUEQYGYRIVXDLdvMkawkKEe3iksKg469SpU//LdDs6zMzM/paXX5DAji0Gji10de7Cu++YEeHriXagkXEHvgSwMWHYY1/Dv8Na57hjSuhiuY3cuOjHt6GQURe0/ffRxEBtYCQfho9PnY4Zic3hvLXvoDkL6+sbOr29fTcyyUoHR0cXxZKSUsmvGUJFeQ0tcGhs4ElTNK1ZgzHxWAPNGN5itmyZEaxYbgIbNljC6vfMYNoMEz40kKJx0Y9fQ8HPnG0C69abEyFbwvK3TageBmpj7TpL+Gz3ZbA096aBN02NTfSZSKK1tQ3CwiK+Y1KVLvb2N5SKi0sS2W8NCm5ZrKrA7asVUFRYQcM70fC/S4sroaa6FqIjUmDBAiMe1ilFG6nosXdZs8aiN46fi176hj38mrWWkJWRQ+5zLZQUVQpqo7qyBtrbh5YLANPAhYdHfs8kOjZYWlq+kZaReY/95qiorKiGd1Zb0JshdJO4Dd9GI3rsfepYJRsueumbjJweHDt6nd5faVBeUVntFxi8n0lzzPlNRETUkdKyijL2+yMC31L79tmTBqkveJO4Dd9GI3rcUMVFPzaGQ1gZWhPQld7f0dDR2QnpGVleNjZX5jI9PjsMDAxkYmIS9CoqKgsFAoOGhPqZ2zBZVpeP66VkXPTj03A8jwk9zhl70vs7EqqqqrvS0tID7t9/sJvI799FKnxOnDx5cpKfn9++5NS02/n5BUUtLa1DThdyxS4Q5PjOPakZF/34NEzeMXWaLoSFpNL7OxTa29uhurqmISUlNTYmLk7T1t5xOZEb1nQYX+zbt++Pjo5O7/v5BR0NDg7XIeP/6zk5uSG5uXlRjY1NYrMTdbWN8P56Cxp4IHSzuA3PuOjHn2GH9sZkXfj6K3voFNj33tzS8qigoDCdaCQiMyvHPTo6xigwMPjEDefb2y0sLFSIrF7ISjyYqucPUdGx1uw6+xEcmApLlhjBJFk9GozAXf2RGxf9+DEUO65MYTHQjR9YQU628BRYYlLqzY0baTXe31O1vEyYWNooVVZWN7Br7UdSYj7s33cFpkzVI2N8HRqFRHPYsQi/XsObOcCEbvhEtYGib25qhA8/tuGil5KJtb++bZMYJrbAmAfMxYjteMZMffie3Mviwkp6XwdSV1ffdumS4wImkZeTiKgoTXa9YnR1dYK/bwLoaLvDB+TNuHSJAUyfrkdeAKJstTRjLWY37WsKWJVV+AFNRBsoeiwJdeiQI7lXg6+QcNE/3TBJCba3/u0PsyeL2qYCaaezZunB228ZwrZtF8HE6D5EhGE8m+QprvDISA0mjZeX9evX/3d6RsZ9ds0Swdz0GLwQHJQCly75g9k5LzA0uAdnf779xH65DT//y4XWjMNhgdCDmmg2UPSImYkXTJLRGtQr4qIf3JRJ+1q5whTOqLnQdoft7wwx9TN3wNjQA8xNvcDePggS4/NogE1jQyO9j4ORnp5xf9WqVf/FpPFyo6Fh/Nfc3Hx3du2jZusWyxcmHfNYm6DoyQuTi350JkPc9f37LtN7Iw1y8vL9dXR0/s4kMTHYtWvXn2Jj403JmGZ01QAed8EX+y8TN2vsSyu9CCYkelPe04/acI1dU+MuuTOjK/rS3NwCiYnJF/bs2TO8VFYvE3fu3N2cnp4ZgGWeR8plW1zr16ETKUIPbCLZaES/bh0XvZDRgihTh7fGPhCsXpuTkxvj7um9kzX9Cc9vXV3vrU1OTXUsKCgswV1DwwFr0mFOdmy4Qg9tItmIRa9kDBs3WkNDvWhxhYv+iWE+yI8+ukh76eGAte5LSkpL4+MTbt27f3/bn+Tl/5u1d05fMAmAk4vrlqCgkNMZGZnXs3Nyo/MKCnMKC4sqCgqKyqqqqwXvvJWFD91/PNGz9I5U9Likh+v57W2i28tFLzKaAnyKLnjfj6P3ZSBYj76wqLibtM+avPzCbGyv2G5DQsJOOTvf3qampvZP1rQ5Q+ePfzh8+PBf9fXNXz179uyrLq6uWxsaG8Wy9GJk36e7LsEkWf1BG/doDOMGsBEI/dt4sYkmeiw3NVbPBDsQXGv/6QcXckfEx/KNTU2dDx74HbJ1cFAwMzN7fc+ew3/F9soaLkeapKalCc7852SXwNo15jSyT5rCx7kCzAOIiQ5WrBBVsxX63HiwiSR6fAmvW2cOCxcZS70AC8Z+YOjs7s/soL5OePktPj7RmjVJzljj6Oi0oL6+XnDmLzO9CD768CINfcS1VaEHOhzDnmSynA58/JENxERnwUVr33G9QWiiiB4jNZVUdCHANx7CQtNpSujJ5GUvjV4f07hhmbADX12l2aCEyMsrSDMxMZFOPjrO0AiLiPiF3X8xqipraYlgDOnFhoviH07Pj4JGsWPSgwULDMHE+D4t6YVgnfJ33jElPf/4jAuYCKKfNlOUGXbnxxdpqigEM8+cIOc8YybmaNQHTM0+nBczfha/M5k889mz9cHY4D6diBOiqqqm3s3NYxFripxnxebNm/8jNTVdYkaCx92PwOdBAq3oOW26viieX1GUjJBu6pmONdqeGPYQCsomtKFjnfJly0zgXz86Q2pKATviE9zuRIOSMhaMHH/j+4kgeiyyMnuOAUSG9y/p9PhxF0SEpcO3Rx1pGrbeBK2k58YXf9/njc8f/4726mToNllWm77gjx65Bg+jMtkRxcGl5aDQ0M9YM+Q8a1hkny97HoJgPH9wYBIY6HvCli3WsOJtI5g5U58I3IC4hwbkoRNPQNUAFszXh7VrzOAr4tI52AdBbnYJO4I43V2PQFPDnYhLZ9yF/77soqcZZJV1wd4umJ6nJJIT8+ACGYrt3m0Hq981gXnzRM8aDZ87Pn98caxaYQy7yHVbW/pAUmIu+7YwjU1NjyIjow+y5sd5Xqipqf0lOzvXiz2XQcGsopUVtWQMmAY3boTCrVvh4OwcBnduR0JWRgmtwd/1SLgC6EA6OzvhzM9utDcZT1V+XmbRyysbgbKKDlhb+GG3Ts/zabS3t0FNdR2kJBeQZ43PO5w+9xs3QiA+LheqyVCwrfXpSSibmppaIyKiv2bNjvO8oSG98YnWzS2iBvus6OjoACPD+3QnINYXGw+Tey+j6HEmfTIZpy9caATONyNw7EbP8VlRVlZe6OZ2fxNrbpzxhL+//+aiouIk9qyeGQF+yTSTrJy8Lt348zzFLy3RHzv+/EWP6+Q4JpdXwqUzW0hKyKPn9qxoaWmFxKTk22bn7ZRZE+OMR3btOvKn0PDIH0pLy1K7usRieEZESUlZRVZ2zqCB1zg0sLngB2vXWtAlPWysONH3rF4AKGhcxsJw2n/8Uw1uEze2h5GI/siRq/CP17TppBktVSbwnbGwnll0GXkidkUd2LrlPFx3DIaO9nZ6XkJgVuWQ0HC7tPT0wIbGp29pfRq4IpCTkxvt4eG1gzSp8ZeTjiPMzp07//LA1/eTmJg49xLCcOKmsRFVV1eXxcbGPwgJizyopqam8M03p1/Jy88PYB+RSEV5NdjZBsCnu2xg1mwD0utq09lhumpAZ41FDVuowQ/Veo6Bs9A9WVpwbkFZVRdWrzaHUyccSaMtZWc0PNG3tojCH3De48hBe1i+3BQUlHSICHVF2YzI74lmv6V3HTgZii8X9CzkFLRpsM3+vXZwyzkCyzXR85FEVXV1c1BQyJfssf/u9t277+FQLys3N6OxsbGTfeyptLW1Q0VlZXlMbLzLvXv3Nv35z39++VJXTSS+++67v9+4cWuzv3/gkZSUVKv09Ax/fJNnZ+dGouF/49/hv/n7B33jfNtt29mzZ19nX+/F1NT073l5Txc+gqsGmDXlvJUvfEZc0xXLjWHqNFGGFRQobtXEDUIYD4BCUp6Ccd7ChsJGUWAvjuvIVIDkOKpTDWDlCmPYvv0iGBl4gpdnDI0jGMhQRb99xyWaU68v+Xll4O4WBWfPuMHWrdbw1ltGoDIFxYnr4bqi6yDnhdeB5yl0/j325DrwJfXkOmbOMoA175nCl19ehWv2wZCchG780yfqysoq0m+5uq5kj6cfM2bM+E97R8dV3j7+X6ekpFlnZGQGZZFnnZubn56XV1ick5MXl5mVHUH+/nZE1MN/3bp152M1Na032Nc5LyG/+9Of5P/7V/g2J0b/m/yd6J8G59ChU/+bkJRkMxzPob2tFaoq62jcwHlrH9DWdIXDB6/REkcrVpjCkiXGMHeuPsyb1996/m7ZWyawcpUZrFlnBce+vQF62nfhAjnO/fuxUFNVT3rnwc9lKKJHAf9w+hZ0d0seEjU3NUFpcRXcdYsGKwtv0NFygwNfOdDzwutYusxk0OtYutSYXsf7G6zh22+ug76O6DrQq8Dgp6GunCAZmZm+uromk9ljGQr/8atf/fn3W7bs+Z9TpzT+OmfOaoyRnxhZazjSwdPbe2d+flEkGQoMXf396IbyskooyCuF7MxiSE3OF7S0lHwaM1BYUAYVFaLiksPlaaLHv8d/tzDzZt8YOhgQU1FOriO/FHKyiyFF4BrodRDLySqGInIdVVXCoa1DoaOjozUxMcl0/e7dfJsq59lz8uRPswODQtO6u5/tEtJwGaro8XPjGYyNCA4OcyK3nk+ucZ4tFy9enh4bG29ZWlpW3NbWNuTJoufFyyJ6fLm2trbVZmZmh/j4Be7fsW/fK+yRcDhjg9l522lE7Fa1tXWies4vCC+L6PuCySuKioqzQkLCT2/ZsuWP7BFxONJhz549f42KeqhVVl4++PrRoOBsNA4DukiDfUQ3BD3NhjKDPRSetei7ujrgUSexR4MY+XecuHvc3QmPH6OzhNc7MkpLS2P9/YM+ZI+LwxkdLi6uawsKC2NZ+xoSuLU3OTGXLtndvxcL2tr34MT3TnD0G0c48KU9fLbrEuz+VLJ9xv789uh1cLsTRev4j4ZnJfrYmFTQVHeCjz40gc2bTWDLIIb/vm3rOdj9mQXs3WMJB7++AAZ6t8HrfjQEB8ZDfGwKtLU2syM/HVxnT0lJd7py5cqb7NFxOMPj1Vdf/V1MTJxmQ0PjU9eRMJkkhohamnvDye9vwIYPrGHeAiOYNk0HFBW1QEZen5gBjdLDQB3MwiNkMorGMJkY5lin/0++o6ikDevft6S7wKqrRjaqGFvRd4Gfz0P4+quLMGOmhmgLsxJmsjEGWWIySib0T1ybFzIMLEKTJTZZVgsmy6iBsspZmDNHi7wUzOC7Y7Zgc9ET8nKLoLPj6RtjCouK8+96eGxmj5HDGRrfqan9PTUtAxOjDUpaagFYWTwgPZc1aaQYeKMHk2mwiqGoDt9UjB83FRTaQJs2wwSmzTaBKXPPgdJcc5CdYQUK0yxAZZopTQWFob2YGuqWUzhxi4c3dzhWos9Mz4Vvv7EF1SkaMElWlwbgqEw3A7lpliA/yxJU5prBVHI902cOdfuxKTkXM1Al16w8xZS8JA1gshwG8OjArNln4PPPz8PVK97E8xl86bK+vqEtIurhcfY4OZzBMTe/9GphcXEYaz+CZGUW03JaixaLwl9RlJg3b6RhqUpEKEvn6cG1t9eD77yl4DB7AxxYdARWLNYEhVkWoDDdgr485MjLRFFZF44cukbTfg8V6Yu+C1ycA2HpMj14c7I2fTGh2PFFNWOOCWxc8hPoLPwUbs95D/wXLIFjy76m1yD020MxvK/KU83ICxWjGdXh3Xf14bzVPairlez5dHR0QtTD2FPssXI4wqirq79SVFQcwdqNGLjp4+J5PxpBhz2Q4jBTMkmyKaSHmzrLFE4t/ArSp04HeO0VaJ/0KmQrTYULCzbD2gVnQGmGBRUneg9vyujRajQYCDMUpCl6DMYx0L0N8gpnWZ0BU/pCmjH7HHyz+BB4zlsOlQryAG++Ai3yr4PH3JWweq46OX9zsd8dieEuQvzdybIasGGDITzwimJnJg4WmIiK4sLnSOY36RkZd1h7EaMgvxy+2G9PU2kpjEFRDRSO/AxLWLZQB04v2gd+s5dBhYIcPH7tn5A/SwWOLDkMilT4ouECjvcxpdfAlFFCSEv0uKKgp32b3ANN6srjd1VQ8HOM4erS96FN8XVof/M1SJs2DeznbYBPl34P0+aaguJ06Qi+r6H4cSee6hRNMDJ0JS9k4Xx2mOfO3z9oL3vGHM4Toh4+1GLtRIyCvHLYuuUCzbA71sU0FMk4nrrJ801g68Kf4LvFB0BnyU7Yu/gYqJLxbo/o0VD4775rDnl9dtQJMVTRnzMeTPSPwdT4LvFwNOhQpue7+LKaM9sQfnxrD/yy6HM4sPgorFigBYpzLMjY3oq+FPr+lrRNSRX3DWjA0SN2EivFVlfXVNk5OMxkj5rD+dWvnO/cWVFf3yC4UFxUWAFbN1+gmVuk4coPxVCEOEbGcTC+AGSmW0vsLbG4x2ef2kJjo+TlraGIHmfOzU0lx977Pogmvao63Skn9H256ZYgQ85VjngrSuRchzqBKQ3Dib/XJ2nAj6eu0Xr8QuAuO/Ko/130xDkTmi1bDvwet1iyttGPhvom2Lf3Cu3hJQnmeRu+iHBm3+aCPztrcZ4qenoMHbC1ET4Gbgxa854BXVabJvD98WAo/Mmy6nDxvCc76/48etQFHl4+n7PHzpnI4Hivq0t4w8w5Y0/Sk47/ari43r1qpSkUFVSwM+/PUEV/2VYgVcDjbrA0dyff1yDff3a990hMkbj6s2ZrQFRkCjv5/uTm5iXuO3GCh+xOcH6bnp7xJJlcH3A76MKFmDN9/FfCpbnk5LXh+rVQcubiobujEX1nRxusW6tPXizjL8e/mM00pfEC+/aeJ+ctHlP1qIv09qIUWJyJiqOT0/KGxkaBbv4x/PSDM3EX9YQb1zg0zMb7+W47eNQpPqYdjehDQ+Jh6nQtulQo9N3xZhgzoKisTucghEhOTrnLHj9nIhIaGq7O2kI/Comb/PZbmALqBejdmOEy4ro1ptDSLB6qOhrRnzN2ASwBPt6HOH0Ng3gOfHmBvLvF52YLC4uq1A0N+Zbcicif/6z4ezLGC2FtoR9XLwcSEYx8eQ4TPWKUHq4jT2Yx9/j/GMwj9HlpGGbeXbjIEDLSithVPGE0otfTdaEz+7guLvTd0Rr+Nr5cMc8eThRirj6M28dVgpG+aHAZb9FiLSgrFZ/j6OjsABcX162sGXAmEmpqWpOKikrEWgWOBY8euU56t5G59ihsrJ2HxRW///4m/PDjLThyxAE+22UDK1YQcWFCy2nC3x2N4Ytm+gxDCAgQT/s/GtHr6twaM9GjOBWUtOGdd3Rhx8eYGPM83W2382NjeHs51hAY2TIpvqzllbTgmr3wEmRgcPAPrBlwJhJXr96cVlFRKTYALiupglWrTAGLHgo1qMEM87UvXXYOvDzjyMsDD41JJrupm4n7x7OzSuDQ1w6kQepKfYIQs9GuWmkMdTXiW/5H19Oj6NG9l57ocRUAN9NMm64Blub3aMLNjo5W6O5qh8fd7eTetdC8e0cOXxqZ8Mm5vjFZA7Q0ndlV9OdhVOw51gw4EwlXD495DQIRXLnZpTB3Lta2F2hMgxg2TEUi5suXxIXTF4zfv3I5CBYsRPffYMRDiIGGW3Y/3n6BvWz6MxrRu7uFkN5YU2reCU4IouewaZMJhATFs18RpqW5EdasNRxR2LMMeal8vN0CGhvEN+UkJibbsWbAmUjccXefW1dXz5rBE+57xIGKqt6wGzm+JGbO1IPE+Bx2pMGJi8mGT3ba0l16uA1X6JhDNfxtGSKka1cFpyhGJfq21hZY/74BTJIzJJ8dTW9vSsftyiqa8NMPV6F6iBlxjx6xoXMiwseUbDh0eGuZLhQViM9x5OcXhJEmwNNfTzQkid7MzJuMuw2G7VKi8GbP1oeEuKGJHmlsaAIjfQ+YPlOfLrnhMYbzu/hZDMyZLKdNs+w0Ngpn4x6N6JHwsGSYMxf3zOvRJbHhiB/3xytPxWg5XViyRJt4DrhreahpwLrg2DeXRiR6/M25czUhM1N8J2JhYVEUaQKY954zkUD3XqhkkoXFgxGJHuvITZmiA0EBwtFgg+HjnQAffGAFU6fpgDwRH74AcLZfXtmIjv1xZh4n6vDP3gox+BkFbVi58hytooPpoiQxWtEjCXGZ8OkuC1BSPkO3FeP2VjwXUZkrDIMVrSDg3AK649ir4/BlsswvMGXqGTh80BayMoZbgLILPvvUnDwPI8HzHsxUiOhnz9GAjHTx30xKSr5CmsC/iVoCZ8Jw/datGeUVlWILuZfImFxeafiiR5NV0ActzbvsSMOjqqoWIsNTwf5yMBw96kgauw1s2mgBq981hVlzsNEbwey5xrT804dbreDAV/bg4hwBhQXl7AiSGaro7S5Jjt9HcL4gKCAWfvrxGhmTG5EXDtad0yZi16V19GbN1oa33taCtWv1YftHJnDsWzu45x4JURHx0N01/Ezh1ZU1sGKl3ogmPdG9X7pUBwryCtnRnhAdHWPMmgFnIoElkYqKS8XSzyQn5sOM6Tp0wkmoMQ1m2Dtv//A8zfA6OrpoSiyc9KutaSTudRoEBiRCZGQ63QREQ0wFAk8kMRTRKyjpgqNDMPvG03hMzq0VWlpaoLioHNzuBFCLjU4FLPfV1tpKzhH3t488wy1y51YgyCuObBJRUeUcrFqlD2UlJexoTwgODuVLdhMRVdVFfy4qKk5g7aAXLNi4eJEox51QYxrMsHFOmWYAWhrug7rbz5qniZ665qr64Ho7kn3j+ZMYnwnvrcHhzPBde5xzeJNc7/ffO8CjR+IvHpc7d3ezZsCZaCQmJtmydtBLfV09bNt2kYwjhz95hEaj8RR0aYAPFnscDwxV9HdcJGYKe4Y8JucRDIsXi2r8T5s5/BUD9Fwmy2qCidFtdswnVFRUNJmamsqxJsCZaNzz9PyCtYV+GBp4kEajI1EkTzPRrjc9+GCDFfj5iDkTz5wXRfQlxaXwi9p1UFJWZ+P4kS0R4tBsylQtwU036ekZMeTR/x9RC+BMOEysrCaXlpaKrdv5eMcRN10fVKaNfP2cTo4Rb2H6DH348bQzZGWKrxc/K8a76Ntam8D1TgisXo31AXA+ZTTxALj5yATee09PrNY+Ehsbb8AeP2eiglstWXvopaW5BbZusaYTc0KNajiGy1gypNd/++1zoKfjDslJeTTB5LNkvIq+rrYWblzzgZ07zcmQ6Cxd5ht9og4zeHOyBpiecyO/0H/XdFNTU9f1684r2KPnTFTc7t3b1CGQcMHBPhjkFXWoIIQb1/AM169l5HVh0SIjOHz4Gnjei4Gy0mcz5h9fou+CyPBEMDZyhTVr9EFBSYuGzE6ZbiZ4bsM1nLWfN08TsgWCcjKzcx7OWLfuP9mj50xU1m7f/ufc/AKxiJq62gbYtNGKbvcUalwjMXT5cVUAg1aUlHVIo7eA48cc4a7bQ9JIi0hPNPTabcPBUN8D/vr3MzSgh5bWGmCT5QxJ76gFzjcx8460eQy52fkQEZYM6r9ch4+3m8LMWVrwpowGDeKRbgJNzJyjBbrat9hvP+Hx48fgHxjIq95wRPj4BR54LBAa6k7EqKyiS6PthBvZyA17VwVl0aYbJfIbixYZws4dtqCv6w5X7QMhOTmfhumK1rtHh+vtCPhyvy18e9RR0LCw5pFDVyEsLI19Y4Q87oDG+jqoqqwh9y6UXIsTHPz6EixdokPuI3npkGEObgzCIc9IJ+kGMxweLH9bGyrLxav/FBUV53355Zd/YY+cM9E5ceLEH3Pz8pJZ++ils7MTvjt2g+ZdE2pk0jKc7cdwVgz/xQ04uJ10/nxD2PC+OXz+uR1cOO8H9z0eQkx0JggNRZ4OvtAGMxz74p/DpRtysgvIUCWUvKj8yIvDFt5fbwCLF2uSFxlx28m1TCaGCStVp0nHfZdkOGOvoKgOrneEvRXSy59gj5vDEXH//v1tra3ivSqutb+/3pK4wQaCjU3ahkMANBpnT9xfzCSD6aqUVXVg9hwjOuT46svLYGbqDqWloythPRww9152Vi4kJWSAx70oOP7tJdixwxSWLNWlO+fenPwLEbgeOWccwog8GbwOoWuUtuFef6ypd+qEPZ6p6IT7UFBQmHNU7ej/sEfN4TwhPiFJfDBICAtJgwULjKg7LtToxtpQRJPlDIjHoQ3LlhoR0V+BW05hUFIy9CKWkuh61AZtrY3Q3NQA9XV1UFNdA+XlVeDn+xCsLV3BQO8W/PSjA3z6qTUsJm76zFnqICd/ls4FoGeCE5SYc/75pcfG6Dtd2P3ZeRDKj4Demo+P/x72iDmc/hgYGMiUlpUJ1oi6fy8WpkwV7S4TbnzSNewl8SWDvee8+QawZ89luHk9lEX5DcUVR5e9m9Z193sQAffcQsDpRgA4XPWDixe8yZDBC6yt7sNXX1ykS2ZbNpvAurUGsHy5Dvk9dVojTl5RF2TldejkGI7HcRMLrqGPVb684ZspuT/6xBMzlli+OiU1w4M82t+KnjCHI4C3t+/21rY2gbTYAM43I2DaDMwBP3Y9PrrFuJVUXlGbiNAC9HTuQmpKgcQCjT1UVVZAXGwqeHpE0SQV+/ZawdatJrB0qR5MnUaGB8QFl5NXh8myZ+G1N34h9jO89uYvdNgip2hMXmYY525Cx99YnLLHPRe56ONF5E8MPQus3vv+ehPIyhTfSYeUl1dUWtvaKrBHy+FIJi4u3oi1GzEws87CBTjWNpC47j0SQ3HhywR3u23bch4cHUIGXcfHPHL5ecVge9EDfjhtD2vXmcD06cTtlv+F9Mr6dJYcIwLR/cZJLuqCkx5a1Euj4cTa+BPzUAyv5U0Zbfh4uznkZAkLvqO9AwKCg/exR8rhDM6qVav+KzMr5x5rP2KEhaaTHsaKupbSWM7DmXtZeV1yTAvqwjfUC1dfRbc+N7sQHOx9Yc/nVsQN16AeAY71MXpQZaoZEfXYzpI/XzOlQww5eU04cugS1NbUsvsiTkJCojl7nBzO0NDR0fl7fmFRFGtDYmDyioNfX6M76kaS5AENe3fsjWfPMQQjA0+oKBfOG9f1qAOiI1Pg2FFbWLKEjLFlztIoNuUp2HMLH/tlM/RQZOUNiDejCeamd+k9kUR2bp43eYS8Qi1n+Jy3s1Mm40KJESvtbW3EvfYnQjQGmWH2+rg2j57Cpo3WEBwgFiLA6AIf7yjYu8caVFSJ0OX1iLuORSBeTLd8pIarFziZ+MEGYwgJHDyDbnFJacIPP5x9lT1CDmf4ODm5KpeWlQ8aqpaSlA9f7rcHZWVd2nM/rffFSTJ05w8dvAYlRcLLbumpOfDdsSugrKIOk4nYRQkphY/3shotPy2vD7NmaYCOljNdThyMsrLy9Dt3PJTYo+NwRo6RhYV8Xp5wCaweMIXVLadw2PiBFV3mwkk5IfHT2Xki4tMnXWhaqYF0dXWAs1MQLFqE8emj32b6IhrG4+NEqYKSOuz53BIS4zPY3ZFMaWlZ+rVrTlPYI+NwRo+FhcVfMrNzXFgbk0h1VR1csPKly21U/Mr9xY/r7kcOOwqm02qoq4OT310BeUV1uoT2os6uj9SwZ8flSlk5dfhwmyl4uIeRIZRwWu++FBQUhllb86U5ztjwnzExsTqNTeLJGQZSXlYNNuf9YO0ac1GQC3H7ZeSNYPMma6itEa+6Ul5WCZ/stKRLUROtd8ehCy4zKqlowJbNxnDT0R+amyStYPQnNTXttrq6wT/Y8+FwxgZvX9+dxJ0UXiAeQGVFLdja+MGmTedh5gxNwbz4lZVVpGczgzdkdCfIJJ0pnfQUFerQA9UpZ+HTXZZw727YkMXe2NjUHhMT9zN5HDx/PefZYGPjMCktI9NRaJOOELW1DZCSjIUX+ofQYg+/dasZjS57fvHrY284xKE74VTOkWvVAGVVDfjgA2NQ+8kBYmPSaErtoVJYWJTm6uq+kT0KDufZ4unpvbWoqCSGtcdhUVVZDR9/bE5EgFt3Xz7Bo9eiNAXj43XpTsGZMzVhzRpd0Ne7A973w58aWjyQquqatqioaGO+JMd57qxfv/u/QyMijpWWlw+5kB2u8R/62oaO4V+OHt6UzrrT6EI6f4GpsM7C/PmasH//RTAycIaY6DSor5McRSeJmpqazqSkZBd7R6fl7JZzOOODr7769tWAoNBD2dk50Q0NjV2YokkSpsZupPfD6i0vmuBxGy1z18m4HDfoyCkZ02SUuOV23jwd4r2YwvfHbeG+ZxSkp2aTEc3wE4G2trZCRWVleVx84tUbt269zW4xhzNu+d358zafVFRUCnZrPt7RMGWqJo00ExbW+DF00THrDYb84i48jPPH+vIYfjx1mhYsX64F27aeAz3d23DLKQAS4jCOaSRZePqTkJiUpKamJsPuJ4cz/vH29f2oncDacC8V5ZWwYrm2VBNujszQw8ANOihqUzrJpqhqTM7LgAbGiMwQZOXVQUX1Z5g7TxNWrTSAw4dsQVP9JliY3obQkAS66aWleWgz7gPJzCiEuJgsmrRyIC0tLbVXHB0XsNvJ4Yxvjhw58qf8/IJU1n576e5+BKdPOZDeUoeJTkiM0jF0wTH6D70JjNfHYB80dMVx//zrk87CG5N+JqI+S9fHp0zThkWL9WDLFhPYvdsCjhy2gR9OXYbbt0MhNiaduunFhcXsSkYGVr0tLqyAG46hcPzb6zQd+IzpmhAaLHarKCkpaW7slnI445uQkLBTrN32475nJMgrnBnT4BvV6WY0/n8yccOVVbGUtAasXq0HmzcZEzOiKah3f2YBpqb34JqDH7g4B8N9j3Dw942EjDQy/paCa94XLFEdH5cD9lcC4csv7GHJEkPyksFKNvp0r/8kOQPYu+cy3f8+kJqa2kf29tfnsdvK4YxPdu3a9ae8vPx01m57qa6qhjVrMEvN2Lj16KKja66kfJaI2xR0tG7SYJfKyhpalBODXtCwhFRH+9jk10dwRJOeVgAhQSmgpXEXPtlxERYsIN6Fgg4dMuDsPu4w7DlvnAxUUtYF19vCu5gfPoy1YbeWwxmf3Lt3/5Ouri7WZJ9gaeEOk+U06Ri6r1hHb6Y0a668ogZ8vtuSbsWtrx18N9pIaGvrgLLSaqirqaeGocQYalxUUAH+fklgYe4NZ39xIV6EHSxeYgJTphCRy+vRl5zSFGM62y98/udIr28An3xyiZYTG0hZWXnFWX19vi7PGbf8Nj09w5O1114KC0pg6VJtOvst1OhHbrgbTR+WLdUl42Q/0osPpwfvIq73I7o7sKa6HgL8EyDALxFaW4WTU5SWVMHnn1qRoYIZbNxoBRs2WMGqVedg8SJ9mDoNxY0Tf6I/FUlvPpzcAqIqs3rg80C8wm93dzf4BwZ/ze4vhzO+MDGxVKqsrBJTnrWlB0yS0SANXJqprESRbh9uNYfkxEz2S8KUFFdBVEQqdbm9PONAXd2NVrXZu8cOPtxmBe++a0rG1/qwdo0pVFUIB8/gDHtocAYsWmQMr03Sp94FThD2zXE/mpyBOMb/7th19mv9iU9Iwkw4HM74IyAo6NDA5aeW5mbSK5IxrVQz6GLqKH3Y/pEFEXQF+yXJmJveBwX5X0BlijbIK2hSdxrr2uGEH03GicU1FYxg7VoLqBYOLeglIiyTCl/aNQBwuXDJUhM6HzCQqqqqGm1tbUV2mzmc8UNMXPxt1k57ue8RBgpKGnSiTaixj8Rwj/7yt/UgL2doNe9NjO7Da29ogjLpkSUVjMR8/uvXWz5V9Mi9uzGgOkWPuuVCxxqJ4eSerII2ON8MY7/yhI6OTrhz5+6H7DZzOOMDNTW1/yksKsZtdP0wMnSDNyapk4YtHdGjK62opAlON/zZLzydp5WqRkPRr1s3NNFjEY1jRx2pSy50rJEaeiBff30NHgkkvYyOidNgt5rDGR9YWV1c3tDY2MnaKAVz0+/+DOvgSc8VxsqsmzYaQWvL0CfthiJ6nGXf9uFFaGxoYN8anLiYbJg3r6cCrfAxh2s4T7BurRk0C5TszsjI8iK3+f+I7jaHMw5wd/fcOnBvfVNjPWzaJKoeI9TIh2u4G2+SjDpYWkhMyS/IUESPL6adu2yhpfnpWYEQDLr57DNb+rIQOt5IDF8g8+cbQFqKeH6SnJy8pNWrV/+B3W4O5/kTGBx6YOAkXk11LaxcqUtnuYUa+XANx+Pyimdp0M1wGKrod3wydNEjFmY+tBz1aGbt+5rqNBNQUjUAJyfx68vNLcg9epRXnuWMI8Iiok6y9tlLVWUNLFumTbefCjXy4RpOBmKcvOe98SF6V5coInoNqQUc4bKfgrI+2NqIz1fkFxQ2WFlZvcJuN4fz/AkNj/yetc9esBbd7DlnQVlKs9worsmyv8At5yD2C0NjrER/wzGM5gSQtujtLomLvqCwqMPe3v6f7HZzOM8fIdFXlFfDokXS3DePoteGH05fA3gsHuoribERfRccOnSNrvcLHW8k9qSn92O/8QTS09cZGhrynp4zfhBy76uramDFch2pufdoeCysIY8beIbKWIi+rKwaVqwQBdUIHW8khmv1KHp7+0D2K0/IyyvI//rrr/8vu90czvMnMDBYbCIP88GtWaMv1Zh77A3lFDTB9pIX+5WnMxai19N1J9/Ro+cjdLyRmMpUE5gx0wCCg8TThGfn5sUvW7bs9+x2czjPHze3e9sGVq3p7GiDvXutaYVZoUY+UsMQ2JUrdKGqQnL9+r5IW/RRERkwb54R3SYrdKyRGr4cV797jrwsxWMF0tMzXMlt/rXobnM44wAjU9P5tbV1YntDL5y/D29OPksEJ70w3J7NNse+vQyPHvWLBxJEmqLHrbQb1lvRuH2h44zGMMLvwAEHwYi8sLCIU+xWczjjg127dv0uKztHzC8NCoiBKVO1pBqnjobLdzJyWvCL2k0aKDMYTxM9uuhvyBjBlm0XB60wU1NVD/v32tPNPtJ069EwvFheURtuO4ezX3tCY1NT95Vr195lt5rDGT9Ex8RdZe20F8yL9+E27Eml3zPinnVZeS04fdIBmhsli1VI9DhphvvecT++vKI+zJihAz/8cBM6OsQLayL5eeWw9/MrtAyVtAWPhrv9sOhnRbl47H9eXkHariNH/sRuM4czfvDy8fmwo1O817108T4RC+6nl6aLLzLs8TEt9faPzCAsRDwJBdIjepqnnm6jNQRFZW1YuNgIDh64CtZWPpCanI8ZaNk3+oNx9h9ssIZJYyR4fAHJyeuQ+yS8iSg8MlKd3WIOZ3yx64sv/lFaWpbP2movuLy2erW+VOPU+xrOF0wmLvesWVpgqO8C5aX999gbGXjCX/+mRgSvB+++awZff3UF3O8+hPRUPNVu0YcEwHRY+rr3YMFCI3LuBoPOCYzGcHfdR9suQEO9uLdSWVVVZ2p6Xo7dYg5n/BEZHm3I2ms/bjgGEFdcnfbMQg1fGoYFKXDf/MKFZ8H+smfvJJ+tjS8cOWQPd1wiRO7zUwJ7igrL4ZKNP7z3njnIkN5d2rP0fQ0TeMyfZwjRkVns1/sTHRNjzW4thzM+uXjxokpFZbXYmhNuhf10lwVx83EPunSFjxtxFJRNyLG1YPZsTfhi/3nw9Y6C7m6RuJtpwknJPbqIx6TnLwB9PXd45x0z6m7jVtexcOd7TGmqCSgo6cLVy8HsHPpTUVlZQe6nPLu1HM745eHDWEvWbvuRmZ5PCzpiwgohEQzX0GvAtNJyChpEqHqgo+0MCfFi2bcHpbamHtzuRMH3x28SD8GQHE+PxgGMpdjRVKaZ0NJYetoe0PVIvNYdJsQMDAw7wm4phzO+sbKzm1xWXlnK2m8/rl31AQVFjVEVvFDB5ToyhldR1aATeHjM2uqhp71ubWmF4MAk0FB3g7VrzEBlChafGFs3vq/h8uVkWR04eeIWzZMvRHpGpie5lTxpBufFwS8g+Nvux0Iu9WPQ03GBSTKaw167x91sGBSjoqoJez63ojnuhxo2W1dbDzEPs0Bb0w22f3gBpk3XI+N1Xep14Bq50O+NheGLBV36s7+4QduApCM9lJWX51lb2yqwW8nhvBi8+/q7/56WkeHB2nE/2lpb4YdTjrT661An9pRUz9FtrJs3mcC9u6HEJRbOT98XLB4RGpIKRgYesGXLeZg+Q5euy9MXBxlPC/3OWBpO2qkSr8LY8D48EnDpkZraugZX13u8NDXnxeTSJQfZ4pJSwaqP7UT4p09dYz2+ZOFj746u97x5WnDO+A401NexI0iim1aCtTT3gS2brWDmLAPao8spodCFf+NZGAYnzZptAI4Ooew8xWlsbGoPDg77nN0+DufFxMPD+726+nrBbrmjvQ20NJ3pRJxoJ15/8YvGvpqwc4cFxMdi3XfJFBeVw43rYbBvrx1NWIlprDDXPvboYz0pN5jh72MU34oVphDgn8TOVpzmlpb2kJCwPey2cTgvNoHBwQdaWlpZ8x7IY7Cz8YKZMzXocp4oC40pfQkoKmmQMbgzNNTVs88OpBsS4nNAS8ONlpjCsTIGuihNeb5CR8Pfx2GEsooufH3gKmSkS87RX9/Q0BkeHrmX3S4O5+UgLi5BU6iwZQ8YQrv9IwyG0YTX3tSm0XUOV8QzyPQQH5sNP552hvnzsVfHZbaxXVMfquE5yCsb0eW499+3hNvOkXT/gSRqa+vaQkLCueA5LyfxiYk6gwm/pamJjMXvwoEDFyDQP479bX8y0gvhh9O3esWOs+FjFR47HEOx44tHloj9nXdMwcLMGyorBl9GrKiorPP1DVjPbg+H83KSmJik1Slh5now2tpaiZAewNIlxkzs0ktTNRqjW2KVjMiwQpuK3dzUG4qLnl5fr7S0LMPZ2Xkhuy0czstNSFjYN7V1dUNW/sOoTNi54yLIyuvSZS8h8T1rU54i2iijRMbsmzZa0rTVRYVPFzuSk5MbbGRhwcNrOROLgIDgD6qra8VLtPahs7MDrl4JhrnzDOkuuuc9Zqe9Ohmvy8rrwaxZevDFPnu4czsSGgV2x0kiJTXV9W9/U/0vdhs4nIlFYmKKL9OCGE2NTXD6lAvIK+rQPfBCInwWhi8aTLaBYb+KSjqwZq0FjerDicSuIaTqGkheXkHwMTW1v7BbwOFMHAKDw052CiTcQKqr6mDfniswWVb3uQTUoNBx2Q+FLq+oB8uWGsE331wHtzuRUFE++ORcWGga/Pyv25AQL1bAt5fUVJrk8t9Ed4LDmQBcd3ZeSMb0gj4xToLt+PgiTJLVpxllhEQ5FiYSOs6+GxKh68K8+QZw6KADrTaTn1cG8FjyFERXVyfEPsyCk9/fhDlzDemS46qVZhAbk8M+IU5YWBRPdMmZGGzZsuWPeXl54tkfCbXV9fDpLlt4Y7LuM1mG6+nRZRUNyThdB+YvMILPd9vCeWs/urceq9gMRl1tA9x1jYavvrSnIbYY108Dg8ixcZJv5QoziI8T7vFramo7rl93XsJuC4fz8hIWFvETa/f9qCcd//59V0gPPzZ56Poa1tbD9Fe4d37BAkP47NNLYGsTQMfpj1niDUl0dnQQFz4F9HTuwbp15jBlqh7IELEL1alH4a9fZwX5ueXs2/3Jyc0N/fjjj3n5ac7Ly4ULFxQrq6rE0r1iEgm1n24TweuOiUuPPS+KEsNicZw+c6YeGULYgJW5N6SmFEB7m6QQYRHNTc10E4+xoScdeog28eg8dVsu/i7G3X+53578hvg2WqwH5Ovv/yW7PRzOy0dCQuJlUXPvj9udaFBS1hHsLUdjdOZdxYQG86BQP9lhAxrqrrRHbyJCHoy6mnoIDkwBXS03+oLAFwV6BpjcE89zqN4IvhRw6IBBO0Lk5OWn7dt34o/sFnE4Lw92Dg5zamrrxLpUnCRbudKU7ogTEs1ITTT7rgdvv20C6mdcITQ45SlLbI/h8eNuCApIhNMnnanrPmOmKKMODgVwSDDSeQYscolhw4nxuey3noBpsfz8Ag+w28ThvDzExMSdZ+28F0xc+a+fXKgLLK2JO+yBMW8ebq/VVHeDwgLh8XQPNdV1ROjJcPjgFZqxNzQ4Fd5aZgD/fF1XanX10XAH4Rf7rwhW48nMyop69dVXf8duFYfz4qOmpfVGWXl5JWvjvSSQnm/2HAOp5aZDweMLZPuHF2mRSUlgZt7YmGzQ13WHTRutQWWKLvz1lV9ofnykqKgS9n5+GWRk9QYdsw/HaLzBND0I8BPfU19TU9vt5HR7MbtdHM6LT1Bo6CGhfHmnTzrRiTUhkQzXqOCJK/7t0RtQVSFeGgrd97TUArhiFwg7yNh+zhyR6y6vZAjKRNhYAQcr4fSAYQTfHXeiwTnSChDCaz36jaNg0c2QsAg9drs4nBefxMQkH9a2e8nKLIYlS4ylFmKLLj32zph/byCYEPPAl3aweLExyMprk88SoeMYnU3G4dBioOiRjvZ2OPOLGygQ4Uujx8edgYsXG0F+rnii4OzcvKBXlyzhLj7nxUdXV3dyebl4IXncSIMClMYSHb44li41gUwJ2WkuWj+Af76mTj8nNOsuSfRIe3sbGe9fAxlapEP8u8MxvFYlZW1wd4thR38CuUfVP/54RpHdNg7nxcXV1f2j9vb+KfI6O9vhi/32tHcWEsdwDAUrp6gP+nqCiXcpl20DQE5BR1DwPceQJHqkqqIONn4gnZr0GLDz4w/O7MhPION64ON6zktBUFDIN6xd94LRd2veE+XBExLGcAz3tc+brw8ZaYXs6OKMVvSIt2csTJmqSyvfCh1jqIYvjgNfXiFHxNCcJzx61AW+AQFb2G3jcF5cUlJSbVi77qUwvwIWLxIlsBQSxnAMU1StXWtJemPJu9+u2I1e9I86O+DLL4h3ojg67wRTce/8+AKNCRhIWFjEWXbbOJwXl8zMLHfWpnu56xYNKlMMQHXa6EWPEXJbtl6E+kHy4duc96PzB6MRPeLoEAwKSqOb1EPRf7TNSlD04WGRuuy2cTgvLhkZWa6sTfdy82Yoce2lI3pMn7VqlSmUl4qFAfSCYkZRo7iFjjFU0Xt5xYLqVMNRnbdI9NbCog/noue8BBDRu7E23YuXVxwRjgGt3CokjOEYrqFjuaqH0cL13ZHxJvod289Lcu9/YbeNw3lxEerpKyvqYOkS6UXi4Yz48WPX2dHFkZbor14Jou69pGHCUAwn8r76Qnwir/NRF/j4BHzAbhuH8+ISHROjydp1Lw31jbBurbnU6tTjrrcZMwzAxzuB/UJ/hiL61ydpgYnRffYNcXD77c4dNkS0o5vIk1EwgFMnndhRn1BbWwcubm5L2W3jcF5cvL199uBOsr5gxZeTJ5zpllchYYzE0G1eu8YCcrPL2K88QZLoMVgGt95ipt3X31ADCzNJon8M54y9QXGUvTx+F5Nr3naJYsd9Qnl5Re2ZM9rK7LZxOC8ulpYXFtbXN4hlkPDyjAVFZR2pjOt7DNNVbd50AbKz+oe59hU9FR7NbqtHc+GtWHGODg3u3Y2G4mKhvPVdYG7qA0oqGIM/unPF4cyihYaQkyVeyDc3Nz9sxowZ/8luG4fzQvPrjIzMaNa2e6mprocPNliRHlq6++hxfL/mPQvwffDE1T9n7AX/fE2D9OgG5EWjB0uXGsGRw45w7WoQFOTj1lvhFFmZGUXw/bEboES+I40EH7jhBvPpdXSIF/ENi4gyZveLw3nxkZQXz9EhhOa1l9b21R7DyTLVKXrws5orFBVVg4nhPZgxXROOHLpG68MX5pfB40Gy2yYn5cEvai6wdJko685oo/DQ8BrxhePiHMF+5QnNLS3g6uq6lt0uDufFR0dHR7asXDxhfGtLGxwgPZ+0kmigOGnCS3kdmD3HELZsNgN3t0jIyiyElCTxrDX9eQw52SWgp+MOCxYa0XOSZpENWUUj2LbtPLS2trDfe0JObm6SoqLi79nt4nBeDsIjIkxYG+9HemohvP2WKGmlkFiGYuh6o1s/ZZoe7PrkEthc8IWkhFyaufbx4/5LYwNpb2+HsJBU+OkHZ1hGenYssIFjfqHfGalhL48RgS63xHt5JDQ0/Ed2mziclwdjY+vXSkvLBHfFPPBKgOkz9Ie9hKc8FcWO2W31yRj9GgT4J0FL8+AJLxHcLovj9fPWPjTP/rTpekSUpGcnYh/N7Lwkw3Pct/cK6eXFM+KWlJaW6+mZvc5uE4fzcvHAL2CfpFJWWDRi2vShCR+FiYkwUOyYiSYyHFNjiUe49QU3zKQk59Pqsnv3XIEF80Ulq3oSagj9jjQMr2fefEOagVeI0PBwdXZ7OJyXk7iEREfW3sVwJ8LHHn+wGX0cZ+OOuY+3X2T55iSLHQtXJMbnwGXbQNj92SVYtMiIfFeX7uNXmiJ6eQj9hrQMdxEqq+jC9Wuh7Iz6U1xckqNuaPgKuzUczsvJaXX1fxQVF4tnh2Tc94yj4kRh9hUl/jeO2+fNNwIr8wfQ1CjZjc/KLIIL1r6wf68dLVUlp6BNS1fRklNjLPQew6EHTijqaN4jZyQ+r4D75729ffey28LhvNxcvHh5VkVlpXiECgNz2m3dcoHOoOMkGM7K49LZls3WEBWZyT7VH5yQ874fCye+d4IFCwyY6y6K739WQu8xfLmg4M/87AZtbe3sDPuTnJx6m9yKX4vuCIczAXB0ur28rq6+gWlAjPKyavju+E2YLKsDMrLacPrULagWr4hFRNUKHu4P4dNdl2hdOXxRPA+h95i8shEoEZdeS8OdJtYUooi49Za2tm+wW8HhTBwCgoM3VlXXiC9cM7AijbnZA7AwfUDj9QcSEZZOZ8WVVXXpRpbnUcseDWMM8EWDL5ylS43hBhnDP34sHOVXXVPT4u5+/y12CziciYeLm8eqiooKia6+EF2dHWB2zpuWh8bQWmlEzI3E0JvAZT5c28e69Ce+uwkZacIZeZGmpiYIDQ3dxy6dw5m4uLi4zS8rr5BckqYPJcVVcODLq2TMLP0gmqFaT+SfnIIeLFliBOpnXeFhFJ6+5EAgIviuiIjob9glczgcHWNj2dy8PK/29naJBeKTE/NpGaqeCT4hQY6VibbhGtPJQWVVHfjowwtk2OEFebll7Owk09ra1hUeGXmIXSqHw+nhzBltxbLycsG1uJysUli7xpIIXl8qcfpDMXTfcT0ft+xibMDbb5+DY99eB3/fRGhqbGJnNji1tXVVoaERvCothyNEVHS0XvfAjBsEzJO/82MbWmVmrAUvEroJFTou+82fbwBff3UVblwLphtynhb5N5CgoJDT7PI4HE5fztvZKVdV14rlsX706BH860cXmCQjOW/9aA2Pi5t2MIBHnvToGDb7xb4rcN7Kh07MYQivJIqLKkBbwxVuOApH3SWnpjmyS+RwOH2JiIjSZzrph/ONMNLjao/JchweE/Pmy8hpw5y5RrBntx3Y2gRAcmIudHVJ3m//qLMd4mKzQVf7Lk2//eobWvD++5Y0999ASkpKa8+e1X+VXSaHw0E0NDT+WlRULLbWVVZaBe+9Zw7yUsyugxNyCspGtKz1zFn6sP2ji6RH94WoiHSJa+s9ZKYXgvPNcNi39zLMnWcIk+V0ad59lWmmNJ2W2x3x3Hc4WvH29v2SXSqHw0E8Pb33dHX3FxzmhT9nfB/enCy5Ms1wDHPxYSILBSUd2jv/8rMLzZXf3DTYhFwXzWeHG2YOHrCHpcvO0Uw/dGfegDh+3D773bEbxEMQf3EkJCXdYpfK4XAIv0lPzxQrfdXS0gwbNliMOoeeKMGGPk2fhb36jethUFQgeZmtq6uT5s2zvxxA02thPXlFZVHEn6KqscTS2vhvWC47N1s8zqisrCxv7969/2TXy+FMbI6rqf2toKBQrH59eFg63S8/0rE89sK4pj53rgGtL3/fIxbaBJJY9JCfXwoPvKLgm28cYfEiQ1rUAjPmDjWxhipx8ZVVtOly3kCqa2oeXXF0XMAumcOZ2Li6e27t6BBPrKGnc5cG4QgJ7GmGIlVQ0ofPPrWDh1GZ0E16byEqyqtpgYzjxxxhwQLcJHMLTIx94E0ZnRG9bNCj0NC4y47+BHT5/QIDP2aXzOFMbIJDw7WZNnrBtFJ791yma+VC4nqa4fcOfu0ADfXi43VMpxURng7amm6wbp0FqOBmHXkD+Mer6mBt6Uc/8+0RR5AjxxjuXAIu+YlKVomv5UdGRhuyS+ZwJjax8QliRehysovopBlWnxES12CGobLvvmsORYWYz14EJseMi82iqbK2bbWGWbP06cw7rgpgOC8G/Lz+piYYGXjQz2MQzlvLTIZdaw+r7Hyy4yI8FtgRGJ+Y6MAumcOZuKiqqv5XXkGhWCGM2IfZZDyvN6LcdTiO/9cPzuxIIu64hMHMGdp0Xz6uy2Olmp7IPpqgg/T0cnJn4bKtqKfHnvqrL6+QYw1vEhFfIls2WQmu8ccnJt1gl83hTFzmz1/+j4qKSrGMkVERGaCsogmq000FxSXJ6OSdogGYnutffRZTYr85WavfJh2alhqX3lR0aVDOA694aGzs2drfBYcOOoAM+fe+x3+aoei3bpYg+oSk6+yyOZyJy3c//ihbUVGFAe39cLgqqnwzZcYwRU8Me3p9PcxL94TLtgF0wwy+FEQvBkO6DIfFJzzcY8TCbEuKK+Hdd0yHXexC1NNb8p6ew5GEnrGxSkVlZTXTRS//UnMhY+6RFb/ASbyjR66xI4lA0cvI69DgHDny59Yt58HZKQyaBXLkt7e1w/fHnejs/3An8lD02z88L5jlJy4ugcfgczibNn38OnHv85guerG5GEB6et1hiw4Ng2SWLDHuFyRjc94XXnv9Z1i/3pJO5tXVCqfmKyqogOPf3iTj/pHt18cqPV/su0yOJD57HxX10IBdNoczcZGTm/X/SkrKUpguegkLSQNl5eGP6dHwRYEz8zra92jee+SOSwRYmN+HqkrxxJpIS3MLuDiHw/p1FnSpbqQJOnBC8NSp/pOISGfnI/D3D9rMLpvDmdgkJqe4Mm30gtVgZs4a/uy9KI2VIUyS1YGVK/Qhn2W1QdEJUVFWA6bn7sO2reeJay5awhuJd4GGLyhFJW1wcxVbjICqqur2S5fsZ7NL5nAmNqGh4YZMG71gj7xxozVd9xYS2EDDeHicnMOsuNs/ugAXz/tBSlJ+n9n4/lRX1cGli/6wbq0F6Z11aXz/SHv3HsNhxaLFRnSDzkDy8wviVmxZ8Ud2yRzOxMbd0/Mjofp22ANjltmnZcrBGXYF0kvv+NgG7rpFU1ddEnk5pbTyzfr1FnTOAMU+0p59oOFW3aNHHOGRgFcRGhamzy6Xw+F8c/r0K4VFxSI/vA9lZdWwcqUoyYWQyNDQlZ87zwhsSK+N6aWFwLh7HC6c/eUOLF9BPAJ5HVqQQlpiR6Nx+tP0WG29/tTV1XdcvX59IbtcDoeDxCUkXmAa6ced21E0eEZhQDgsClZWXh/eW20OIcGp7NP9wSw2mNTiqy/saXQflqLGhBfSFHuPYe6+L/fb0+IcA8nKyg4gl/hb0ZVyOBwKbjutrasXrAF19UowLROFvTMKDEU7ibj9n+6yg+xMsbgeuhzn6BACmzZZsc00+qA8RVis0jDceosZeDCKcCA4bPHwuM9313E4QjyMjbdhWhHD6UY4LFiAs/J6VPBfH3Cgte76glF1NxzDYNNGq97x+mgn555mOIGIy4NG+p7sLPqTmZmdRC6N9/IcjhB65869WVZWns/0IgYWutj7uR18d/yGWDKMzPQiOPS1AygqEbGTcf5Yix1N5HHo0fTYQoUqu7q6wcc/8Gt2eRwORwhXd/d3a2vrWpluxOjo6CDj5v6z49734+Htt0TFI59VTTvs4SfJ4PKgDZSWiEURU/ILCuJ279793+zSOByOJAIDgz9vbm4WrvHcj8dwxS4IZs40GHSGX9qGe+wx+OeLvVegIO/Jnv2B5OblJ65evfoP7LI4HM5geHv77qyqqhZbxuvL1cshNNklzsgLiVPahstyWCEXVwJ0td2hpVmiQ0Jpa2sDT0/vLeySOBzO07hx49a79fX1gqVlggNSYPoMInjlsRU8BgbhzD8tmDlVD/Z8bgfRkZnsLJ5OXFyCE7scDofzNLy8fPZ3dnaKJZHHVFarVprSsFshoY7WcIKOlrhSMKSlqOfO1aeThN73YwXLWz161AkWZt4QH5vL/uYJxcUlRWpqan9hl8ThcAYjKSn5FtNOLyiwb4440p5XSLDDNRQ4TsqhyOWJ14AJOOQUtGDOXEPY9YkNGBt6QlpKgWDOOwTj7L89egP++dovYG72AJPxsX8RgVl+nW64bGWXxOFwJHHw4LE3y8rLxcpc3XV7CErKuqOapcflPAyowaU9GTkdkFfSo3XsNqw3o/nxnW+EQ0x0hmB0XQ/NTc1w7WoIrCQeB+bGnyxvBB9+dFGwWk5QaOj37LI4HI4kHBxuLK2uqRng2nfDwa+v0j3rQmIezLAnl1M0oN9VnaILS5cawccfX6IFKK/Y+UNUVCa0NLVI7NF7aGxoAk/3GLq5R05RFMOPx8cZ/eVvG0N5qfjyXUpKih27LA6HIwlf34CtjwbUhMMUVJheGnvogaKWZDgRJ69sQN313Z/a0vp1fr5JkJ9XKnGDjhClJVVga+MHH314ke4FGBgAhJN9C+brQVFhJfvGE3JycnzIJf0f0ZVxOBxBIqKiNJlmennU2QmbPrCgiS76Cnswk1cygD2fX4HYh5mDuuviPCauegt4ecTQ2vjLl5vQMlcodqGhxWCiz83Lj5KXX8CDdDicwYiOjjFlmulluKJHl3vxEiPIzxXflCOJ7KxieOAdD6dPOcPGDRYwZZo+Gffr0qIbg+3OQ9HPnydJ9HmRiorLfs8ujcPhCEFEb8Y00wv21Nu2WA1J9OjW40aYX36+Qzpt8SSVOOFWVlJJd+lhum0NdVc4ftQBtm8lbvsUbbozD2fz+xbFGMyUppAXzCIDKCkSq8MJ2dnZ98kl/Vp0ZRwOR5DIyMjvmWae8LgL9u+7PKT1eRxv4067q5cD6Vfb2tohwC8BrtkH0Zr3H++4BKtWnYN5czHAB2ffSY8urwPHvrGFNWvOD7ucFQYJrVtrDnU19fT3+pKQkGjOLovD4Uji3r0Ha9sEdq4ZG90n4sQUWoNnyUXRK6nqwy2ncPo9m/PeIC9/li7PTZbVpi8OBeKyY+JNXKfHUN7X3lQnLwU/UPvpNs2dL3RcSYZeBX6vm2Xf7YuPj98BdlkcDkcShoamsyorq8SC2xPjcmAW1rgj7rSQ+HpMZRpxy6cZgpdXLP2eseF9ImpNUCEC7zs2x+PgOvv8BYagrXUX6uuaQFPdbViiV8YhwFRdCAoQTJXVanXx4nx2WRwORxJbtmz5fXZOTgjTTi/t7R1w5PDTI/J6RP/gQTz9nqmJF0yS0eodn6MngOP26dP14PQJJ0hOfFJvA/fs45r+wGNKMhk5A9i39zLNkjOQlJS0MHZJHA7nafj4+B9h2ulHUkIuzJtnQCfahESIhqJXnWoIHp4x9Ds9oseeHpfdlFRFm2eCApLJvz4JnXW9HQVTpxvQCTyh4w40miprpj6Eh6azIzwBk2gEBIceZpfD4XCeBm5UKSgsEt/FQnC8FkrDcXHWXEiMNNRWRR9uXg+lnzc19oJ/vKpB3HYd2LTRGu64REJ7W//sO3ddH8KiRcZDLlqJW20xs66luQ/5dv+YeyQ/vyBj//79/49dDofDGQr+gYFHHg/YxCKiGzTP3iVuvg4dUw8UZI/ob5CXA6Kvcxfmz9MCu0sBUD2gtBWmurKy8KEprBVYWO3TjNa0l9OFn35wge4BkYM9+PgE8F6ewxkumzdv/o+srGx/pqN+tLS0wdmfXWnJaawu01eUvaJ3FIk+OiodMjMK6X/3BTPffLHfnpaxluQ1DDSc8Z8sqwOnT96CVnIOQmRmZvuuWrXqv9hlcDic4WBrazutsrKylOmpHxiPb3PBnya46Nn80iPMN4kwr14JYp8cwOMucLoeBu+sMiMCHnrBSlyPV1DUpTP8HW3CGb2qqmrrLl+7NoudPofDGQmeng/er6tvkFivyt3tIcybb0Sj9VCYWIN+00YLwVz0WRnFcPJ7J+ohDDWkF18Kk+X0yZjfCK7YYsCPeJQf0tLSAv7+oZ+x0+ZwOKMhKDT8q2YiKkkE+CfDfCL8ZcuMaXHK2ur+0XEtLa1wmYzpcfOMzBAz56LYMYMO1srDZbnEBLFS+v2IjY03ZKfL4XCkQdTDh8c6OjqamcbEiAhLp+m0BhIanAJ7dl+m4h1sqa/HcHIQxa6orEPLWN9wDIF2Ce480t7R0Z2ennGVnOK/i86Uw+FIjcDgUGvBCX0BigsrQEfrLsyabUBEbDDo2B0j9TDmHiP0Zs7CLbl24OkRA/V1Dexokmlra+/29Pbhs/UcjjTZvn3//wuPjP6huKREvAD8ADCBpYN9MKxejRN1ktfz0VDsOAkoq6AHCxcawonvb0JQYPJTs+gMpKGhsSs6OsZYfgHfO8/hjJrrzs7v5OblRTN9DcJjiAxPh/17r1BXHtfdJe2DxzE9FsmQV9CFd94xA0P9e5CRjst64pN0OGQ48d0N8hlPwRJWfcnPL7hnZWX1Cjt1DoczTH4d9TD2DG5cYZqSSF5uKaj95EJdeYyrF3Ll++axx4q2H247T6viFhcJV6opLqoAczNvWL7clHgMenRV4MjBa5CfW8Y+IUxBQWGKk5PTAnYNHA5nKBw5cuRPqanpN5iOJNLS3AzXHUJJb21Ot7gK7cDD3h63z2KN+jlz9OHoN9fhvsdDaGoSnxNEtz4iLBW0NO7CihW4MUevNzQXt+HizP+6tZbg7yu+q64vVTW1hW73vFazy+FwOIPx3Xff/Xd2ds4dph+JJMTlwO7PbGm1WlxzH5jlBjfOyBIXHiPuVq48B5rqrhBPvgOPxcfrDfUN4O4WQfPdT5uOabJEJbMGDg/wN3DTzqzZ+jR8d2AF3b7U1NQ2Y5JPdlkcDkeIZcu2/D4nJ+82040gNdX1tBDFvPksrXUfVx5FirvfULRTpurB9o8uwDWHECjIEwrq64KszGJaoebDbVbw42kH2LPnCnHjtemS3WCz/Rj6i/MGWP2mMF9yIcvGxqZGb98AXtOOw5HAvyUmJ19gehEkOioTtm45T8TeP+ZeNDEnKkW1eLERnD7pDA+8YmkhyYE0NTaDj1ccHD92g0bZYSw9pssK8E+kIbpurtGwc4cNqEzRo0t9kgJ5aPAOcf/XrjGHQLpNV5j6+oZG34BgLnwOZyD+gYFfd3cLh7hibfrz1r4wd54hyBAhYo8u6tVF5ahUVHVIb30BzE29IDcHe3Xx42RlFoGl+QPYstmaTuTheB2X8zDb7aJFJpCaUsA+KQqpxf31n+y8RMSvQ8RtQH9LyN1Hr2D2HAO4cjmYfFM4iKCurr45ICCYu/ocTg+Oji6KFRWV4iViCA31jXDs2xt0eQ3H2biXHXt1WeKGv/32OTh21BH8fBLo54SIjEiDo984wMJFxBNQxGIVRv16b4zXf3+dGXS0ixeobG5uBt8HCXDksAMsXoLDBvICICLHQB7s6XteAnheSsp6YKDngbOB7Nv9aWhsaiJjfN7jczhIQkKyM9NGP6qr6sg4+zK8Non0zERsk0nvPGuWPp1ws78cSHPVS+pdezhn5Al/+/vPdKw/sKdGw0lAXL4bfA3+MV3Ht7Pxp5OHy5YagZKKPnXvcW89HkNWwQhee0MbfvrJFVpbhY/V0NDAx/gcjoeH1wdCm2lqaxqJe20L//uKOnXHP/jACnS03CA6MgNaB9l8MxBbIlTMciMk+B7R79xxUbAUtRDtba1QWlwFzk4RoKdzF06duAkfbDwPby83Jd6EMcycqQXm5+5BZ6dwVF9lVXXt3buei9nlczgTi3/84x//kZGRKZYEE3PVf3/cERYt1Ifvv7sJPt7xUFlZw/51eDg6BNOZdkmz8ZgS+8v9l6G7a3iht32prqqFvNwSyMoohPTUAkhLLYSODsnHK6+oKvHweLCI3QYOZ+Jw69at9bjtdSBNTa00gKa0RHCYPyzuuESAsqrkZBko+gNf2Q873n60lJeXF128eFGF3QoOZ0Lwm5SUNDemgVFTXFLaVF1TI/YG8fGJI669ocQst7jW//33TuST4hNw9fVP32U3GsrKyuItLS/JsvvB4bzcWF64MKempnY45WTFqKmtrU9OSfEPDg09rKVvMjMxKVmU97oPsbHZMG++scQiGSj6k6dwHlFc9P7+gepBQUFfxscnBlRVVVdh7n1pU1FZlfvgQcBsdls4nJeXqKiH51i7HxZlZeUtWdm53gEBwd+anT8/jR2Okpya5s4+1gsmsFy3xpQuzQ0UPE7uycnrgomJN/lk/1WArq4u4iX4b2CH/pWmpr7crVt3PomNjdfOzMoJzs0tyC0sLOro6Bj6ewvr4hcUFnXm5xdU5+XlZ2dn50ZmZWUH3bvv/TX7GQ7n5eS0uvo/iotLB9+u1oey8vLGtIxMH7+AoO/t7OyUySF+KzpSfxITkxzYV3rBpbitEireqk43BXkFTfC+H8c+/QTckHP3rud6duiB/OehQ6f+187OcfLt224b3D08DgYEhJwm53cqODj858jIGFP80z8w8KR/YPAP3t4+R+7cubvbxcVtlY2NwyRtbZN/Hjp06H9/9as/YxlrvBZe2ZbzcoO9NNOWRGrr6uqSU1L9QkLCj5qaWs1gXx2UiIgosco4WOZ696eXaFCOZNGLSmD1hQwd2i5fdljKDs3hcEbKii1b/piXXyBeC4pAevRm6roHB3+rb2ExnX1lyGDm3PaBQTaPH8HBr6/SSLqBosfoPtUpOhDoL75VtrqmpvbMGa1hnwOHwxkA6eV3M11RysorGzMyM318/f1PWNrYKJGP/Eb0yeFjZ3f1LSLWAYPsbvjhBxcaPy8k+qnTdCEkKIV99gllZRXZO3bs4FlwOJzRoKam9nfisue2trXhrLtfSFjYNwMn40bDt9+elKuqrhabK3C+GUHceC3qzvcVPc7o42aZmOhM9sknpKSk+bPDcjickXL+vO0817v31C5fvjZWbvNvsrKzw5huewkOSAEVFW2xbbK4y27eAiNIjM9hn3xCXHziDXZMDoczUn788ccxn6V++DDOjum2F4zXnznbgJa86it6TIW1ZJkpZGWJ17oLC4vSZ4fkcDjjGf/AwGNMt72kp+XDosWi/fd9RS+vZAibNp2n2Xj6gvv6vX19v2SH5HA445kbN1zmVFVV96sh3dzcCjt34LJd/8k8zJx75KDY0j6Ul1d0XbvmNIUdksPhjGf27fvmlcLCYjF//afTTlTkfUUvq6APp0/cZJ94QlFRcfmJE2pvsENyOJxxzr+lpKVdY/rtBQtaoMh79tXjn/j/2lrie35y8wpC/va3v/Ea8xzOi4LXgwffMP32Eh6aBtOnP6lYi3nsFZT0aQmsgUQ9fGjADsXhcF4ELCwsVMi4vl/SvMqKGpr/XkFFtNsOA3PmztGDzHTx8ngeHl772KE4HM6LAgbXMA1THj3qhCOHHWlOfBQ97rp7d7UZVFf1z8hTWVXVZG5+gU/icTgvGsHBoYcHlrXG1FlY8Qaz6GBNux9Ou9CXQV/S0jMjyNf5rjcO50VDQ0NfobKyqpZpmVKQXw7Ll+P6vDHIK+rC7Vvh7F+eEB0d8zM7BIfDedGIjY23Z1ruRf2sK/z175qw6h1TmmK7Lw0Nje22trYL2dc5HM6LhrPznSX19Q39clzl5ZTB5k1mcNk2kP3NE7KyssOXLFnyO/Z1DofzIpKQkOzCNN2LUGGLx48fY3osnraKw3nRwR191dU14sXoB5BfWJh9+NSpv7KvcTicF5nAsIiTTNuCYHJLX3//z9jHORzOS8BvMzOzrzONi5GYlOJAPsOX6Ticl4kdO479V0pK6qXGxieBeu0dHZCekXXjxAntP7KPcTiclw1X13tvR0ZGm4WGhl3y8PDazP6aMyH41a/+PwrNCx2bTGXNAAAAAElFTkSuQmCC";

        Location location = new Location();
        location.setAddress("casablanca, morocco");
        // @33.5416166,-7.6279767,11.54z
        location.setLongitude(-7.6279767);
        location.setLatitude(33.5416166);
        location.setZoomLevel(11.54);
        MedicalOfficeInfos infos
                = new MedicalOfficeInfos(
                null, "medical office name",
                Doctor.builder()
                        .name("Dr Ayoub Anbara")
                        .specialty("Allergists/Immunologists")
                        .build(),

                "+212 6-26-08-83-91", logo, location


        );

        this.infosMedOfficeRepo.save(infos);
    }

    @Override
    public void initOthers() {


        for (long i = 1; i <= 8; i++) {
            Optional<Patient> optional = patientRepository.findById(i);
            if (optional.isPresent()) {
                Patient p = optional.get();
                // first appointment
                PatientAppointment personal = new PatientAppointment();
                personal.setDate(today); //LocalDateTime.now().plusDays(14)
                personal.setTime(LocalTime.now());
                personal.setStatus(AppointmentStatus.Pending);

                personal.setPatient(p);

                this.appointmentPatientRepo.save(personal);

                // second appointment
                personal = new PatientAppointment();
                personal.setDate(today.plusDays(12));
                personal.setStatus(AppointmentStatus.Done);
                personal.setPatient(p);

                this.appointmentPatientRepo.save(personal);
            }
        }
        for (long i = 1; i <= 8; i++) {
            Optional<Patient> optional = patientRepository.findById(i);
            if (optional.isPresent()) {
                Patient p = optional.get();
                // first appointment
                PatientAppointment personal = new PatientAppointment();
                personal.setDate(today.plusDays(i)); //LocalDateTime.now().plusDays(14)
                personal.setTime(LocalTime.now());
                personal.setStatus(AppointmentStatus.Pending);

                personal.setPatient(p);

                this.appointmentPatientRepo.save(personal);

                // second appointment
                personal = new PatientAppointment();
                personal.setDate(today.plusDays(12));
                personal.setStatus(AppointmentStatus.Done);
                personal.setPatient(p);

                this.appointmentPatientRepo.save(personal);
            }
        }

        initPrescriptions();
        initCertificates();
    }

    @Override
    public void initMedications() {
        Stream.of(
                new Medication(null,
                        "medicament 1"),
                new Medication(null,
                        "medicament 2"),
                new Medication(null,
                        "medicament 3")
        ).forEach(medicationRepo::save);
    }


    public void initPrescriptions() {
        // get consulation then add
        Optional<Consultation> optional = consultationRepository.findById(1L);
        if (optional.isPresent()) {
            Consultation consultation = optional.get();

            //  Prescription prescription = consultation.getPrescription();
            //  Map<String, String> map = prescription.getMedicamentMap();
            //  prescription.setMedicamentMap(map);
            Prescription prescription = new Prescription(LocalDate.now(), new HashMap<String, String>() {
                {
                    put("medicament AAA", "2 every day at 17:00");
                    put("medicament BBB", "after launch");
                }
            });
            consultation.setPrescription(prescription);
            consultationRepository.save(consultation);

        }
    }

    public void initCertificates() {
// get consulation then add
        Optional<Consultation> optional = consultationRepository.findById(1L);
        if (optional.isPresent()) {
            Consultation consultation = optional.get();
            //  certificateType, commentCertificate
            Certificate certificate = new Certificate(Certificate.CertificateType.Medical
                    , "comment Certificate....");

            consultation.setCertificate(certificate);
            consultationRepository.save(consultation);
//            certificateRepository.save(certificate);

        }
    }


    /*public void initMedicalExams() {
        // get consulation then add
        Optional<Consultation> optional = consultationRepository.findById(1L);
        if (optional.isPresent()) {
            Consultation consultation = optional.get();
            // firstName,  lastName,  commentCertificate
            MedicalExam exam = new MedicalExam("description of exam ...");
            consultation.setMedicalExams(exam);
            consultationRepository.save(consultation);
        }
    }*/
}
