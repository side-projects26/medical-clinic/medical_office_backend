package com.anbara.medicaloffice;

import com.anbara.medicaloffice.services.DatabaseManagementService;
import com.anbara.medicaloffice.init.IMedicalInitService;
import com.anbara.medicaloffice.init.IMedicalUpdateService;
import com.anbara.medicaloffice.services.storage.StorageProperties;
import com.anbara.medicaloffice.services.storage.StorageService;
import com.anbara.medicaloffice.utils.AppConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
@Slf4j
public class MedicalofficeApplication
        // extends SpringBootServletInitializer // 1
        implements CommandLineRunner {
    @Autowired
    private IMedicalInitService initService;
    @Autowired
    private IMedicalUpdateService updateService;
    @Autowired
    private StorageService storageService;

    @Value("${generateFakeData}")
    private String generateFakeData;
//    @Value("${timeZone}")
//    private String timeZone;


//    public MedicalofficeApplication(IMedOfficeInitService initService, IMedOfficeUpdateService updateService,
//                                    StorageService storageService) {
//        this.initService = initService;
//        this.updateService = updateService;
//        this.storageService = storageService;
//
//    }

//    @PostConstruct
//    public void init(){
     //   log.info("Spring Boot TimeZone: "+timeZone);
        // Setting Spring Boot SetTimeZone
      //  TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
//    }
    public MedicalofficeApplication() {
    }

    public static void main(String[] args) {
        SpringApplication.run(MedicalofficeApplication.class, args);

    }

    @PostConstruct
    public void setUp() {
        objectMapper().registerModule(new JavaTimeModule());
    }

    @Override
    public void run(String... args) throws Exception {
        if (Boolean.parseBoolean(generateFakeData)) {
            initService.initPatients();
            initService.initAppointmentsPatient();
            initService.initConsultations();
            initService.initOthers();

            updateService.updatePatients();
            updateService.updateAppointmentsPatient();

            initService.initMedications();
        }
        initService.initUsers();
        initService.initInfos();

        // create folder that contains uploaded files
        storageService.createDirectories(AppConstants.PARENT_FOLDERS_APPLICATION,AppConstants.CHILDREN_FOLDERS);


    }

    @Autowired
    DatabaseManagementService databaseManagementService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedMethods("*")
                        .allowedOrigins("*");
            }
        };
    }


//    @Override //2
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
//        return builder.sources(MedicalofficeApplication.class);
//    }

}
