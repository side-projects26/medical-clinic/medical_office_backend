package com.anbara.medicaloffice.services;

import com.smattme.MysqlExportService;
import com.smattme.MysqlImportService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Properties;

import static com.anbara.medicaloffice.utils.AppConstants.NAME_FOLDER_BACKUP_DB;
import static com.anbara.medicaloffice.utils.AppConstants.PARENT_FOLDERS_APPLICATION;

@Service
public class DatabaseManagementService {

    @Value("${spring.datasource.username}")
    private String DB_USERNAME;
    @Value("${spring.datasource.password}")
    private String DB_PASSWORD;
//    @Value("${databaseName}")
//    private String DB_NAME;
    @Value("${spring.datasource.url}")
    private String springDatasourceUrl;

    private final String LOCATION_BACKUP_GENERATED = System.getProperty("user.home")+File.separator + PARENT_FOLDERS_APPLICATION + File.separator + NAME_FOLDER_BACKUP_DB;

    public Path exportDB() throws SQLException, IOException, ClassNotFoundException {
       //required properties for exporting of db
        Properties properties = new Properties();
     //   properties.setProperty(MysqlExportService.DB_NAME, DB_NAME);
        properties.setProperty(MysqlExportService.DB_USERNAME, DB_USERNAME);
        properties.setProperty(MysqlExportService.DB_PASSWORD, DB_PASSWORD);

        //set the outputs temp dir
        properties.setProperty(MysqlExportService.TEMP_DIR, new File(LOCATION_BACKUP_GENERATED).getPath());

        properties.setProperty(MysqlExportService.JDBC_CONNECTION_STRING, springDatasourceUrl);
        //  properties.setProperty(MysqlExportService.PRESERVE_GENERATED_SQL_FILE,"true");
        properties.setProperty(MysqlExportService.PRESERVE_GENERATED_ZIP, "true");

        MysqlExportService mysqlExportService = new MysqlExportService(properties);

        mysqlExportService.export();
        return mysqlExportService.getGeneratedZipFile().toPath();

    }

    public boolean importDB(String pathFileSql) throws IOException, SQLException, ClassNotFoundException {
        String sql = new String(Files.readAllBytes(Paths.get(pathFileSql)));

        boolean res = MysqlImportService.builder()
                .setJdbcConnString(springDatasourceUrl)
                .setSqlString(sql)
                .setUsername(DB_USERNAME)
                .setPassword(DB_PASSWORD)
                .setDeleteExisting(true)
                .setDropExisting(true)
                .importDatabase();

        return res;
    }
}
