package com.anbara.medicaloffice.services;

import com.anbara.medicaloffice.dto.*;
import com.anbara.medicaloffice.entities.AppRole;
import com.anbara.medicaloffice.entities.AppUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface UserService {
    UserDto saveUser(UserRequest userRequest);

    AppRoleDto saveRole(AppRoleDto roleDto);

    void addRoleToUser(String username, String roleName);

    AppUser findUserByUsername(String username);

    PageDto<UserDto> findAllUser(int page, int limit, String keyword);

    void deleteUserById(Long id);

    UserDto findById(Long id);

    UserDto updateUser(UserRequest userRequest, Long id);

    boolean isUserAlreadyExists(String username);

    boolean isRoleAlreadyExists(String roleName);


    long numberCurrentUser();

    void changePassword(String username, String password, String confirmPassword);

    void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
