package com.anbara.medicaloffice.services;

import com.anbara.medicaloffice.entities.Certificate;
import com.anbara.medicaloffice.entities.Prescription;
import org.springframework.http.ResponseEntity;

public interface PrescriptionService {
    Prescription add(Long idConsultation, Prescription prescription);

    Prescription getOne(Long consultationId);

    Prescription update(Prescription newPrescription, Long id);

    void delete(Long id);
}
