package com.anbara.medicaloffice.services;

import com.anbara.medicaloffice.dto.VisitorAppointmentDto;
import org.springframework.data.domain.Page;

public interface VisitorAppointmentService {
    Page<VisitorAppointmentDto> getAll(int page, int limit, String keyword);

    VisitorAppointmentDto newAppointment(VisitorAppointmentDto visitorAppointmentDto);

    VisitorAppointmentDto getOne(Long id);

    VisitorAppointmentDto update(VisitorAppointmentDto visitorAppointmentDto, Long id);

    void deleteOne(Long id);

    void deleteMultiRows(Long[] ids);
}
