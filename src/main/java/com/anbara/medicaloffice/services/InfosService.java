package com.anbara.medicaloffice.services;

import com.anbara.medicaloffice.dto.MedicalOfficeInfosDto;
import org.springframework.http.ResponseEntity;


public interface InfosService {
    MedicalOfficeInfosDto getInfos();

 MedicalOfficeInfosDto updateInfos(MedicalOfficeInfosDto InfosDto);
}
