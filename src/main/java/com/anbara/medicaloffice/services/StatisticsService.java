package com.anbara.medicaloffice.services;

import com.anbara.medicaloffice.dto.StatisticResponse;
import com.anbara.medicaloffice.utils.Helpers;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class StatisticsService {
    private final JdbcTemplate jdbcTemplate;
    @Value("${generateFakeStatistics}")
    private String generateFakeStatistics;
    private final long[] fakeData = {2, 2, 3, 2, 3, 3, 2, 1, 3, 4, 4, 8};


    public StatisticsService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public StatisticResponse getStatistics() {

        final StatisticResponse statisticResponse = new StatisticResponse();

        calculateGenderStatistics(statisticResponse);
        calculateNewPatientsStatistics(statisticResponse);
        calculateAppointmentsTodayStatistics(statisticResponse);
        calculateConsultationStatistics(statisticResponse);


        return statisticResponse;
    }

    private void calculateConsultationStatistics(StatisticResponse statisticResponse) {
        List<StatisticResponse.DataConsultation> dataConsultationList = new ArrayList<>();

        long[] data = new long[12];
        int year = LocalDate.now().getYear();
        String query;
        for (int i = 0; i < 2; i++) {
            StatisticResponse.DataConsultation dataConsultation = new StatisticResponse.DataConsultation();
            dataConsultation.setYear(year);
            if (Boolean.parseBoolean(generateFakeStatistics)) {
                dataConsultation.setData(fakeData);
            } else {
                for (int j = 1; j <= 12; j++) {
                    query = "SELECT COUNT(*) FROM consultation cons WHERE YEAR(cons.date)='" + year + "' and MONTH(cons.date)='" + j + "'";
                    Long numberConsultation = jdbcTemplate.queryForObject(query, Long.class);
                    data[j - 1] = numberConsultation;

                }
                dataConsultation.setData(data);
            }


            dataConsultationList.add(dataConsultation);
            data = new long[12];
            year--;
        }

        statisticResponse.setDataConsultation(dataConsultationList);
    }

    private void calculateAppointmentsTodayStatistics(StatisticResponse statisticResponse) {
        final String query_appointmentsToday = "SELECT (SELECT COUNT(*) FROM visitor_appointment app WHERE app.date=CURDATE()) AS countNew,(   SELECT COUNT(*) FROM patient_appointment  WHERE patient_appointment.date=CURDATE()) AS countOld FROM dual";

        jdbcTemplate.queryForObject(query_appointmentsToday, null, (resultSet, i) -> {
            long countNew = resultSet.getLong("countNew");
            long countOld = resultSet.getLong("countOld");
            statisticResponse.setAppointmentsToday(countNew + countOld);
            return null;
        });
    }

    private void calculateGenderStatistics(StatisticResponse statisticResponse) {
        Long malesNumber = jdbcTemplate.queryForObject("SELECT COUNT(gender) FROM patient WHERE patient.gender=\'Male\'", Long.class);
        Long femalesNumber = jdbcTemplate.queryForObject("SELECT COUNT(gender) FROM patient WHERE patient.gender=\'Female\'", Long.class);


        float total = malesNumber + femalesNumber;
        BigDecimal malePercentage = Helpers.calculatePercentage(total, malesNumber);
        BigDecimal femalePercentage = Helpers.calculatePercentage(total, femalesNumber);


        statisticResponse.setMalePercentage(malePercentage);
        statisticResponse.setFemalePercentage(femalePercentage);
    }

    private void calculateNewPatientsStatistics(StatisticResponse statisticResponse) {
        Long newPatients = jdbcTemplate.queryForObject("SELECT COUNT(date_creation) FROM patient WHERE date(patient.date_creation)= CURDATE()", Long.class);
        statisticResponse.setNewPatients(newPatients);
    }


}
