package com.anbara.medicaloffice.services;

import com.anbara.medicaloffice.dto.ConsultationDto;
import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.entities.Consultation;

import java.time.LocalDate;


public interface ConsultationService {

     PageDto<ConsultationDto> allByPatientIdAndDate(Long idPatient, int page, int limit, LocalDate dateConsultation);
     PageDto<ConsultationDto> allByPatientId(Long idPatient, int page, int limit);

     ConsultationDto newConsultation( Long idPatient,  ConsultationDto consultationDto);

     ConsultationDto update(Long idPatient, ConsultationDto consultationDto) ;

     void deleteConsultation( Long id);

}
