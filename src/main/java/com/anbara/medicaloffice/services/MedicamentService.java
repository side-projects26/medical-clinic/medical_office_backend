package com.anbara.medicaloffice.services;

import com.anbara.medicaloffice.dto.MedicationDto;
import com.anbara.medicaloffice.dto.PageDto;

public interface MedicamentService {
    // Aggregate root
    PageDto<MedicationDto> getAll(int page, int limit, String keyword);

    MedicationDto add(MedicationDto newMedicationDto);

    MedicationDto getOne(Long id);

    MedicationDto update(MedicationDto newMedicationDto, Long id);

    void delete(Long id);

    void deleteMultiRows(Long[] ids);
}
