package com.anbara.medicaloffice.services.auth;

import com.anbara.medicaloffice.entities.AppUser;
import com.anbara.medicaloffice.services.UserService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserService userService;

    public UserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser appUser=userService.findUserByUsername(username);
        Collection<GrantedAuthority> authorities=new ArrayList<>();
        appUser.getRoles().forEach(
                r->authorities.add(new SimpleGrantedAuthority(r.getRoleName()))
        );
        return new User(appUser.getUsername(),appUser.getPassword(),authorities);
    }
}
