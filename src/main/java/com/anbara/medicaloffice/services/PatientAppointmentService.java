package com.anbara.medicaloffice.services;

import com.anbara.medicaloffice.dto.PatientAppointmentDto;
import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.dto.ResponseAllAppointmentsPendings;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface PatientAppointmentService {

     PageDto<PatientAppointmentDto> getAllByPatientId(Long patientId, int page, int limit, String keyword);


     PatientAppointmentDto newAppointment(Long patientId, PatientAppointmentDto appointmentDto) ;


     PatientAppointmentDto update(PatientAppointmentDto appointmentDto , Long id) ;

     void delete(Long id) ;

     List<ResponseAllAppointmentsPendings> getAllAppointmentsPending();
     PageDto<ResponseAllAppointmentsPendings> allPendingByPage(Pageable pageable, String date) ;


}



