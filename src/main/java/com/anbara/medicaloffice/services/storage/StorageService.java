package com.anbara.medicaloffice.services.storage;

import org.springframework.web.multipart.MultipartFile;
public interface StorageService {
    void createDirectories(String parent, String[] children);
    String store(MultipartFile file);
     String storeBackupDB(MultipartFile file);
}
