package com.anbara.medicaloffice.services.storage;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Locale;
import java.util.Optional;

import static com.anbara.medicaloffice.utils.AppConstants.*;

@Service
@Slf4j
public class FileSystemStorageService implements StorageService {

    private final Path rootLocation;

    @Autowired
    public FileSystemStorageService(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
        System.err.println(this.rootLocation.getRoot());
    }

    @Override
    public String store(MultipartFile file) {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new StorageException(
                        "Cannot store file with relative path outside current directory "
                                + filename);
            }
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, this.rootLocation.resolve(filename),
                        StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + filename, e);
        }
        return this.rootLocation + File.separator + filename;
    }

    private Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String storeBackupDB(MultipartFile file) {
        if (file == null) {
            // todo
            throw new RuntimeException("file must be not null");

        }
        String filename = StringUtils.cleanPath(file.getOriginalFilename());

        // Optional<String> optionalExtension = getExtensionByStringHandling(filename);
        //  if (!optionalExtension.isPresent() || !optionalExtension.get().equalsIgnoreCase("sql")) {
        if (!filename.toLowerCase().endsWith(".sql")) {
            // todo
            throw new RuntimeException("Extension file should be .sql ");
        }


//        Optional<Consultation> optional = consultationRepository.findById(cons_id);
//        if (optional.isPresent()) {
//            Consultation consultation = optional.get();
//
//            FilesConsultation filesConsultation = new FilesConsultation();
//            filename=filename.replaceAll(" ","");
//            filesConsultation.setNameFile(filename);
//            consultation.getFilesConsultations().add(filesConsultation);
//            consultationRepository.save(consultation);
//
//
//        }else {
//            throw new RuntimeException("consultation with id="+cons_id+" not found");
//        }
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new StorageException(
                        "Cannot store file with relative path outside current directory "
                                + filename);
            }

            try (InputStream inputStream = file.getInputStream()) {
                final String PWD = System.getProperty("user.home") + File.separator + PARENT_FOLDERS_APPLICATION + File.separator + NAME_UPLOAD_BACKUP_DB;
                Path p1 = Paths.get(PWD);
                if (!Files.exists(p1)) {
                    p1 = Files.createDirectory(p1);
                }

                //String uuid = UUID.randomUUID().toString();


                Files.copy(inputStream, p1.resolve(filename),
                        StandardCopyOption.REPLACE_EXISTING);
                return PWD + File.separator + filename;
            }
        } catch (
                IOException e) {
            throw new StorageException("Failed to store file " + filename, e);
        }

    }

    @Override
    public void createDirectories(String parent, String[] children) {
        String root = System.getProperty("user.home") + File.separator + parent;
        if (!Files.exists(Paths.get(root))) {
            try {
                Files.createDirectories(Paths.get(root));
                for (String child : children) {
                    if (!Files.exists(Paths.get(root + File.separator + child))) {
                        Files.createDirectories(Paths.get(root + File.separator + child));
                    }
                }
            } catch (IOException e) {
                throw new StorageException("Could not initialize storage", e);
            }
        }
    }
}
