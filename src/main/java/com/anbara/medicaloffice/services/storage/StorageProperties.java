package com.anbara.medicaloffice.services.storage;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.File;

import static com.anbara.medicaloffice.utils.AppConstants.NAME_FOLDER_UPLOAD_FILES;

@ConfigurationProperties("storage")
public class StorageProperties {
    /**
     * Folder location for storing files
     */
//    private String location ="/home/hp/uploadFile" ;
    private String location =System.getProperty("user.home")  + File.separator+  NAME_FOLDER_UPLOAD_FILES;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
