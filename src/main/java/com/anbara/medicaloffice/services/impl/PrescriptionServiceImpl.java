package com.anbara.medicaloffice.services.impl;

import com.anbara.medicaloffice.dao.ConsultationRepository;
import com.anbara.medicaloffice.dao.PrescriptionRepo;
import com.anbara.medicaloffice.entities.Consultation;
import com.anbara.medicaloffice.entities.Prescription;
import com.anbara.medicaloffice.exceptions.EntityNotFoundException;
import com.anbara.medicaloffice.exceptions.ErrorCodes;
import com.anbara.medicaloffice.exceptions.InvalidEntityException;
import com.anbara.medicaloffice.services.PrescriptionService;
import com.anbara.medicaloffice.validators.PrescriptionValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class PrescriptionServiceImpl implements PrescriptionService {
    private final ConsultationRepository consultationRepo;
    private final PrescriptionRepo prescriptionRepo;

    public PrescriptionServiceImpl(ConsultationRepository consultationRepo, PrescriptionRepo prescriptionRepo) {
        this.consultationRepo = consultationRepo;
        this.prescriptionRepo = prescriptionRepo;
    }


    @Override
    public Prescription add(Long idConsultation, Prescription prescription) {

        if (idConsultation == null) {
            //log.error("id is null");
            return null; // TODO
        }
        Consultation consultation = consultationRepo.findById(idConsultation)
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                "entity with id=" + idConsultation + " not found",
                                ErrorCodes.CONSULTATION_NOT_FOUND
                        )
                );
        List<String> errors = PrescriptionValidator.validate(prescription);
        if (!errors.isEmpty()) {
            throw new InvalidEntityException("entity is invalid", ErrorCodes.PRESCRIPTION_INVALID, errors);
        }
        /*Prescription prescription0 = new Prescription(LocalDate.now(), new HashMap<String, String>() {
            {
                put("medicament AAA", "2 every day at 17:00");
                put("medicament BBB", "after launch");
            }
        });*/
        consultation.setPrescription(prescription);
        Consultation consultationUpdated = consultationRepo.save(consultation);
        return consultationUpdated.getPrescription();

    }

    @Override
    public Prescription getOne(Long consultationId) {
        if (consultationId == null) {
            //log.error("id is null");
            return null; // TODO
        }
        Consultation consultation = consultationRepo.findById(consultationId)
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                "entity with id=" + consultationId + " not found",
                                ErrorCodes.CONSULTATION_NOT_FOUND
                        )
                );

        Prescription prescription = consultation.getPrescription();
        if (prescription == null) {
            throw new EntityNotFoundException(
                    "prescription with id consultation=" + consultationId + " not found",
                    ErrorCodes.PRESCRIPTION_NOT_FOUND
            );
        }
        return prescription;
    }

    @Override
    public Prescription update(Prescription newPrescription, Long consultationId) {
        // get consulation then add
        Optional<Consultation> optional = consultationRepo.findById(consultationId);
        if (optional.isPresent()) {
            Consultation consultation = optional.get();
            consultation.setPrescription(newPrescription);
            Consultation consultationUpdated = consultationRepo.save(consultation);
            return consultationUpdated.getPrescription();
        } else {
            throw new EntityNotFoundException(
                    "prescription with id consultation=" + consultationId + " not found",
                    ErrorCodes.PRESCRIPTION_NOT_FOUND
            );
        }
    }

    @Override
    public void delete(Long id) {
        if (prescriptionRepo.existsById(id)) {
            prescriptionRepo.deleteById(id);

        } else {
            // todo
        }
    }
}
