package com.anbara.medicaloffice.services.impl;

import com.anbara.medicaloffice.dao.ConsultationRepository;
import com.anbara.medicaloffice.dao.PatientRepository;
import com.anbara.medicaloffice.dto.ConsultationDto;
import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.entities.Consultation;
import com.anbara.medicaloffice.entities.Patient;
import com.anbara.medicaloffice.entities.Payment;
import com.anbara.medicaloffice.enums.PaymentStatus;
import com.anbara.medicaloffice.exceptions.EntityNotFoundException;
import com.anbara.medicaloffice.exceptions.ErrorCodes;
import com.anbara.medicaloffice.exceptions.InvalidEntityException;
import com.anbara.medicaloffice.services.ConsultationService;
import com.anbara.medicaloffice.validators.ConsultationValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.anbara.medicaloffice.dto.ConsultationDto.fromEntity;
import static com.anbara.medicaloffice.dto.ConsultationDto.toEntity;

@Service
@Slf4j
public class ConsultationServiceImpl implements ConsultationService {
    private final ConsultationRepository consultationRepository;
    private final PatientRepository patientRepository;

    public ConsultationServiceImpl(ConsultationRepository consultationRepository, PatientRepository patientRepository) {
        this.consultationRepository = consultationRepository;
        this.patientRepository = patientRepository;
    }

//    @GetMapping("/consultations")
//    Iterable<Consultation> all() {
//        return consultationRepository.findAll();
//    }

  /*  @GetMapping("/{idPatient}")
    public List<Consultation> allByPatientId(@PathVariable Long idPatient) {
//        return consultationRepository.findAllByPatientId(idPatient);
        Patient patient = patientRepository.findById(idPatient)
                .orElseThrow(() -> new PatientNotFoundException(idPatient));
        return consultationRepository.findAllByPatient(patient);

    }*/

    @Override
    public PageDto<ConsultationDto> allByPatientIdAndDate(Long idPatient, int page, int limit, LocalDate dateConsultation) {

        Patient patient = patientRepository.findById(idPatient)
                .orElseThrow(() -> new RuntimeException("idPatient"));
        Page<Consultation> consultationPage = consultationRepository.findAllByPatientAndDateOrderByDateDesc(patient, dateConsultation, PageRequest.of(page, limit));
        return PageDto.fromPage(consultationPage.map(ConsultationDto::fromEntity));

    }

    @Override
    public PageDto<ConsultationDto> allByPatientId(Long idPatient, int page, int limit) {
        if (idPatient == null) {
            log.error("id is null");
            return null; // TODO
        }
        Patient patient = patientRepository.findById(idPatient)
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                "patient with id=" + idPatient + " not found",
                                ErrorCodes.PATIENT_NOT_FOUND
                        )
                );
        Page<Consultation> consultationPage = consultationRepository.
                findAllByPatientOrderByDateDesc(patient, PageRequest.of(page, limit));

        return PageDto.fromPage(consultationPage.map(ConsultationDto::fromEntity));

    }

    @Override
    public ConsultationDto newConsultation(Long idPatient, ConsultationDto consultationDto) {
        if (idPatient == null) {
            log.error("id is null");
            return null; // TODO
        }
        Patient patient = patientRepository.findById(idPatient)
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                "patient with id=" + idPatient + " not found",
                                ErrorCodes.PATIENT_NOT_FOUND
                        )
                );
        Consultation newConsultation = toEntity(consultationDto);
        newConsultation.setPatient(patient);
        setPayment(newConsultation);
        Consultation consultationAdded = consultationRepository.save(newConsultation);
        return fromEntity(consultationAdded);

    }

    private void setPayment(Consultation consultation) {
        Payment payment = consultation.getPayment();
        if (payment.getTotal().compareTo(payment.getAmountPaid()) == 0) {
            payment.setStatus(PaymentStatus.Complete);
        } else {
            payment.setStatus(PaymentStatus.Incomplete);
        }
        consultation.setPayment(payment);
    }


//    @GetMapping("/consultations/{id}")
//    Consultation one(@PathVariable Long id) {
//        return consultationRepository.findById(id)
//                .orElseThrow(() -> new ConsultationNotFoundException(id));
//    }

    @Override
    public ConsultationDto update(Long idPatient, ConsultationDto consultationDto) {
//         // get Patient then add consultaion

        if (idPatient == null) {
            log.error("id patient is null");
            return null; // TODO
        }
        List<String> errors = ConsultationValidator.validate(consultationDto);
        if (!errors.isEmpty()) {
            throw new InvalidEntityException("entity is invalid", ErrorCodes.CONSULTATION_INVALID, errors);
        }
        Optional<Patient> optionalPatient = patientRepository.findById(idPatient);
        if (!optionalPatient.isPresent()) {
            throw new EntityNotFoundException("patient is not found", ErrorCodes.PATIENT_NOT_FOUND);
        }
        if (!consultationRepository.existsById(consultationDto.getId())) {
            throw new EntityNotFoundException("consultation is not found", ErrorCodes.CONSULTATION_NOT_FOUND);
        }
        Consultation consultation = toEntity(consultationDto);

        Patient patient = optionalPatient.get();
        consultation.setPatient(patient);
        consultation.setReason(consultation.getReason());
        consultation.setDate(consultation.getDate());
        setPayment(consultation);
        Consultation consultationUpdated = consultationRepository.save(consultation);
        return fromEntity(consultationUpdated);



       /* Optional<Consultation> optional = consultationRepository.findById(id);
        if (optional.isPresent()) {

            Consultation consultation=toEntity(consultationDto);
            consultation.setReasonConsultation(consultation.getReasonConsultation());
            consultation.setDate(consultation.getDate());
            Consultation  consultationUpdated = consultationRepository.save(consultation);
            return fromEntity(consultationUpdated);
        } else {
            throw new EntityNotFoundException(
                    "this entity with id=" + id + " not found",
                    ErrorCodes.CONSULTATION_NOT_FOUND
            );
        }*/
    }

    @Override
    public void deleteConsultation(Long id) {
        // todo remove all entities related to this consultation
        if (consultationRepository.existsById(id)) {
            consultationRepository.deleteById(id);
        } else {
            // todo
        }
    }

}
