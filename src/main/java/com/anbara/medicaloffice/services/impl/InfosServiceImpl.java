package com.anbara.medicaloffice.services.impl;

import com.anbara.medicaloffice.dao.InfosMedOfficeRepo;
import com.anbara.medicaloffice.dto.MedicalOfficeInfosDto;

import static com.anbara.medicaloffice.dto.MedicalOfficeInfosDto.fromEntity;
import static com.anbara.medicaloffice.dto.MedicalOfficeInfosDto.toEntity;

import com.anbara.medicaloffice.entities.MedicalOfficeInfos;
import com.anbara.medicaloffice.exceptions.EntityNotFoundException;
import com.anbara.medicaloffice.exceptions.ErrorCodes;
import com.anbara.medicaloffice.exceptions.InvalidEntityException;
import com.anbara.medicaloffice.services.InfosService;
import com.anbara.medicaloffice.validators.MedicalOfficeInfosValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class InfosServiceImpl implements InfosService {
    private final InfosMedOfficeRepo repository;
    final long ID_ENTITY_INFOS = 1L; // because always there is one row in db with id=0

    public InfosServiceImpl(InfosMedOfficeRepo repository) {

        this.repository = repository;
    }

    @Override
    public MedicalOfficeInfosDto getInfos() {
        return repository.findById(ID_ENTITY_INFOS)
                .map(MedicalOfficeInfosDto::fromEntity)
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                "this entity with id=" + ID_ENTITY_INFOS + " not found",
                                ErrorCodes.INFOS_MEDICAL_OFFICE_NOT_FOUND
                        )
                );

    }

    @Override
    public MedicalOfficeInfosDto updateInfos(MedicalOfficeInfosDto infosDto) {
        List<String> errors = MedicalOfficeInfosValidator.validate(infosDto);
        if (!errors.isEmpty()) {
            throw new InvalidEntityException("entity is invalid", ErrorCodes.INFOS_MEDICAL_OFFICE_INVALID, errors);
        }
        Optional<MedicalOfficeInfos> optional = repository.findById(ID_ENTITY_INFOS);
        if (optional.isPresent()) {
            MedicalOfficeInfos oldInfos=optional.get();
            infosDto.setLogoBase64(oldInfos.getLogoBase64());
            infosDto.setId(ID_ENTITY_INFOS);
            MedicalOfficeInfos infos = toEntity(infosDto);
            MedicalOfficeInfos infosUpdated = repository.save(infos);
            return fromEntity(infosUpdated);
        } else {
            throw new EntityNotFoundException(
                    "this entity with id=" + ID_ENTITY_INFOS + " not found",
                    ErrorCodes.INFOS_MEDICAL_OFFICE_NOT_FOUND);
        }



        /*return infosMedOfficeRepo.findById(id)
                .map(inf -> {
                    inf.setMedicalOfficeName(newInfos.getMedicalOfficeName());
                    inf.setPhone(newInfos.getPhone());
                    inf.setDoctorFullName(newInfos.getDoctorFullName());
                    inf.setSpecialtyDoctor(newInfos.getSpecialtyDoctor());

                    inf.setLocationOffice(newInfos.getLocationOffice());

                    return infosMedOfficeRepo.save(inf);
                }).orElseGet(() -> {
                    throw new RuntimeException("infos with id=" + id + " not found");
                });*/
    }
}
