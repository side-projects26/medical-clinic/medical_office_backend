package com.anbara.medicaloffice.services.impl;

import com.anbara.medicaloffice.dao.PatientRepository;
import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.dto.PatientDto;
import com.anbara.medicaloffice.entities.Patient;
import com.anbara.medicaloffice.exceptions.EntityNotFoundException;
import com.anbara.medicaloffice.exceptions.ErrorCodes;
import com.anbara.medicaloffice.exceptions.InvalidEntityException;
import com.anbara.medicaloffice.services.PatientService;
import com.anbara.medicaloffice.validators.PatientValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.anbara.medicaloffice.dto.PatientDto.fromEntity;
import static com.anbara.medicaloffice.dto.PatientDto.toEntity;

@Service
@Slf4j
// todo use @RequiredArgsConstructor
public class PatientServiceImpl implements PatientService {
    private final PatientRepository repository;


    public PatientServiceImpl(PatientRepository repository) {
        this.repository = repository;

    }


    @Override
    public PageDto<PatientDto> all(int page, int limit, String keyword) {
//        Page<Patient> patientPage= patientRepository.findAll(PageRequest.of(page,limit));

//        Page<Patient> patientPage= patientRepository.findAllByLastNameIgnoreCaseContaining(keyword,PageRequest.of(page,limit));
        Page<Patient> patientPage = repository.findAllByLastNameIgnoreCaseContainingOrderByDateCreationAsc(keyword, PageRequest.of(page, limit));

        return PageDto.fromPage(patientPage.map(PatientDto::fromEntity));

    }

    @Override
    public PatientDto newPatient(PatientDto patientDto) {

        List<String> errors = PatientValidator.validate(patientDto);
        if (!errors.isEmpty()) {
            throw new InvalidEntityException("entity is invalid", ErrorCodes.PATIENT_INVALID, errors);
        }
        patientDto.setDateCreation( LocalDateTime.now());
        Patient patientSaved =
                repository.save(PatientDto.toEntity(patientDto));
        return PatientDto.fromEntity(patientSaved);
    }

    @Override
    public PatientDto one(Long id) {
        if (id == null) {
            log.error("id is null");
            return null; // TODO
        }
        return repository.findById(id)
                .map(PatientDto::fromEntity)
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                "this entity with id=" + id + " not found",
                                ErrorCodes.PATIENT_NOT_FOUND
                        )
                );
    }

    @Override
    public PatientDto replacePatient(PatientDto patientDto, Long id) {

        if (id == null) {
            log.error("id is null");
            return null; // TODO
        }
        List<String> errors = PatientValidator.validate(patientDto);
        if (!errors.isEmpty()) {
            throw new InvalidEntityException("entity is invalid", ErrorCodes.PATIENT_INVALID, errors);
        }
        Optional<Patient> optional = repository.findById(id);
        if (optional.isPresent()) {
            Patient oldPatient=optional.get();
            patientDto.setId(id);

            patientDto.setDateCreation(oldPatient.getDateCreation());


            Patient patient = toEntity(patientDto);
            Patient patientUpdated = repository.save(patient);
            return fromEntity(patientUpdated);
        } else {
            throw new EntityNotFoundException(
                    "this entity with id=" + id + " not found",
                    ErrorCodes.MEDICAMENT_NOT_FOUND
            );
        }
    }

    @Override
    public void deletePatient(Long id) {
        // todo remove all entities related to this patient
        if (repository.existsById(id)) {
            repository.deleteById(id);
        }else {
            // todo
        }
    }
}
