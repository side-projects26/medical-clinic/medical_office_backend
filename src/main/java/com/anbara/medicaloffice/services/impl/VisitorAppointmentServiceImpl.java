package com.anbara.medicaloffice.services.impl;

import com.anbara.medicaloffice.dao.AppointmentFirstVisitRepo;
import com.anbara.medicaloffice.dto.VisitorAppointmentDto;
import com.anbara.medicaloffice.entities.VisitorAppointment;
import com.anbara.medicaloffice.exceptions.EntityNotFoundException;
import com.anbara.medicaloffice.exceptions.ErrorCodes;
import com.anbara.medicaloffice.exceptions.InvalidEntityException;
import com.anbara.medicaloffice.services.VisitorAppointmentService;
import com.anbara.medicaloffice.validators.VisitorAppointmentValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.anbara.medicaloffice.dto.VisitorAppointmentDto.fromEntity;
import static com.anbara.medicaloffice.dto.VisitorAppointmentDto.toEntity;


@Service
@Slf4j
public class VisitorAppointmentServiceImpl implements VisitorAppointmentService {
    private final AppointmentFirstVisitRepo repository;


    public VisitorAppointmentServiceImpl(AppointmentFirstVisitRepo repository) {
        this.repository = repository;
    }

    @Override
    public Page<VisitorAppointmentDto> getAll(int page, int limit, String keyword) {

        Page<VisitorAppointment> appFVPage = repository
                .findAllByLastNameIgnoreCaseContainingOrderByDateDesc(keyword, PageRequest.of(page, limit));


        /*PageDto<AppointmentFirstVisit> pageDTO = new PageDto<>();
        pageDTO.setContent(appFVPage.getContent());
        pageDTO.setTotalElements(appFVPage.getTotalElements());
        pageDTO.setNumber(appFVPage.getNumber());
        pageDTO.setSize(appFVPage.getSize());*/

        return appFVPage.map(VisitorAppointmentDto::fromEntity);
    }

    @Override
    public VisitorAppointmentDto newAppointment(VisitorAppointmentDto appointmentFVDto) {
        List<String> errors = VisitorAppointmentValidator.validate(appointmentFVDto);
        if (!errors.isEmpty()) {
            throw new InvalidEntityException("appointment is invalid", ErrorCodes.VISITOR_APPOINTMENT_INVALID, errors);
        }
        VisitorAppointment appointmentFVSaved =
                repository.save(toEntity(appointmentFVDto));
        return fromEntity(appointmentFVSaved);

    }

    @Override
    public VisitorAppointmentDto getOne(Long id) {
        if (id == null) {
            log.error("id is null");
            return null; // TODO
        }
        return repository.findById(id)
                .map(VisitorAppointmentDto::fromEntity)
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                "this entity with id=" + id + " not found",
                                ErrorCodes.VISITOR_APPOINTMENT_NOT_FOUND
                        )
                );
    }

    @Override
    public VisitorAppointmentDto update(VisitorAppointmentDto appointmentFVDto, Long id) {

        if (id == null) {
            log.error("id is null");
            return null; // TODO
        }
        List<String> errors = VisitorAppointmentValidator.validate(appointmentFVDto);
        if (!errors.isEmpty()) {
            throw new InvalidEntityException("entity is invalid", ErrorCodes.VISITOR_APPOINTMENT_INVALID, errors);
        }
        Optional<VisitorAppointment> optional = repository.findById(id);
        if (optional.isPresent()) {
            appointmentFVDto.setId(id);
            VisitorAppointment appointmentFV = toEntity(appointmentFVDto);
            VisitorAppointment appointmentFVUpdated = repository.save(appointmentFV);
            return fromEntity(appointmentFVUpdated);
        } else {
            throw new EntityNotFoundException(
                    "this entity with id=" + id + " not found",
                    ErrorCodes.VISITOR_APPOINTMENT_NOT_FOUND
            );
        }
    }

    @Override
    public void deleteOne(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void deleteMultiRows(Long[] ids) {
        for (long i : ids) {
            repository.deleteById(i);
        }
    }

}
