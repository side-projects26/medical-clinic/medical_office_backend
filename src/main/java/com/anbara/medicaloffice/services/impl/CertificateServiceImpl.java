package com.anbara.medicaloffice.services.impl;

import com.anbara.medicaloffice.dao.CertificateRepo;
import com.anbara.medicaloffice.dao.ConsultationRepository;
import com.anbara.medicaloffice.dao.PrescriptionRepo;
import com.anbara.medicaloffice.entities.Certificate;
import com.anbara.medicaloffice.entities.Consultation;
import com.anbara.medicaloffice.entities.Prescription;
import com.anbara.medicaloffice.exceptions.EntityNotFoundException;
import com.anbara.medicaloffice.exceptions.ErrorCodes;
import com.anbara.medicaloffice.exceptions.InvalidEntityException;
import com.anbara.medicaloffice.services.CertificateService;
import com.anbara.medicaloffice.validators.CertificateValidator;
import com.anbara.medicaloffice.validators.PrescriptionValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class CertificateServiceImpl implements CertificateService {
    private final ConsultationRepository consultationRepo;
    private final CertificateRepo certificateRepo;

    public CertificateServiceImpl(ConsultationRepository consultationRepo, CertificateRepo certificateRepo) {
        this.consultationRepo = consultationRepo;
        this.certificateRepo = certificateRepo;
    }


    @Override
    public Certificate add(Long idConsultation, Certificate certificate) {

        if (idConsultation == null) {
            //log.error("id is null");
            return null; // TODO
        }
        Consultation consultation = consultationRepo.findById(idConsultation)
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                "entity with id=" + idConsultation + " not found",
                                ErrorCodes.CONSULTATION_NOT_FOUND
                        )
                );
        List<String> errors = CertificateValidator.validate(certificate);
        if (!errors.isEmpty()) {
            throw new InvalidEntityException("entity is invalid", ErrorCodes.CERTIFICATE_INVALID, errors);
        }
        /*Prescription prescription0 = new Prescription(LocalDate.now(), new HashMap<String, String>() {
            {
                put("medicament AAA", "2 every day at 17:00");
                put("medicament BBB", "after launch");
            }
        });*/
        consultation.setCertificate(certificate);
        Consultation consultationUpdated = consultationRepo.save(consultation);
        return consultationUpdated.getCertificate();

    }

    @Override
    public Certificate getOne(Long consultationId) {
        if (consultationId == null) {
            //log.error("id is null");
            return null; // TODO
        }
        Consultation consultation = consultationRepo.findById(consultationId)
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                "entity with id=" + consultationId + " not found",
                                ErrorCodes.CONSULTATION_NOT_FOUND
                        )
                );

        Certificate certificate = consultation.getCertificate();
        if (certificate == null) {
            throw new EntityNotFoundException(
                    "certificate with id consultation=" + consultationId + " not found",
                    ErrorCodes.CERTIFICATE_NOT_FOUND
            );
        }
        return certificate;
    }

    @Override
    public Certificate update(Certificate newCertificate, Long consultationId) {
        // get consulation then add
        Optional<Consultation> optional = consultationRepo.findById(consultationId);
        if (optional.isPresent()) {
            Consultation consultation = optional.get();
            consultation.setCertificate(newCertificate);
            Consultation consultationUpdated = consultationRepo.save(consultation);
            return consultationUpdated.getCertificate();
        } else {
            throw new EntityNotFoundException(
                    "certificate with id consultation=" + consultationId + " not found",
                    ErrorCodes.CERTIFICATE_NOT_FOUND
            );
        }
    }

    @Override
    public void delete(Long id) {
        if (certificateRepo.existsById(id)) {
            certificateRepo.deleteById(id);

        } else {
            // todo
        }
    }
}
