package com.anbara.medicaloffice.services.impl;

import com.anbara.medicaloffice.dao.MedicationRepo;
import com.anbara.medicaloffice.dto.MedicationDto;
import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.entities.Medication;
import com.anbara.medicaloffice.exceptions.EntityNotFoundException;
import com.anbara.medicaloffice.exceptions.ErrorCodes;
import com.anbara.medicaloffice.exceptions.InvalidEntityException;
import com.anbara.medicaloffice.services.MedicamentService;
import com.anbara.medicaloffice.validators.MedicationValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.anbara.medicaloffice.dto.MedicationDto.fromEntity;
import static com.anbara.medicaloffice.dto.MedicationDto.toEntity;

@Service
@Slf4j
public class MedicamentServiceImpl implements MedicamentService {
    private final MedicationRepo repository;

    public MedicamentServiceImpl(MedicationRepo medicationRepo) {
        this.repository = medicationRepo;
    }

    // Aggregate root
    @Override
    public PageDto<MedicationDto> getAll(int page, int limit, String keyword) {
        Page<Medication> medicamentPage=
                    repository.findByNameContaining(keyword,PageRequest.of(page, limit));

        
        return PageDto.fromPage(medicamentPage.map(MedicationDto::fromEntity));

    }


    @Override
    public MedicationDto add(MedicationDto medicationDto) {
        List<String> errors = MedicationValidator.validate(medicationDto);
        if (!errors.isEmpty()) {
            throw new InvalidEntityException("medicament is invalid", ErrorCodes.MEDICAMENT_IS_INVALID, errors);
        }
        Medication medicationSaved =
                repository.save(MedicationDto.toEntity(medicationDto));
        return MedicationDto.fromEntity(medicationSaved);
    }

    @Override
    public MedicationDto getOne(Long id) {
        if (id == null) {
            log.error("id is null");
            return null; // TODO
        }
        /*
        Optional<Announcement> optional= repository.findById(id);
        return
                Optional.of(fromEntity(optional.get()))
                        .orElseThrow(() -> new EntityNotFoundException("this entity with id=" + id + " not found", ErrorCodes.ANNOUNCEMENT_NOT_FOUND));
        */
        return repository.findById(id)
                .map(MedicationDto::fromEntity)
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                "this entity with id=" + id + " not found",
                                ErrorCodes.MEDICAMENT_NOT_FOUND
                        )
                );
    }

    @Override
    public MedicationDto update(MedicationDto medicationDto, Long id) {
        if (id == null) {
            log.error("id is null");
            return null; // TODO
        }
        List<String> errors = MedicationValidator.validate(medicationDto);
        if (!errors.isEmpty()) {
            throw new InvalidEntityException("entity is invalid", ErrorCodes.MEDICAMENT_IS_INVALID, errors);
        }
        Optional<Medication> optional = repository.findById(id);
        if (optional.isPresent()) {
            medicationDto.setId(id);
            Medication announcement = toEntity(medicationDto);
            Medication announcementUpdated = repository.save(announcement);
            return fromEntity(announcementUpdated);
        } else {
            throw new EntityNotFoundException(
                    "this entity with id=" + id + " not found",
                    ErrorCodes.MEDICAMENT_NOT_FOUND
            );
        }
    }

    @Override
    public void delete(Long id) {
        if (repository.existsById(id)) {
            repository.deleteById(id);
        }else {
            // todo
        }
    }

    @Override
    public void deleteMultiRows(Long[] ids) {

        for (long i : ids) {
            boolean exist=repository.existsById(i);
            if (exist){
                repository.deleteById(i);
            }else {
                // todo return msg infos contains list of id that not exist
                log.info("entity with id="+i+" not exist");
            }

        }

    }
}
