package com.anbara.medicaloffice.services.impl;

import com.anbara.medicaloffice.dao.RoleRepository;
import com.anbara.medicaloffice.dao.UserRepository;
import com.anbara.medicaloffice.dto.AppRoleDto;
import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.dto.UserDto;
import com.anbara.medicaloffice.dto.UserRequest;
import com.anbara.medicaloffice.entities.AppRole;
import com.anbara.medicaloffice.entities.AppUser;
import com.anbara.medicaloffice.exceptions.EntityNotFoundException;
import com.anbara.medicaloffice.exceptions.ErrorCodes;
import com.anbara.medicaloffice.exceptions.InvalidEntityException;
import com.anbara.medicaloffice.services.UserService;
import com.anbara.medicaloffice.utils.AppConstants;
import com.anbara.medicaloffice.utils.Helpers;
import com.anbara.medicaloffice.validators.AppUserValidator;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.anbara.medicaloffice.utils.AppConstants.ROLE_ADMIN;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;


@Service
@Slf4j
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

//    public AccountServiceImpl(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
//        this.userRepository = userRepository;
//        this.roleRepository = roleRepository;
//        this.passwordEncoder = passwordEncoder;
//    }

    @Override
    public UserDto saveUser(UserRequest userRequest) {
        List<String> errors = AppUserValidator.validateNewUser(userRequest, this);
        if (!errors.isEmpty()) {
            throw new InvalidEntityException("entity is invalid", ErrorCodes.USER_INVALID, errors);
        }
        //user.getRoles().forEach(role -> role.setId(null)); // on a ajoute ca, si on nous untise javax.validation
        AppUser appUser = new AppUser();
        appUser.setUsername(userRequest.getUsername());
        appUser.setPassword(userRequest.getPassword());
        appUser.setActive(userRequest.isActive());
        appUser.setPassword(passwordEncoder.encode(appUser.getPassword()));


        userRepository.save(appUser);

        addRoleToUser(userRequest.getUsername(), userRequest.getRoleName());

        return UserDto.toUserDto(appUser);


    }

    @Override
    public AppRoleDto saveRole(AppRoleDto roleDto) {

        AppRole appRole = roleRepository.save(AppRoleDto.toEntity(roleDto));
        return AppRoleDto.fromEntity(appRole);
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
        AppUser appUser = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username + " not found"));

        if (roleName.equalsIgnoreCase(ROLE_ADMIN)) {
            roleRepository.findAll().forEach(appRole -> appUser.getRoles().add(appRole));

//            Stream.of("ADMIN","DOCTOR","SECRETARY")   // equivalent to code above
//                    .forEach(name->{
//                        AppRole appRole = roleRepository.findByNameRole(name).orElseThrow(() -> new RuntimeException(name + " not found"));
//                        appUser.getRoles().add(appRole);
//                    });
        } else {
            AppRole appRole = roleRepository.findByRoleName(roleName).orElseThrow(() -> new EntityNotFoundException(roleName + " not found", ErrorCodes.ROLE_NOT_FOUND));
            appUser.getRoles().add(appRole);
        }

        //userRepository.save(appUser); // comme la methode est transactional, des qu'il fait commit automatiqument il sait
        // qu il faut ajouter un role, automatiqument il ajout au table d'association
    }

    @Override
    public AppUser findUserByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException(
                        "this entity with username=" + username + " not found",
                        ErrorCodes.USER_NOT_FOUND
                ));


    }

    @Override
    public boolean isUserAlreadyExists(String username) {
        return userRepository.findByUsername(username).isPresent();
    }

    @Override
    public boolean isRoleAlreadyExists(String roleName) {
        return roleRepository.findByRoleName(roleName).isPresent();
    }

    @Override
    public PageDto<UserDto> findAllUser(int page, int limit, String keyword) {
        Page<AppUser> userPage = userRepository.findAllByUsernameIgnoreCaseContaining(keyword, PageRequest.of(page, limit));

        PageDto<UserDto> pageDTO = new PageDto<>();
        List<UserDto> userDtoList = UserDto.convertAppUsersToUserResponses(userPage.getContent());
        pageDTO.setContent(userDtoList);
        pageDTO.setTotalElements(userPage.getTotalElements());
        pageDTO.setNumber(userPage.getNumber());
        pageDTO.setSize(userPage.getSize());

        return pageDTO;

    }


    @Override
    public void deleteUserById(Long id) {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
        } else {
            // todo
        }

    }

    @Override
    public UserDto findById(Long id) {
        if (id == null) {
            log.error("id is null");
            return null; // TODO
        }
        AppUser appUser = userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        "this entity with id=" + id + " not found",
                        ErrorCodes.USER_NOT_FOUND
                ));
        return UserDto.toUserDto(appUser);
    }

    @Override
    public UserDto updateUser(UserRequest userRequest, Long id) {
        if (id == null) {
            log.error("id is null");
            return null; // TODO
        }
        List<String> errors = AppUserValidator.validateUpdateUser(userRequest, this);
        if (!errors.isEmpty()) {
            throw new InvalidEntityException("entity is invalid", ErrorCodes.USER_INVALID, errors);
        }
        Optional<AppUser> optional = userRepository.findById(id);
        if (optional.isPresent()) {
            AppUser appUser = optional.get();
            appUser.setUsername(userRequest.getUsername());
            appUser.setPassword(passwordEncoder.encode(userRequest.getPassword()));
            appUser.setActive(userRequest.isActive());
            appUser.setRoles(new ArrayList<>());
            addRoleToUser(userRequest.getUsername(), userRequest.getRoleName());
            AppUser appUserUpdated = userRepository.save(appUser);
            return UserDto.toUserDto(appUserUpdated);
        } else {
            throw new EntityNotFoundException(
                    "this entity with id=" + id + " not found",
                    ErrorCodes.USER_NOT_FOUND
            );
        }


    }

    @Override
    public long numberCurrentUser() {
        return this.userRepository.count();

    }

    @Override
    public void changePassword(String username, String password, String confirmPassword) {
        final List<String> errors = AppUserValidator.validatePassword(password, confirmPassword, new ArrayList<>());
        if (!errors.isEmpty()) {
            throw new InvalidEntityException(errors.toString(), ErrorCodes.USER_INVALID, errors);
        }
        AppUser user = findUserByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException("user with username " + username + " not exist", ErrorCodes.USER_NOT_FOUND);
        }
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
    }

    @Override
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if (StringUtils.hasLength(authorizationHeader) && authorizationHeader.startsWith("Bearer ")) {
            try {
                String refresh_token = authorizationHeader.substring("Bearer ".length());
                Algorithm algorithm = Helpers.getAlgorithm();
                JWTVerifier verifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = verifier.verify(refresh_token);
                String username = decodedJWT.getSubject();
                AppUser appUser= findUserByUsername(username);


                String access_token = JWT.create()
                        .withSubject(appUser.getUsername())
                        .withExpiresAt(new Date(System.currentTimeMillis() + 10 * 60 * 1000))
                        .withIssuer(request.getRequestURL().toString())
                        .withClaim(AppConstants.CLAIM_ROLES_LABEL, appUser.getRoles().stream().map(AppRole::getRoleName).collect(Collectors.toList()))
                        .sign(algorithm);

//        response.setHeader("access_token",access_token);
//        response.setHeader("refresh_token",refresh_token);

//        send in body
                final Map<String, String> tokens = new HashMap<>();
                tokens.put("access_token", access_token);
                tokens.put("refresh_token", refresh_token);
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), tokens);

            } catch (Exception ex) {
                log.error("Error logging in : {}", ex.getMessage());
                response.setHeader("error", ex.getMessage());
                //response.sendError(FORBIDDEN.value());
                response.setStatus(FORBIDDEN.value());
                final Map<String, String> errors = new HashMap<>();
                errors.put("error_message", ex.getMessage());
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), errors);
            }

        } else {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,"Refresh token is missing");
        }
    }
}
