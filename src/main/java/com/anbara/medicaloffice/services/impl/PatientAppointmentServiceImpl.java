package com.anbara.medicaloffice.services.impl;

import com.anbara.medicaloffice.dao.AppointmentPatientRepo;
import com.anbara.medicaloffice.dao.PatientRepository;
import com.anbara.medicaloffice.dto.PatientAppointmentDto;
import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.dto.ResponseAllAppointmentsPendings;
import com.anbara.medicaloffice.entities.PatientAppointment;
import com.anbara.medicaloffice.entities.Patient;
import com.anbara.medicaloffice.exceptions.EntityNotFoundException;
import com.anbara.medicaloffice.exceptions.ErrorCodes;
import com.anbara.medicaloffice.exceptions.InvalidEntityException;
import com.anbara.medicaloffice.services.PatientAppointmentService;
import com.anbara.medicaloffice.validators.AppointmentPatientValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.anbara.medicaloffice.dto.PatientAppointmentDto.fromEntity;
import static com.anbara.medicaloffice.dto.PatientAppointmentDto.toEntity;
import static com.anbara.medicaloffice.utils.Helpers.changeDateString;

@Service
@Slf4j
public class PatientAppointmentServiceImpl implements PatientAppointmentService {
    private final AppointmentPatientRepo appointmentRepo;
    private final PatientRepository patientRepository;
    private final JdbcTemplate jdbcTemplate;

    public PatientAppointmentServiceImpl(AppointmentPatientRepo appointmentRepo, PatientRepository patientRepository,
                                         JdbcTemplate jdbcTemplate) {
        this.appointmentRepo = appointmentRepo;
        this.patientRepository = patientRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public PageDto<PatientAppointmentDto> getAllByPatientId(Long patientId, int page, int limit, String keyword) {

       /* Page<AppointmentPatient> appointmentPage = appointmentPatientRepo.findAllByPatientIdOrderByDateDesc(patientId, PageRequest.of(page, limit));

        PageDto<AppointmentPatient> pageDTO = new PageDto<>();
        pageDTO.setContent(appointmentPage.getContent());
        pageDTO.setTotalElements(appointmentPage.getTotalElements());
        pageDTO.setNumber(appointmentPage.getNumber());
        pageDTO.setSize(appointmentPage.getSize());

        return pageDTO;*/

        Page<PatientAppointment> appointmentPage = appointmentRepo.findAllByPatientIdOrderByDateDesc(patientId, PageRequest.of(page, limit));

        return PageDto.fromPage(appointmentPage.map(PatientAppointmentDto::fromEntity));
    }

    @Override
    public PatientAppointmentDto newAppointment(Long patientId,
                                                PatientAppointmentDto appointmentDto) {
        if (patientId == null) {
            // todo
            return null;
        }
        List<String> errors = AppointmentPatientValidator.validate(appointmentDto);
        if (!errors.isEmpty()) {
            throw new InvalidEntityException("entity is invalid", ErrorCodes.PATIENT_APPOINTMENT_INVALID, errors);
        }

        Patient patient = patientRepository.findById(patientId)
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                "patient with id=" + patientId + " not found",
                                ErrorCodes.PATIENT_NOT_FOUND
                        )
                );
        PatientAppointment newAppointment = toEntity(appointmentDto);
        newAppointment.setPatient(patient);
        PatientAppointment appointmentAdded = appointmentRepo.save(newAppointment);
        return fromEntity(appointmentAdded);
    }

    @Override
    public PatientAppointmentDto update(PatientAppointmentDto appointmentDto, Long id) {
        if (id == null) {
            log.error("id appointment of patient is null");
            return null; // TODO
        }
        List<String> errors = AppointmentPatientValidator.validate(appointmentDto);
        if (!errors.isEmpty()) {
            throw new InvalidEntityException("entity is invalid", ErrorCodes.PATIENT_APPOINTMENT_INVALID, errors);
        }
        Optional<PatientAppointment> optional = appointmentRepo.findById(id);
        if (optional.isPresent()) {
            PatientAppointment oldAppointment=optional.get();
            oldAppointment.setDate(oldAppointment.getDate());
            oldAppointment.setTime(oldAppointment.getTime());
            oldAppointment.setStatus(oldAppointment.getStatus());
            PatientAppointment appointmentUpdated= appointmentRepo.save(oldAppointment);
            return fromEntity(appointmentUpdated);
        }else {
            throw new EntityNotFoundException("appointment of patient not found", ErrorCodes.PATIENT_APPOINTMENT_NOT_FOUND);

        }


    }

    @Override
    public void delete(Long id) {
        if (appointmentRepo.existsById(id)) {
            appointmentRepo.deleteById(id);
        } else {
            // todo
        }

    }

    @Override
    public List<ResponseAllAppointmentsPendings> getAllAppointmentsPending() {


        return jdbcTemplate.query(
                "SELECT patient.id,patient.first_name, patient.last_name, patient_appointment.date, patient_appointment.time FROM patient_appointment INNER JOIN patient ON patient_appointment.patient_id=patient.id AND patient_appointment.status=\'Pending\'",
                (resultSet, i) -> {

                    ResponseAllAppointmentsPendings resp = new ResponseAllAppointmentsPendings();
                    resp.setPatient_id(resultSet.getLong("id"));
                    resp.setFirst_name(resultSet.getString("first_name"));
                    resp.setLast_name(resultSet.getString("last_name"));
                    resp.setDate(resultSet.getString("date"));
                    resp.setTime(resultSet.getString("time"));

                    return resp;


                }
        );
    }

    @Override
    public PageDto<ResponseAllAppointmentsPendings> allPendingByPage(Pageable pageable, String date) {

        final String rowCountSql = "SELECT count(1) AS row_count " +
                " FROM patient_appointment INNER JOIN patient ON patient_appointment.patient_id=patient.id AND patient_appointment.status=\'Pending\'";
        int total =
                jdbcTemplate.queryForObject(
                        rowCountSql,
                        new Object[]{}, (rs, rowNum) -> rs.getInt(1)
                );
        long limit = pageable.getPageSize();
        long offset = pageable.getOffset();
        final String QUERY = "SELECT patient.id,patient.first_name, patient.last_name, "
                + " patient_appointment.date, patient_appointment.time FROM " +
                " patient_appointment INNER JOIN patient ON "
                + " patient_appointment.patient_id=patient.id AND "
                + " patient_appointment.status=\'Pending\' AND patient_appointment.date=STR_TO_DATE(\'" + date + "\',\'%d/%m/%Y\')"
              //  + " patient_appointment.status=\'Pending\' AND patient_appointment.date=\'2021-05-03\'"
                + " LIMIT " + limit
                + " OFFSET " + offset;
        List<ResponseAllAppointmentsPendings> demos =
                jdbcTemplate.query(QUERY, (resultSet, i) -> {

                    ResponseAllAppointmentsPendings resp = new ResponseAllAppointmentsPendings();
                    resp.setPatient_id(resultSet.getLong("id"));
                    resp.setFirst_name(resultSet.getString("first_name"));
                    resp.setLast_name(resultSet.getString("last_name"));

                    resp.setDate(changeDateString(resultSet.getString("date")));

                    resp.setTime(resultSet.getString("time"));
                    return resp;

                });
        PageDto<ResponseAllAppointmentsPendings> pageDTO = new PageDto<>();
        pageDTO.setContent(demos);
        pageDTO.setTotalElements(total);
        pageDTO.setSize(pageable.getPageSize());
        long pageNumber = (offset + limit) / limit - 1; // -1 ??
        pageDTO.setNumber(pageNumber);

        return pageDTO;
    }


}
