package com.anbara.medicaloffice.services;

import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.dto.PatientDto;
import com.anbara.medicaloffice.entities.Patient;


public interface PatientService {

     PageDto<PatientDto> all(int page, int limit, String keyword);
     PatientDto newPatient(PatientDto patientDto);
     PatientDto one(Long id);
     PatientDto replacePatient(PatientDto patientDto, Long id);
     void deletePatient(Long id);
}
