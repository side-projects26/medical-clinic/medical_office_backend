package com.anbara.medicaloffice.services;

import com.anbara.medicaloffice.entities.Certificate;
import com.anbara.medicaloffice.entities.Prescription;

public interface CertificateService {
    Certificate add(Long idConsultation, Certificate certificate);

    Certificate getOne(Long consultationId);

    Certificate update(Certificate newCertificate, Long id);

    void delete(Long id);
}
