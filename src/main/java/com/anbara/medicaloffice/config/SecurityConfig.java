package com.anbara.medicaloffice.config;

import com.anbara.medicaloffice.services.auth.UserDetailsServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static com.anbara.medicaloffice.utils.AppConstants.*;


@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ObjectMapper objectMapper;

   /* @Value("${server.servlet.context-path}")
    private String CONTEXT_PATH;*/

    public SecurityConfig(UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder);
    }

    @Bean
    public JWTAuthorizationFilter jwtAuthorizationFilter() {
        return new JWTAuthorizationFilter();

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        // dont create session
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .mvcMatchers("/login/**","/users/token/refresh"
                        , "/api-mobile/**"
                        , "/v2/api-docs", "/swagger-resources/**", "/swagger-ui/**",
                        "/webjars/**"
                )
                .permitAll()
                .and()
                .authorizeRequests()
               // .mvcMatchers(HttpMethod.GET, "/infos/**").permitAll()
                .and()
                .authorizeRequests()
                .mvcMatchers("/users/**","/infos/**",
                        "/databaseManagement/**").hasAuthority(ROLE_ADMIN)
                .and()
                .authorizeRequests()
                .mvcMatchers("/patients/**", "/consultations/**",
                        "/patientAppointments/**", "/visitorAppointments/**"
                        , "/statistics/**", "/medications/**")
                .hasAnyAuthority(ROLE_DOCTOR, ROLE_SECRETARY)
                .and()
                .authorizeRequests()
                .mvcMatchers("/prescriptions/**","/certificates/**")
                .hasAuthority(ROLE_DOCTOR)

                // .and()
                // .authorizeRequests().antMatchers("/patients/**","/appointments/**").hasAuthority(ROLE_SECRETARY")
                .and()
                .authorizeRequests().anyRequest().authenticated()
                .and()
                .addFilter(new JWTAuthenticationFilter(authenticationManager(), objectMapper))
                .addFilterBefore(jwtAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);

    }
}
