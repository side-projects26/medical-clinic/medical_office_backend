package com.anbara.medicaloffice.enums;

public enum Gender {
    Female, Male;
}
