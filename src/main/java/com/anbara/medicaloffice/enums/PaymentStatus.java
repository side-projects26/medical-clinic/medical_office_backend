package com.anbara.medicaloffice.enums;

public enum PaymentStatus {
    Complete, Incomplete
}
