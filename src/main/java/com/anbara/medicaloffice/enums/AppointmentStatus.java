package com.anbara.medicaloffice.enums;

public enum AppointmentStatus {
    Pending, Done
}
