package com.anbara.medicaloffice.validators;

import com.anbara.medicaloffice.entities.Payment;

import java.util.ArrayList;
import java.util.List;

public class PaymentValidator {

    public static List<String> validate(Payment payment) {
        final List<String> errors = new ArrayList<>();
        if (payment == null) {
            errors.add("should not null");
            return errors;
        }
        if (payment.getTotal().signum() < 0) {
            errors.add("Minimum value of total should be 0");
        }
        if (payment.getAmountPaid().signum() < 0) {
            errors.add("Minimum value of amount paid should be 0");
        }
        if (payment.getAmountPaid().compareTo(payment.getTotal()) > 0) {
            errors.add("Maximum value of amount paid should be " + payment.getTotal());

        }

        return errors;
    }
}
