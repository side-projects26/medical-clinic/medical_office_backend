package com.anbara.medicaloffice.validators;

import com.anbara.medicaloffice.dto.LocationDto;
import static com.anbara.medicaloffice.utils.AppConstants.MAX_LENGTH_ADDRESS_OFFICE;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class LocationOfficeValidator {
    // TODO
    public static List<String> validate(LocationDto locationDto) {
        final List<String> errors = new ArrayList<>();
        if (locationDto == null) {
            errors.add("should not null");
            return errors;
        }
        if (!StringUtils.hasLength(locationDto.getAddress())) {
            errors.add("Address is obligatory");
            return errors;
        }
        if (locationDto.getAddress().length()>MAX_LENGTH_ADDRESS_OFFICE) {
            errors.add("length od address should less then "+MAX_LENGTH_ADDRESS_OFFICE);
        }
        return errors;
    }
}
