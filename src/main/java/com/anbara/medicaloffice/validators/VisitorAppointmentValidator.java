package com.anbara.medicaloffice.validators;

import com.anbara.medicaloffice.dto.VisitorAppointmentDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class VisitorAppointmentValidator {
    public static List<String> validate(VisitorAppointmentDto appointmentDto) {
        final List<String> errors = new ArrayList<>();
        if (appointmentDto == null){
            errors.add("FirstName is obligatory");
            errors.add("LastName is obligatory");
            return errors;
        }
        if (!StringUtils.hasLength(appointmentDto.getFirstName())) {
            errors.add("FirstName is obligatory");
        }if (!StringUtils.hasLength(appointmentDto.getLastName())) {
            errors.add("LastName is obligatory");
        }
        return errors;
    }
}
