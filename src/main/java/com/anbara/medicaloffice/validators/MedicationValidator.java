package com.anbara.medicaloffice.validators;

import com.anbara.medicaloffice.dto.MedicationDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class MedicationValidator {

    public static List<String> validate(MedicationDto medicationDto) {
        final List<String> errors = new ArrayList<>();
        if (medicationDto == null
                || !StringUtils.hasLength(medicationDto.getName())) {
            errors.add("name of Medication is obligatory");
        }


        return errors;
    }
}
