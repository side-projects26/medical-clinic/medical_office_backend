package com.anbara.medicaloffice.validators;

import com.anbara.medicaloffice.dto.VisitorAppointmentDto;
import com.anbara.medicaloffice.entities.Prescription;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class PrescriptionValidator {
    public static List<String> validate(Prescription prescription) {
        final List<String> errors = new ArrayList<>();
        if (prescription == null){
            errors.add("prescription should be not null");
            return errors;
        }
        return errors;
    }
}
