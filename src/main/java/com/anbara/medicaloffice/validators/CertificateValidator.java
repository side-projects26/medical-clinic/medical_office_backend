package com.anbara.medicaloffice.validators;

import com.anbara.medicaloffice.entities.Certificate;
import com.anbara.medicaloffice.entities.Prescription;

import java.util.ArrayList;
import java.util.List;

public class CertificateValidator {
    public static List<String> validate(Certificate certificate) {
        final List<String> errors = new ArrayList<>();
        if (certificate == null){
            errors.add("certificate should be not null");
            return errors;
        }
        return errors;
    }
}
