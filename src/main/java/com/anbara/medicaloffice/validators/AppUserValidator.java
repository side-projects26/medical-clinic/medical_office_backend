package com.anbara.medicaloffice.validators;

import static com.anbara.medicaloffice.utils.AppConstants.MIN_LENGTH_USERNAME;
import static com.anbara.medicaloffice.utils.AppConstants.MIN_LENGTH_PASSWORD;

import com.anbara.medicaloffice.dto.UserRequest;
import com.anbara.medicaloffice.services.UserService;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class AppUserValidator {

    public static List<String> validateNewUser(UserRequest userRequest, UserService userService) {
        final List<String> errors = new ArrayList<>();
        if (userRequest == null) {
            errors.add("should not null");
            return errors;
        }
        final String username = userRequest.getUsername();
        final String password = userRequest.getPassword();
        final String confirmPassword = userRequest.getConfirmPassword();
        if (!StringUtils.hasLength(username)) {
            errors.add("username is obligatory");
        }
        if (username.length() < MIN_LENGTH_USERNAME) {
            errors.add("username should have at least " + MIN_LENGTH_USERNAME + " characters");
        }
        boolean isUserAlreadyExists = userService.isUserAlreadyExists(username);
        if (isUserAlreadyExists) {
            errors.add("this user already exists");
        }
        if (!StringUtils.hasLength(username)) {
            errors.add("password is obligatory");
        }
        // todo valid mix password
        if (password.length() < MIN_LENGTH_PASSWORD) {
            errors.add("password should have at least " + MIN_LENGTH_PASSWORD + " characters");
        }
        if (!password.equals(confirmPassword)) {
            errors.add("You must confirm your password");
        }
        /*boolean isRoleAlreadyExists = userService.isRoleAlreadyExists(userRequest.getRoleName());
        if (!isRoleAlreadyExists) {
            errors.add("role: "+userRequest.getRoleName()+" not exist!");
        }*/
        return errors;
    }
    public static List<String> validateUpdateUser(UserRequest userRequest, UserService userService) {
        final List<String> errors = new ArrayList<>();
        if (userRequest == null) {
            errors.add("should not null");
            return errors;
        }
        final String username = userRequest.getUsername();
        if (!StringUtils.hasLength(username)) {
            errors.add("username is obligatory");
        }
        if (username.length() < MIN_LENGTH_PASSWORD) {
            errors.add("username should have at least " + MIN_LENGTH_USERNAME + " characters");
        }
        //validatePassword(userRequest.getPassword(),userRequest.getConfirmPassword(),errors);
        boolean isUserAlreadyExists = userService.isUserAlreadyExists(username);
        if (!isUserAlreadyExists) {
            errors.add("this user dont exists");
        }
        /*boolean isRoleAlreadyExists = userService.isRoleAlreadyExists(userRequest.getRoleName());
        if (!isRoleAlreadyExists) {
            errors.add("role: "+userRequest.getRoleName()+" not exist!");
        }*/
        return errors;
    }

    public static List<String> validatePassword(String password, String confirmPassword, List<String> errors) {
        if (!password.equals(confirmPassword))
        {
            errors.add("You must confirm your password");
        }
        if (StringUtils.hasLength(password) && password.length()<MIN_LENGTH_PASSWORD){
            errors.add("password should have at least " + MIN_LENGTH_PASSWORD + " characters");
        }
        return errors;
    }
}
