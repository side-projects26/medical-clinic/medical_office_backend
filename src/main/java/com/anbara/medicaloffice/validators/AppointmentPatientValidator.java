package com.anbara.medicaloffice.validators;

import com.anbara.medicaloffice.dto.PatientAppointmentDto;

import java.util.ArrayList;
import java.util.List;

public class AppointmentPatientValidator {
    // TODO
    public static List<String> validate(PatientAppointmentDto appointmentDto) {
        final List<String> errors = new ArrayList<>();
        if (appointmentDto == null){
            errors.add("should not null");
            return errors;
        }
//        if (appointmentDto.getPatientDto()==null){
//            errors.add("Patient is obligatory");
//        }
        if (appointmentDto.getDate()==null) {
            errors.add("Date is obligatory");
        }
        return errors;
    }
}
