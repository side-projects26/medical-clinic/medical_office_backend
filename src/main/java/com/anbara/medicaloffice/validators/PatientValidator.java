package com.anbara.medicaloffice.validators;

import com.anbara.medicaloffice.dto.PatientDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class PatientValidator {

    public static List<String> validate(PatientDto patientDto) {
        final List<String> errors = new ArrayList<>();
        if (patientDto == null) {
            errors.add("should not null");
            return errors;
        }
        if (!StringUtils.hasLength(patientDto.getFirstName())) {
            errors.add("FirstName is obligatory");
        }if (!StringUtils.hasLength(patientDto.getLastName())) {
            errors.add("LastName is obligatory");
        }
        return errors;
    }
}
