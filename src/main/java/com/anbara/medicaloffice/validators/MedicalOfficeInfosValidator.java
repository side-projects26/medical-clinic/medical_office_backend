package com.anbara.medicaloffice.validators;

import com.anbara.medicaloffice.dto.DoctorDto;
import com.anbara.medicaloffice.dto.MedicalOfficeInfosDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static com.anbara.medicaloffice.utils.AppConstants.*;

public class MedicalOfficeInfosValidator {
    public static List<String> validate(MedicalOfficeInfosDto officeDto) {
        final List<String> errors = new ArrayList<>();
        if (officeDto == null) {
            errors.add("Medical Office Name is obligatory");
            errors.add("Doctor FullName is obligatory");
            errors.add("Specialty Doctor is obligatory");
            return errors;
        }
        final String medicalOfficeName = officeDto.getMedicalOfficeName();
        final DoctorDto doctorDto=officeDto.getDoctorDto();
        final String doctorFullName = doctorDto.getName();
        final String specialtyDoctor = doctorDto.getSpecialty();
        final String contactPhone = officeDto.getPhone();

        if (!StringUtils.hasLength(medicalOfficeName)) {
            errors.add("Medical Office Name is obligatory");
        } else if (medicalOfficeName.length() > MAX_LENGTH_MEDICAL_OFFICE_NAME) {
            errors.add("length of Medical Office Name should less then " + MAX_LENGTH_MEDICAL_OFFICE_NAME + " character's");

        }
        if (!StringUtils.hasLength(doctorFullName)) {
            errors.add("Doctor FullName is obligatory");
        } else if (doctorFullName.length() > MAX_LENGTH_DOCTOR_FULL_NAME) {
            errors.add("length of doctor FullName should less then " + MAX_LENGTH_DOCTOR_FULL_NAME + " character's");
        }
        if (!StringUtils.hasLength(specialtyDoctor)) {
            errors.add("Specialty Doctor is obligatory");
        } else if (specialtyDoctor.length() > MAX_LENGTH_SPECIALTY_DOCTOR) {
            errors.add("length of specialty Doctor should less then " + MAX_LENGTH_SPECIALTY_DOCTOR + " character's");
        }
        if (!StringUtils.hasLength(contactPhone)) {
            errors.add("contact phone is obligatory");
        } else if (contactPhone.length() > MAX_LENGTH_PHONE) {
            errors.add("length of phone should less then " + MAX_LENGTH_PHONE + " character's");
        }
        return errors;
    }
}
