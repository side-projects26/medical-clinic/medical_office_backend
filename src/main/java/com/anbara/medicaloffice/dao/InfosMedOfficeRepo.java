package com.anbara.medicaloffice.dao;

import com.anbara.medicaloffice.entities.MedicalOfficeInfos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InfosMedOfficeRepo extends JpaRepository<MedicalOfficeInfos,Long> {
}
