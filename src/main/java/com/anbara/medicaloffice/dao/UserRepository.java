package com.anbara.medicaloffice.dao;

import com.anbara.medicaloffice.entities.AppUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends PagingAndSortingRepository<AppUser,Long> {

    Optional<AppUser> findByUsername(String username);
    Page<AppUser> findAllByUsernameIgnoreCaseContaining(String username, Pageable pageable);


}
