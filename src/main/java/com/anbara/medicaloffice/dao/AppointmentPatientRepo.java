package com.anbara.medicaloffice.dao;

import com.anbara.medicaloffice.entities.PatientAppointment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AppointmentPatientRepo extends PagingAndSortingRepository<PatientAppointment,Long> {
    @Query(value = "SELECT * FROM patient_appointment app WHERE app.patient_id=:patient_id",
            nativeQuery=true)
    List<PatientAppointment> findAllByPatientId(@Param("patient_id") Long patientId);


    @Query(value = "SELECT * FROM patient_appointment app WHERE app.patient_id=:patient_id",
            countQuery = "SELECT count(*) FROM patient_appointment app WHERE app.patient_id=:patient_id",
            nativeQuery=true)
    Page<PatientAppointment> findAllByPatientIdOrderByDateDesc(@Param("patient_id") Long patientId, Pageable pageable);

}
