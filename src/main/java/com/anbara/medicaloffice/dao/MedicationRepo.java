package com.anbara.medicaloffice.dao;

import com.anbara.medicaloffice.entities.Medication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MedicationRepo extends PagingAndSortingRepository<Medication, Long> {
    //Page<Medication> findAllByNameLike(String name, Pageable pageable);
    Page<Medication> findByNameContaining(String name, Pageable pageable);
//    Page<Medicament> findAllByOrderByDateCreationDesc(Pageable pageable);



}
