package com.anbara.medicaloffice.dao;

import com.anbara.medicaloffice.entities.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepository extends PagingAndSortingRepository<Patient,Long> {
     Page<Patient> findAllByLastNameIgnoreCaseContainingOrderByDateCreationAsc(String lastName, Pageable pageable);
}
