package com.anbara.medicaloffice.dao;

import com.anbara.medicaloffice.entities.Prescription;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrescriptionRepo extends PagingAndSortingRepository<Prescription, Long> {
}
