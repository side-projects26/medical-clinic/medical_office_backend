package com.anbara.medicaloffice.dao;

import com.anbara.medicaloffice.entities.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface RoleRepository extends JpaRepository<AppRole,Long> {
    Optional<AppRole> findByRoleName(String roleName);

}
