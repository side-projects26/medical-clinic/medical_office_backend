package com.anbara.medicaloffice.dao;

import com.anbara.medicaloffice.entities.VisitorAppointment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AppointmentFirstVisitRepo extends PagingAndSortingRepository<VisitorAppointment,Long> {

    Page<VisitorAppointment> findAllByLastNameIgnoreCaseContainingOrderByDateDesc(String lastName, Pageable pageable);

}
