package com.anbara.medicaloffice.dao;

import com.anbara.medicaloffice.entities.Consultation;
import com.anbara.medicaloffice.entities.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface ConsultationRepository extends PagingAndSortingRepository<Consultation,Long> {
    Page<Consultation> findAllByPatientAndDateOrderByDateDesc(Patient patient, LocalDate date, Pageable pageable);
    Page<Consultation> findAllByPatientOrderByDateDesc(Patient patient, Pageable pageable);

}
