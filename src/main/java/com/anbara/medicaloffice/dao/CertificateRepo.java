package com.anbara.medicaloffice.dao;

import com.anbara.medicaloffice.entities.Certificate;
import com.anbara.medicaloffice.entities.Payment;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CertificateRepo extends PagingAndSortingRepository<Certificate, Long> {
}
