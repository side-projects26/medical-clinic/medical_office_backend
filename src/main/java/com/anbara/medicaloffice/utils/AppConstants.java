package com.anbara.medicaloffice.utils;

public interface AppConstants {
    String PARENT_FOLDERS_APPLICATION = "medical_office_files";
    String NAME_FOLDER_UPLOAD_FILES = "uploadFile";
    String NAME_UPLOAD_BACKUP_DB = "backup_from_user";
    String NAME_FOLDER_BACKUP_DB = "backup_send_to_user";
    String[] CHILDREN_FOLDERS = {NAME_UPLOAD_BACKUP_DB, NAME_FOLDER_BACKUP_DB};
    String PATTERN_DATE = "dd/MM/yyyy"; // used with LocalDate
    String PATTERN_TIME = "HH:mm";
    String PATTERN_DATE_TIME = PATTERN_DATE + " HH:mm"; // used with LocalDateTime

    /**
     * for validations
     */
    int MAX_LENGTH_ADDRESS_OFFICE = 74;
    int MAX_LENGTH_MEDICAL_OFFICE_NAME = 74;
    int MAX_LENGTH_DOCTOR_FULL_NAME = 74;
    int MAX_LENGTH_SPECIALTY_DOCTOR = 74;
    int MAX_LENGTH_PHONE = 25;
    // for user
    int MIN_LENGTH_USERNAME = 4;
    int MIN_LENGTH_PASSWORD = 8;





    /**
     * Security
     */
    String SECRET = "ayoub_anbara96";
    long EXPIRATION_TIME_ACCESS_TOKEN = 10 * 24 * 3600 * 1000;// 10 days
    long EXPIRATION_TIME_REFRESH_TOKEN = 20 * 24 * 3600 * 1000;// 20 days
    String TOKEN_PREFIX = "Bearer ";


    String ROLE_DOCTOR = "DOCTOR";
    String ROLE_SECRETARY = "SECRETARY";
    String ROLE_ADMIN = "ADMIN";
    String CLAIM_ROLES_LABEL = "roles";
}
