package com.anbara.medicaloffice.utils;

import com.auth0.jwt.algorithms.Algorithm;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE;

public class Helpers {

    public static BigDecimal calculatePercentage(float total, long obtained) {
        if (total == 0.0) {       //when we have any record in the database
            return new BigDecimal(0);
        }
        BigDecimal percentage = BigDecimal.valueOf((obtained / total) * 100);
        return percentage.setScale(2, BigDecimal.ROUND_HALF_EVEN);


    }

    public static String formatDate(LocalDate  localDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN_DATE);
        return localDate.format(formatter);
    }
    public static String getTodayDate(){
        LocalDate now = LocalDate.now();
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern(PATTERN_DATE);
        return now.format(formatters);
//        String text = now.format(formatters);
//        LocalDate parsedDateNow = LocalDate.parse(text, formatters);
//        return parsedDateNow.toString();
    }

    public static String changeDateString(String from){
        // yyyy-mm-dd to yyyy/mm/dd
        return from.replace('-','/');
    }
    public static Algorithm getAlgorithm(){
        return Algorithm.HMAC256(AppConstants.SECRET.getBytes());
    }
}
