package com.anbara.medicaloffice.utils;

import lombok.Data;
import lombok.SneakyThrows;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.validation.annotation.Validated;

import javax.mail.internet.InternetAddress;
import javax.validation.constraints.NotNull;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

@Data
@ConfigurationProperties("app.config")
@Configuration
@EnableAsync
@Validated
public class AppConfig {
//    private boolean initDb;
//    private Path uploadFolderPath = Paths.get(System.getProperty("user.home")).resolve("karaz");
//    private Path viewImagePath = Paths.get("/uploads");
//
//    @NotNull
//    private Url url;
//    @Length(min = 32)
//    private String jwtSecret = "12345678900987654321123456789009";

    // TODO use this


    static String APP_ROOT;
    public static String INFOS_ENDPOINT = APP_ROOT + "/infos";// use @API(INFOS_ENDPOINT)
    public static String LOGIN_ENDPOINT = APP_ROOT + "/login";// use @API(INFOS_ENDPOINT)
    public static String USERS_ENDPOINT = APP_ROOT + "/users";// use @API(INFOS_ENDPOINT)

    @Value("${server.servlet.context-path}")
    public static void setAppRoot(String appRoot) {
        APP_ROOT = appRoot;
    }
}
