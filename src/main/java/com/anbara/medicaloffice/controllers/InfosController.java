package com.anbara.medicaloffice.controllers;

import com.anbara.medicaloffice.controllers.api.InfosApi;
import com.anbara.medicaloffice.dto.MedicalOfficeInfosDto;
import com.anbara.medicaloffice.services.InfosService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/infos")
public class InfosController implements InfosApi {

    private final InfosService infosService;

    public InfosController(InfosService infosService) {
        this.infosService = infosService;
    }

    @Override
    public ResponseEntity<MedicalOfficeInfosDto> get() {
        return ResponseEntity.ok(infosService.getInfos());
    }

    @Override
    public ResponseEntity<MedicalOfficeInfosDto> update(@RequestBody MedicalOfficeInfosDto infosDto) {

        // return this.infosService.updateInfos(infosDto);
        return new ResponseEntity<>(this.infosService.updateInfos(infosDto), HttpStatus.OK);
    }

}
