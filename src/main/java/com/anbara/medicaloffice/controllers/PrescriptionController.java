package com.anbara.medicaloffice.controllers;

import com.anbara.medicaloffice.controllers.api.PrescriptionApi;
import com.anbara.medicaloffice.entities.Prescription;
import com.anbara.medicaloffice.services.PrescriptionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/prescriptions")
public class PrescriptionController implements PrescriptionApi {
    private final PrescriptionService prescriptionService;

    public PrescriptionController(PrescriptionService prescriptionService) {
        this.prescriptionService = prescriptionService;
    }

    @Override
    public ResponseEntity<Prescription> add(Long idConsultation, Prescription prescription) {
        return new ResponseEntity<>(prescriptionService.add(idConsultation, prescription), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Prescription> getOne(Long consultationId) {
        return new ResponseEntity<>(prescriptionService.getOne(consultationId), HttpStatus.ACCEPTED);
    }

    @Override
    public ResponseEntity<Prescription> update(Prescription newPrescription, Long consultationId) {
        return ResponseEntity.ok(prescriptionService.update(newPrescription, consultationId));

    }

    @Override
    public ResponseEntity<Void> delete(Long id) {
        prescriptionService.delete(id);
        return ResponseEntity.noContent().build();

    }
}
