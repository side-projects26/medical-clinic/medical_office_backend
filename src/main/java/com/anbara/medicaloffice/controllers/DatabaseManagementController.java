package com.anbara.medicaloffice.controllers;

import com.anbara.medicaloffice.services.DatabaseManagementService;
import com.anbara.medicaloffice.services.storage.StorageService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;

@RestController
@RequestMapping("/databaseManagement")
public class DatabaseManagementController {
    private final StorageService storageService;

    private final DatabaseManagementService dbService;

    public DatabaseManagementController(StorageService storageService, DatabaseManagementService dbService) {
        this.storageService = storageService;
        this.dbService = dbService;
    }

    @GetMapping(produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public byte[] exportBackupDB() throws SQLException, IOException, ClassNotFoundException {
        return Files.readAllBytes(dbService.exportDB());
    }


    @PutMapping(
            //consumes = "application/sql" // todo
    )
    public void handleFileSql(@RequestParam("backup") MultipartFile file) throws SQLException, IOException, ClassNotFoundException {
        String pathFileSql = storageService.storeBackupDB(file);
        boolean res = dbService.importDB(pathFileSql);

    }
}
