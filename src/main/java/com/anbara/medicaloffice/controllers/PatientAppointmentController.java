package com.anbara.medicaloffice.controllers;

import com.anbara.medicaloffice.controllers.api.PatientAppointmentsApi;
import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.dto.PatientAppointmentDto;
import com.anbara.medicaloffice.dto.ResponseAllAppointmentsPendings;
import com.anbara.medicaloffice.services.PatientAppointmentService;
import com.anbara.medicaloffice.utils.Helpers;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/patientAppointments")
public class PatientAppointmentController implements PatientAppointmentsApi {

    private final PatientAppointmentService appointmentService;

    public PatientAppointmentController(PatientAppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @Override
    public ResponseEntity<PageDto<PatientAppointmentDto>> getAllByPatientId(Long patientId, int page, int limit, String keyword) {
        return ResponseEntity.ok(this.appointmentService.getAllByPatientId(patientId, page, limit, keyword));
    }

    @Override
    public ResponseEntity<PageDto<ResponseAllAppointmentsPendings>> allPending(
            int page, int limit, String date) {
        if (date.equals("null")) {  // we use date today
            date = Helpers.getTodayDate();
        }
        return ResponseEntity.ok(this.appointmentService.allPendingByPage(PageRequest.of(page, limit), date));
    }

    @Override
    public ResponseEntity<PatientAppointmentDto> newAppointment(Long patientId, PatientAppointmentDto appointmentDto) {
        return new ResponseEntity<>(this.appointmentService.newAppointment(patientId, appointmentDto), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<PatientAppointmentDto> update(Long id, PatientAppointmentDto appointmentDto) {
        return ResponseEntity.ok(this.appointmentService.update(appointmentDto, id));
    }

    @Override
    public ResponseEntity<Void> delete(Long id) {
        this.appointmentService.delete(id);
        return ResponseEntity.noContent().build();
    }


}


