package com.anbara.medicaloffice.controllers;

import com.anbara.medicaloffice.controllers.api.UserApi;
import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.dto.UserDto;
import com.anbara.medicaloffice.dto.UserRequest;
import com.anbara.medicaloffice.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@RestController
@RequestMapping("/users")
public class UserController implements UserApi {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Override
    public ResponseEntity<PageDto<UserDto>> getAll(int page, int limit, String keyword) {
        return ResponseEntity.ok(userService.findAllUser(page, limit, keyword));
    }


    @Override
    public ResponseEntity<Void> delete(long id) {
        userService.deleteUserById(id);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<UserDto> getOne(Long id) {

        return ResponseEntity.ok(userService.findById(id));
    }

    @Override
    //  @Transactional(propagation = Propagation.REQUIRED)
    public ResponseEntity<UserDto> newUser(UserRequest userRequest) {
        return new ResponseEntity<>(userService.saveUser(userRequest), HttpStatus.CREATED);
    }


    @Override
    public ResponseEntity<UserDto> update(UserRequest userRequest, Long id) {
        return ResponseEntity.ok(userService.updateUser(userRequest, id));
    }

    @Override
    public ResponseEntity<Void>  changePassword(String username, String password, String confirmPassword) {
        userService.changePassword(username, password, confirmPassword);
        return ResponseEntity.noContent().build();
    }

    @Override
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        userService.refreshToken(request,response);
    }
}
