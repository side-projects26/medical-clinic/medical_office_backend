package com.anbara.medicaloffice.controllers;

import com.anbara.medicaloffice.controllers.api.VisitorAppointments;
import com.anbara.medicaloffice.dto.VisitorAppointmentDto;
import com.anbara.medicaloffice.services.VisitorAppointmentService;
import com.anbara.medicaloffice.services.impl.VisitorAppointmentServiceImpl;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/visitorAppointments")
public class VisitorAppointmentController implements VisitorAppointments {

    private final VisitorAppointmentService appointmentService;

    public VisitorAppointmentController(VisitorAppointmentServiceImpl appointmentService) {
        this.appointmentService = appointmentService;
    }

    @Override
    public ResponseEntity<Page<VisitorAppointmentDto>> all(int page, int limit, String keyword) {
        return ResponseEntity.ok(appointmentService.getAll(page, limit, keyword));
    }

    @Override
    public ResponseEntity<VisitorAppointmentDto> newAppointment(VisitorAppointmentDto appointmentFirstVisit) {
        return new ResponseEntity<>(this.appointmentService.newAppointment(appointmentFirstVisit), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<VisitorAppointmentDto> getOne(Long id) {
        return ResponseEntity.ok(appointmentService.getOne(id));
    }

    @Override
    public ResponseEntity<VisitorAppointmentDto> update(VisitorAppointmentDto newAppointmentGen, Long id) {
        return ResponseEntity.ok(appointmentService.update(newAppointmentGen, id));
    }

    @Override
    public ResponseEntity<Void> delete(Long id) {
        appointmentService.deleteOne(id);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Void> deleteMultiRows(Long[] ids) {
        appointmentService.deleteMultiRows(ids);
        return ResponseEntity.noContent().build();
    }
}
