package com.anbara.medicaloffice.controllers;

import com.anbara.medicaloffice.controllers.api.ConsultationApi;
import com.anbara.medicaloffice.dto.ConsultationDto;
import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.services.ConsultationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE;

@RestController
@RequestMapping("/consultations")
public class ConsultationController implements ConsultationApi {
    private final ConsultationService consultationService;

    public ConsultationController(ConsultationService consultationService) {
        this.consultationService = consultationService;
    }

    //    @GetMapping("/consultations")
//    Iterable<Consultation> all() {
//        return consultationRepository.findAll();
//    }

  /*  @GetMapping("/{idPatient}")
    public List<Consultation> allByPatientId(@PathVariable Long idPatient) {
//        return consultationRepository.findAllByPatientId(idPatient);
        Patient patient = patientRepository.findById(idPatient)
                .orElseThrow(() -> new PatientNotFoundException(idPatient));
        return consultationRepository.findAllByPatient(patient);

    }*/


    @Override
    public ResponseEntity<PageDto<ConsultationDto>> all(Long idPatient, int page, int limit,
                                                        String date) {
        PageDto<ConsultationDto> pageDto;
        if (date == null || date.equals("null")) {
            pageDto = consultationService.allByPatientId(idPatient, page, limit);
        } else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN_DATE);
            LocalDate localDate = LocalDate.parse(date, formatter);
            pageDto = consultationService.allByPatientIdAndDate(idPatient, page, limit, localDate);
        }
        return ResponseEntity.ok(pageDto);
    }

    @Override
    public ResponseEntity<ConsultationDto> newConsultation(Long idPatient, ConsultationDto newConsultation) {
        return new ResponseEntity<>(consultationService.newConsultation(idPatient, newConsultation), HttpStatus.CREATED);

    }

    @Override
    public ResponseEntity<ConsultationDto> update(Long idPatient, ConsultationDto consultationDto) {
        return ResponseEntity.ok(this.consultationService.update(idPatient, consultationDto));
    }

    @Override
    public ResponseEntity<Void> delete(Long id) {
        consultationService.deleteConsultation(id);
        return ResponseEntity.noContent().build();
    }


}
