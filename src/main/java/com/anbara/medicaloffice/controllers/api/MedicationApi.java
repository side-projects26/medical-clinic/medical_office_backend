package com.anbara.medicaloffice.controllers.api;

import com.anbara.medicaloffice.dto.MedicationDto;
import com.anbara.medicaloffice.dto.PageDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface MedicationApi {

    @GetMapping
    PageDto<MedicationDto> all(
            @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int limit
            , @RequestParam(defaultValue = "") String keyword);

    @PostMapping
    ResponseEntity<MedicationDto> add(@RequestBody MedicationDto medicationDto);

    // Single item

    @GetMapping("/{id}")
    ResponseEntity<MedicationDto> getOne(@PathVariable Long id);

    @PutMapping("/{id}")
    ResponseEntity<MedicationDto> update(@RequestBody MedicationDto newAnnouncement, @PathVariable Long id);

    @DeleteMapping("/{id}")
    ResponseEntity<Void> delete(@PathVariable Long id);

    @PostMapping("/deleteMultiRows")
    ResponseEntity<Void> deleteMultiRows(@RequestBody Long[] ids);
}
