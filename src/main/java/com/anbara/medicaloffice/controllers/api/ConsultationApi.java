package com.anbara.medicaloffice.controllers.api;

import com.anbara.medicaloffice.dto.ConsultationDto;
import com.anbara.medicaloffice.dto.PageDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface ConsultationApi {
    @PostMapping
    ResponseEntity<PageDto<ConsultationDto>> all(@RequestParam Long idPatient, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int limit,
                                                 @RequestParam(defaultValue = "null") String date);


    @PostMapping("/{idPatient}")
    ResponseEntity<ConsultationDto> newConsultation(@PathVariable Long idPatient, @RequestBody ConsultationDto newConsultation);

    @PutMapping("/{idPatient}")
    ResponseEntity<ConsultationDto> update(@PathVariable Long idPatient, @RequestBody ConsultationDto consultationDto);

    @DeleteMapping("/{id}")
    ResponseEntity<Void> delete(@PathVariable Long id);

}
