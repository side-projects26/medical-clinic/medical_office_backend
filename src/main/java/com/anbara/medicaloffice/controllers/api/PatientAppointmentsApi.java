package com.anbara.medicaloffice.controllers.api;

import com.anbara.medicaloffice.dto.PatientAppointmentDto;
import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.dto.ResponseAllAppointmentsPendings;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface PatientAppointmentsApi {
    @GetMapping("/{patientId}")
    ResponseEntity<PageDto<PatientAppointmentDto>> getAllByPatientId(@PathVariable Long patientId,
                                                     @RequestParam(defaultValue = "0") int page,
                                                     @RequestParam(defaultValue = "5") int limit,
                                                     @RequestParam(defaultValue = "") String keyword);


    @PostMapping("/{patientId}")
    ResponseEntity<PatientAppointmentDto> newAppointment(@PathVariable Long patientId, @RequestBody PatientAppointmentDto appointmentDto);

    @PutMapping("/{id}")
    ResponseEntity<PatientAppointmentDto> update(@PathVariable Long id, @RequestBody PatientAppointmentDto appointmentDto);

    @DeleteMapping("/{id}")
    ResponseEntity<Void> delete(@PathVariable Long id);

    @GetMapping("/allPending")
    ResponseEntity<PageDto<ResponseAllAppointmentsPendings>> allPending(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "5") int limit,
            @RequestParam(defaultValue = "null") String date
    );
}
