package com.anbara.medicaloffice.controllers.api;

import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.dto.UserDto;
import com.anbara.medicaloffice.dto.UserRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface UserApi {
    @GetMapping
    ResponseEntity<PageDto<UserDto>> getAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int limit,
                                            @RequestParam(defaultValue = "") String keyword);


    @DeleteMapping("/{id}")
    ResponseEntity<Void> delete(@PathVariable long id);

    @GetMapping("/{id}")
    ResponseEntity<UserDto> getOne(@PathVariable Long id);

    @PostMapping
    ResponseEntity<UserDto> newUser(@RequestBody UserRequest userRequest);


    @PutMapping("/{id}")
    ResponseEntity<UserDto> update(@RequestBody UserRequest userRequest, @PathVariable Long id);

    @PostMapping("/changePassword")
    ResponseEntity<Void>  changePassword(@RequestParam String username, @RequestParam String password,@RequestParam  String confirmPassword);

    @PostMapping("/token/refresh")
    // todo
    void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
