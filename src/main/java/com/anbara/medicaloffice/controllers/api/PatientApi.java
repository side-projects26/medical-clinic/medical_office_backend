package com.anbara.medicaloffice.controllers.api;

import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.dto.PatientDto;
import com.anbara.medicaloffice.entities.Patient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface PatientApi {
    @GetMapping
    ResponseEntity<PageDto<PatientDto>> all(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int limit,
                         @RequestParam(defaultValue = "") String keyword);

    @PostMapping
    ResponseEntity<PatientDto> newPatient(@RequestBody PatientDto patientDto);

    @GetMapping("/{id}")
    ResponseEntity<PatientDto> getOne(@PathVariable Long id);

    @PutMapping("/{id}")
    ResponseEntity<PatientDto> update(@RequestBody PatientDto patientDto, @PathVariable Long id);

    @DeleteMapping("/{id}")
    ResponseEntity<Void> deletePatient(@PathVariable Long id);


}
