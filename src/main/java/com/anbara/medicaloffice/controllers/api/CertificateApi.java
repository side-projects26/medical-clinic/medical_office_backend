package com.anbara.medicaloffice.controllers.api;

import com.anbara.medicaloffice.entities.Certificate;
import com.anbara.medicaloffice.entities.Prescription;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface CertificateApi {
    @PostMapping("/{consultationId}")
    ResponseEntity<Certificate> add(@PathVariable("consultationId") Long consultationId, @RequestBody Certificate certificate);

    // Single item

    @GetMapping("/{consultationId}")
    ResponseEntity<Certificate> getOne(@PathVariable Long consultationId);

    @PutMapping("/{id}")
    ResponseEntity<Certificate> update(@RequestBody Certificate newCertificate, @PathVariable Long consultationId);

    @DeleteMapping("/{id}")
    ResponseEntity<Void> delete(@PathVariable Long id);

}
