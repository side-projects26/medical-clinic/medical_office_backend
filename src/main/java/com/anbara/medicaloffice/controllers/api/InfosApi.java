package com.anbara.medicaloffice.controllers.api;

import com.anbara.medicaloffice.dto.MedicalOfficeInfosDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface InfosApi {
    @GetMapping
//    ResponseEntity<MedicalOfficeInfosDto> get();
    ResponseEntity<MedicalOfficeInfosDto> get();

    @PutMapping
    ResponseEntity<MedicalOfficeInfosDto> update(@RequestBody MedicalOfficeInfosDto infosDto);

}
