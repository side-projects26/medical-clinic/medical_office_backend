package com.anbara.medicaloffice.controllers.api;

import com.anbara.medicaloffice.dto.VisitorAppointmentDto;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface VisitorAppointments {
    @GetMapping
    ResponseEntity<Page<VisitorAppointmentDto>> all(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int limit,
                                                    @RequestParam(defaultValue = "") String keyword);

    @PostMapping
    ResponseEntity<VisitorAppointmentDto> newAppointment(@RequestBody VisitorAppointmentDto appointmentFirstVisit);

    @GetMapping("/{id}")
    ResponseEntity<VisitorAppointmentDto> getOne(@PathVariable Long id);

    @PutMapping("/{id}")
    ResponseEntity<VisitorAppointmentDto> update(@RequestBody VisitorAppointmentDto newAppointmentGen, @PathVariable Long id);


    @DeleteMapping("/{id}")
    ResponseEntity<Void> delete(@PathVariable Long id);

    @PostMapping("/deleteMultiRows")
    ResponseEntity<Void> deleteMultiRows(@RequestBody Long[] ids);

}
