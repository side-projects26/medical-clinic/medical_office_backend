package com.anbara.medicaloffice.controllers.api;

import com.anbara.medicaloffice.entities.Prescription;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface PrescriptionApi {
    @PostMapping("/{consultationId}")
    ResponseEntity<Prescription> add(@PathVariable("consultationId") Long consultationId,@RequestBody Prescription prescription);

    // Single item

    @GetMapping("/{consultationId}")
    ResponseEntity<Prescription> getOne(@PathVariable Long consultationId);

    @PutMapping("/{id}")
    ResponseEntity<Prescription> update(@RequestBody Prescription newPrescription, @PathVariable Long consultationId);

    @DeleteMapping("/{id}")
    ResponseEntity<Void> delete(@PathVariable Long id);
}
