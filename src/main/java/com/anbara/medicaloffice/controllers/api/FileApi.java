package com.anbara.medicaloffice.controllers.api;

import com.anbara.medicaloffice.dto.ResponseOperation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

public interface FileApi {
    @PutMapping("/logo/{infosId}")
    ResponseOperation handleLogoUpload(@RequestParam("logo") MultipartFile file, @PathVariable Long infosId) ;

}
