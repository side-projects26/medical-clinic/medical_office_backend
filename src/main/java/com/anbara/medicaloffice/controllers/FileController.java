package com.anbara.medicaloffice.controllers;

import com.anbara.medicaloffice.controllers.api.FileApi;
import com.anbara.medicaloffice.dao.InfosMedOfficeRepo;
import com.anbara.medicaloffice.dto.ResponseOperation;
import com.anbara.medicaloffice.entities.MedicalOfficeInfos;
import com.anbara.medicaloffice.exceptions.EntityNotFoundException;
import com.anbara.medicaloffice.exceptions.ErrorCodes;
import com.anbara.medicaloffice.services.storage.StorageException;
import com.anbara.medicaloffice.services.storage.StorageService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import static com.anbara.medicaloffice.utils.AppConstants.NAME_FOLDER_UPLOAD_FILES;

@RestController
public class FileController implements FileApi {
    private final StorageService storageService;
    private final InfosMedOfficeRepo infosMedOfficeRepo;
    private final ResponseOperation resOperation;

    public FileController(StorageService storageService, InfosMedOfficeRepo infosMedOfficeRepo) {
        this.storageService = storageService;
        this.infosMedOfficeRepo = infosMedOfficeRepo;
        resOperation = new ResponseOperation();
    }

    @Override
    public ResponseOperation handleLogoUpload(MultipartFile file,  Long infosId) {
        resOperation.setSuccess(true);
        resOperation.setMessage("");
        try {
            String logoPath = storageService.store(file);
            updateLogoInDB(logoPath, infosId);
        } catch (Exception e) {
            resOperation.setSuccess(false);
            resOperation.setMessage(e.getMessage());
        }
        return resOperation;

    }

    private void updateLogoInDB(String logoPath, Long infosId) {
        String logo = null;
        try {
            byte[] allBytes = Files.readAllBytes(Paths.get(logoPath));
            logo = new String(Base64.getEncoder().encode(allBytes));
            logo = "data:image/png;base64," + logo;

        } catch (IOException e) {
            e.printStackTrace();
        }

        Optional<MedicalOfficeInfos> optional = this.infosMedOfficeRepo.findById(infosId);
        if (optional.isPresent()) {
            MedicalOfficeInfos infos = optional.get();
            infos.setLogoBase64(logo);
            infosMedOfficeRepo.save(infos);
        } else {
            throw new EntityNotFoundException("infos with id=" + infosId + " not found", ErrorCodes.INFOS_MEDICAL_OFFICE_NOT_FOUND);
        }


    }






}



