package com.anbara.medicaloffice.controllers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UnionAppointmentController {

    private final JdbcTemplate jdbcTemplate;
    private static final String QUERY =
            "SELECT p.first_name, p.last_name,p.phone,app.date,app.time, app.status FROM patient p INNER JOIN " +
                    " patient_appointment app ON p.id=app.patient_id "
                    + "WHERE app.date=CURDATE()"
                    + "UNION" +
                    " SELECT first_name, last_name, phone ,date, time, status FROM visitor_appointment visitor_appointment "
                    + "WHERE visitor_appointment.date=CURDATE()"
                    + "ORDER BY CONVERT(date, DATE) DESC,CONVERT(time, TIME) ASC ";


    public UnionAppointmentController(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @GetMapping("/union")
    public List<AppointmentsUnion> getAll() {

        /*
        SELECT p.first_name, p.last_name,p.phone,app.date_appointment FROM patient p INNER JOIN appointment_old_patient app
        ON p.id=app.patient_id
        UNION
        SELECT app_new.first_name, app_new.last_name, app_new.phone ,app_new.date_appointment FROM `visitor_appointment` app_new
        */

        return jdbcTemplate.query(QUERY,
                (resultSet, i) ->
                        new AppointmentsUnion(resultSet.getString("first_name"), resultSet.getString("last_name"),
                                resultSet.getString("phone"),
                                resultSet.getString("time"), resultSet.getString("status"))
        );


    }
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class AppointmentsUnion {
    private String firstName;
    private String lastName;
    private String phone;
    private String time;
    private String status;

}
