package com.anbara.medicaloffice.controllers;

import com.anbara.medicaloffice.controllers.api.PatientApi;
import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.dto.PatientDto;
import com.anbara.medicaloffice.entities.Patient;
import com.anbara.medicaloffice.services.PatientService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/patients")
public class PatientController implements PatientApi {
    private final PatientService patientService;

    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @Override
    public ResponseEntity<PageDto<PatientDto>> all(int page, int limit, String keyword) {
        return ResponseEntity.ok(this.patientService.all(page, limit, keyword));

    }

    @Override
    public ResponseEntity<PatientDto> newPatient(PatientDto patientDto) {
        return new ResponseEntity<>(this.patientService.newPatient(patientDto), HttpStatus.CREATED);
    }


    @Override
    public ResponseEntity<PatientDto> getOne(Long id) {
        return ResponseEntity.ok(patientService.one(id));
    }

    @Override
    public ResponseEntity<PatientDto> update(PatientDto newPatient, Long id) {
        return ResponseEntity.ok(patientService.replacePatient(newPatient, id));
    }

    @Override
    public ResponseEntity<Void> deletePatient(Long id) {
        patientService.deletePatient(id);
        return ResponseEntity.noContent().build();
    }


}


