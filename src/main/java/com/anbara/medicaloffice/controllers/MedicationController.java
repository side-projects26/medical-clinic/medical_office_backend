package com.anbara.medicaloffice.controllers;

import com.anbara.medicaloffice.controllers.api.MedicationApi;
import com.anbara.medicaloffice.dto.MedicationDto;
import com.anbara.medicaloffice.dto.PageDto;
import com.anbara.medicaloffice.services.MedicamentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/medications")
public class MedicationController implements MedicationApi {
    private final MedicamentService medicamentService;


    public MedicationController(MedicamentService medicamentService) {
        this.medicamentService = medicamentService;
    }

    @Override
    public PageDto<MedicationDto> all(int page, int limit, String keyword) {
        return this.medicamentService.getAll(page, limit, keyword);
    }

    @Override
    public ResponseEntity<MedicationDto> add(MedicationDto medicationDto) {
        return new ResponseEntity<>(medicamentService.add(medicationDto), HttpStatus.CREATED);
    }

    // Single item

    @Override
    public ResponseEntity<MedicationDto> getOne(Long id) {
        return ResponseEntity.ok(this.medicamentService.getOne(id));
    }

    @Override
    public ResponseEntity<MedicationDto> update(MedicationDto newAnnouncement, Long id) {

        return ResponseEntity.ok(this.medicamentService.update(newAnnouncement, id));
    }

    @Override
    public ResponseEntity<Void> delete(Long id) {
        this.medicamentService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Void> deleteMultiRows(Long[] ids) {
        this.medicamentService.deleteMultiRows(ids);
        return ResponseEntity.noContent().build();
    }
}
