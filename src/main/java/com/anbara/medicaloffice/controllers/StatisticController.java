package com.anbara.medicaloffice.controllers;

import com.anbara.medicaloffice.dto.StatisticResponse;
import com.anbara.medicaloffice.services.StatisticsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatisticController {

   private final StatisticsService statisticsService;

    public StatisticController(com.anbara.medicaloffice.services.StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @GetMapping("/statistics")
    public StatisticResponse getStatistics(){
        return statisticsService.getStatistics();
    }




}
