package com.anbara.medicaloffice.controllers;

import com.anbara.medicaloffice.controllers.api.CertificateApi;
import com.anbara.medicaloffice.controllers.api.PrescriptionApi;
import com.anbara.medicaloffice.entities.Certificate;
import com.anbara.medicaloffice.entities.Prescription;
import com.anbara.medicaloffice.services.CertificateService;
import com.anbara.medicaloffice.services.PrescriptionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/certificates")
public class CertificateController implements CertificateApi {
    private final CertificateService certificateService;

    public CertificateController(CertificateService certificateService) {
        this.certificateService = certificateService;
    }

    @Override
    public ResponseEntity<Certificate> add(Long idConsultation, Certificate certificate) {
        return new ResponseEntity<>(certificateService.add(idConsultation, certificate), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Certificate> getOne(Long consultationId) {
        return new ResponseEntity<>(certificateService.getOne(consultationId), HttpStatus.ACCEPTED);
    }

    @Override
    public ResponseEntity<Certificate> update(Certificate newPrescription, Long consultationId) {
        return ResponseEntity.ok(certificateService.update(newPrescription, consultationId));

    }

    @Override
    public ResponseEntity<Void> delete(Long id) {
        certificateService.delete(id);
        return ResponseEntity.noContent().build();

    }
}
