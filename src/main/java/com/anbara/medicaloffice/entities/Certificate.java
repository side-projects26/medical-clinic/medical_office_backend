package com.anbara.medicaloffice.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDate;

import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Certificate implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = PATTERN_DATE)
    @DateTimeFormat(pattern = PATTERN_DATE)
    private LocalDate dateCreation;

    // private String firstName, lastName, national_identity_card;

    // todo size
    private String detail;

    private CertificateType type;

    public Certificate(CertificateType type, String detail) {
        this.type = type;
        this.detail = detail;

    }

    public enum CertificateType {
        Aptitude, Medical, Dispense;
    }
}
