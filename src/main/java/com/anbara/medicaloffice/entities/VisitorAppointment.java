package com.anbara.medicaloffice.entities;

import com.anbara.medicaloffice.enums.AppointmentStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE;
import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_TIME;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class VisitorAppointment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 74)
    private String firstName;
    @Column(length = 74)
    private String lastName;
    @Column(length = 60)
    private String phone;
    //    @Temporal(TemporalType.DATE)  ex: @Temporal should only be set on a java.util.Date or java.util.Calendar
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = PATTERN_DATE)
    @DateTimeFormat(pattern = PATTERN_DATE)
    private LocalDate date;
    //    @Temporal(TemporalType.TIME)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = PATTERN_TIME)
    @DateTimeFormat(pattern = PATTERN_TIME)
    private LocalTime time;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private AppointmentStatus status;



}
