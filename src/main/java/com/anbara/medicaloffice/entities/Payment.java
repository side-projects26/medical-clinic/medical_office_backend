package com.anbara.medicaloffice.entities;

import com.anbara.medicaloffice.enums.PaymentStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Payment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal total;
    private BigDecimal amountPaid;
    private PaymentStatus status;

    @OneToOne(mappedBy = "payment")
    private Consultation consultation;

}
