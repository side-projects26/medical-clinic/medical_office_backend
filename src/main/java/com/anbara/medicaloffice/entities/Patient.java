package com.anbara.medicaloffice.entities;

import com.anbara.medicaloffice.enums.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE;
import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE_TIME;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Patient implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 75)
    private String firstName;
    @Column(length = 75)
    private String lastName;

    @Column(length = 75)
    private String phone;
    @Column(length = 75)
    private String address;
    @Column(length = 75)
    private String profession;
    @Embedded
    private  MedicalHistory medicalHistory;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = PATTERN_DATE)
    @DateTimeFormat(pattern =PATTERN_DATE)
    private LocalDate dateOfBirth;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = PATTERN_DATE_TIME)
    @DateTimeFormat(pattern = PATTERN_DATE_TIME)
    private LocalDateTime dateCreation;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private Gender gender;

    @OneToMany(mappedBy = "patient", fetch = FetchType.EAGER) // recommended value LAZY
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Collection<PatientAppointment> appointments;

    @OneToMany(mappedBy = "patient", cascade = CascadeType.REMOVE
            , fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Collection<Consultation> consultations;

   /* {
        dateCreation = LocalDateTime.now();
    }*/



}


