package com.anbara.medicaloffice.entities;

import com.anbara.medicaloffice.utils.AppConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class MedicalHistory {
    //@Column(length = AppConstants.MAX_LENGTH_ADDRESS_OFFICE)
    private String allergies;
    private String medications;
    private String oldDiseases;
    private String currentDiseases;
}

