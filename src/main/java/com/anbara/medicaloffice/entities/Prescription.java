package com.anbara.medicaloffice.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Map;

import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE;

@Entity
@NoArgsConstructor
//@AllArgsConstructor
@Data
public class Prescription implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = PATTERN_DATE)
    @DateTimeFormat(pattern = PATTERN_DATE)
    private LocalDate date;


    @ElementCollection(fetch = FetchType.EAGER)  // best is lazy
    @MapKeyColumn(length = 64)
    private Map<String, String> medicamentMap; // <name,details> ;

    public Prescription(LocalDate date, Map<String, String> medicamentMap) {
        this.date = date;
        this.medicamentMap = medicamentMap;
    }
}
