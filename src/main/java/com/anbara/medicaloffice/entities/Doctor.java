package com.anbara.medicaloffice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import static com.anbara.medicaloffice.utils.AppConstants.MAX_LENGTH_DOCTOR_FULL_NAME;
import static com.anbara.medicaloffice.utils.AppConstants.MAX_LENGTH_SPECIALTY_DOCTOR;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Doctor {
    @Column(length = MAX_LENGTH_DOCTOR_FULL_NAME)
    private String name;
    @Column(length = MAX_LENGTH_SPECIALTY_DOCTOR)
    private String specialty;
}
