package com.anbara.medicaloffice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static com.anbara.medicaloffice.utils.AppConstants.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class MedicalOfficeInfos {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(length = MAX_LENGTH_MEDICAL_OFFICE_NAME)
    private String medicalOfficeName;

    @Embedded
    private Doctor doctor;

    @Column(length = MAX_LENGTH_PHONE)
    private String phone;

    @Lob
    private String logoBase64;
    @Embedded
    private Location location;

}
