package com.anbara.medicaloffice.entities;

import com.anbara.medicaloffice.utils.AppConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

 @Embeddable
@NoArgsConstructor
 @AllArgsConstructor
@Data
 @Builder
public class Location {
    @Column(length = AppConstants.MAX_LENGTH_ADDRESS_OFFICE)
    private String  address;
    private double longitude;
    private double latitude ;
    private double zoomLevel;
}
