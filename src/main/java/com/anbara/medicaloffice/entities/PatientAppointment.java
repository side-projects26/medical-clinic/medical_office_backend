package com.anbara.medicaloffice.entities;

import com.anbara.medicaloffice.enums.AppointmentStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE;
import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_TIME;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class PatientAppointment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = PATTERN_DATE)
    @DateTimeFormat(pattern = PATTERN_DATE)
    private LocalDate date;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = PATTERN_TIME)
    @DateTimeFormat(pattern = PATTERN_TIME)
    private LocalTime time;

    @ManyToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Patient patient;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private AppointmentStatus status;
}
