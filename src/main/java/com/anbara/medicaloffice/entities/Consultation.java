package com.anbara.medicaloffice.entities;

import com.anbara.medicaloffice.utils.Helpers;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.anbara.medicaloffice.utils.AppConstants.PATTERN_DATE;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Consultation implements Serializable {
    //todo chaq consultation a un examLabo et prescription
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = PATTERN_DATE)
    @DateTimeFormat(pattern = PATTERN_DATE)
    private LocalDate date;

    private String reason;

    @ManyToOne
    @JsonIgnore
    private Patient patient;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "id_payment",referencedColumnName = "id")
    private Payment payment;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_prescription",referencedColumnName = "id")
    // @Embedded
    private Prescription prescription;


//@Embedded
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_certificate")
    private Certificate certificate;

    {
        this.date = LocalDate.parse(Helpers.getTodayDate(), DateTimeFormatter.ofPattern(PATTERN_DATE));

    }

}
