package com.anbara.medicaloffice.controllers;

import com.anbara.medicaloffice.entities.Patient;
import com.anbara.medicaloffice.enums.Gender;
import com.anbara.medicaloffice.services.PatientService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class PatientControllerV2Test {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    PatientService patientService;

    @Test
    public void getPatientTest() throws Exception {
        Patient fakePatient = Patient
                .builder()
                .firstName("ayoub")
                .lastName("anbara")
                .phone("1234")
                .dateOfBirth(LocalDate.of(1996, 12, 9))
                .gender(Gender.Male)
                .build();

      //  given(patientService.one(1L)).willReturn(fakePatient);

        mockMvc.perform(
                get("/patients/{id}",1L)
        ).andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(fakePatient.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(fakePatient.getLastName()))
                .andExpect(jsonPath("$.phone").value(fakePatient.getPhone()))
                .andExpect(jsonPath("$.dateOfBirth").value(fakePatient.getDateOfBirth().toString()))
                .andExpect(jsonPath("$.gender").value(fakePatient.getGender().toString()));



    }

}
