package com.anbara.medicaloffice.controllers;

import com.anbara.medicaloffice.entities.Patient;
import com.anbara.medicaloffice.enums.Gender;
import com.anbara.medicaloffice.services.PatientService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class PatientControllerTest {

    MockMvc mockMvc;

    PatientController patController;

    @InjectMocks
    PatientService patService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        patController = new PatientController(patService);
       // patService
                mockMvc = MockMvcBuilders.standaloneSetup(patController).build();
    }


    @Test
    public void when_call_one() throws Exception {
        Patient patient =
                Patient.builder()
                .firstName("first")
                        .lastName("last")
                        .phone("1234")
                        .dateOfBirth( LocalDate.of(1996, 12, 9))
                        .gender(Gender.Male)
                        .build() ;
       // Mockito.when(patService.one(1L)).thenReturn(patient);
        mockMvc.perform(
                MockMvcRequestBuilders.get("/patients/1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is(200));
    }

}
