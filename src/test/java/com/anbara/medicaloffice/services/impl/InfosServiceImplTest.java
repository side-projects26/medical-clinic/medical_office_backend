package com.anbara.medicaloffice.services.impl;

import com.anbara.medicaloffice.dao.InfosMedOfficeRepo;
import com.anbara.medicaloffice.dto.MedicalOfficeInfosDto;
import com.anbara.medicaloffice.entities.MedicalOfficeInfos;
import com.anbara.medicaloffice.entities.Location;
import com.anbara.medicaloffice.services.impl.InfosServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InfosServiceImplTest {

    @Autowired
    InfosServiceImpl infosServiceimpl;
    @MockBean
    InfosMedOfficeRepo repository;

    @Test
    public void when_call_getInfos(){
        /*MedicalOfficeInfos expectedInfos=new MedicalOfficeInfos(1L,"medical_name","doctor_name"
                ,"speciality_XY","1234","",
                new Location("address_XY",23.9,33.8,44.8)
        );
        List<MedicalOfficeInfos> list = new ArrayList<>();
        list.add(expectedInfos);
        given(repository.findAll()).willReturn(list);
        // Mockito.when(infosMedOfficeRepo.findAll().get(0)).thenReturn(expectedInfos);

        MedicalOfficeInfosDto targetInfos= infosServiceimpl.getInfos();
        assertEquals(expectedInfos.getId(),targetInfos.getId());
        assertEquals(expectedInfos.getPhone(),targetInfos.getPhone());
        assertEquals(expectedInfos.getDoctorFullName(),targetInfos.getDoctorFullName());
        assertEquals(expectedInfos.getMedicalOfficeName(),targetInfos.getMedicalOfficeName());
        assertEquals(expectedInfos.getSpecialtyDoctor(),targetInfos.getSpecialtyDoctor());
        assertEquals(expectedInfos.getLogoBase64(),targetInfos.getLogoBase64());

        assertEquals(expectedInfos.getLocation().getAddress(),targetInfos.getLocationDto().getAddress());
        assertEquals(expectedInfos.getLocation().getLatitude(),targetInfos.getLocationDto().getLatitude());
        assertEquals(expectedInfos.getLocation().getLongitude(),targetInfos.getLocationDto().getLongitude());
        assertEquals(expectedInfos.getLocation().getLatitude(),targetInfos.getLocationDto().getLatitude());
*/
        //  verify(infosMedOfficeRepo, times(1)).save(sender);


    }

    @Test
    public void when_call_updateInfos(){
       /* MedicalOfficeInfos oldInfos=new MedicalOfficeInfos(1L,"medical_name","doctor_name"
                ,"speciality_XY","1234","",
                new LocationOffice("address_XY",10,20,30)
        );

        MedicalOfficeInfosDto newInfos=new MedicalOfficeInfosDto(1L,"new_medical_name","new_doctor_name"
                ,"new_speciality_XY","new_1234","",
                new LocationOffice("new_address_XY",40,50,60)
        );

        given(repository.findById(1L)).willReturn(Optional.of(oldInfos));
        given(repository.save(newInfos)).willReturn(newInfos);

        MedicalOfficeInfos infosReturned= infosServiceimpl.updateInfos(newInfos,1L);

        assertEquals(newInfos.getId(),infosReturned.getId());
        assertEquals(newInfos.getPhone(),infosReturned.getPhone());
        assertEquals(newInfos.getDoctorFullName(),infosReturned.getDoctorFullName());
        assertEquals(newInfos.getMedicalOfficeName(),infosReturned.getMedicalOfficeName());
        assertEquals(newInfos.getSpecialtyDoctor(),infosReturned.getSpecialtyDoctor());
        assertEquals(newInfos.getLogoBase64(),infosReturned.getLogoBase64());

        assertEquals(newInfos.getLocationOffice().getAddress(),infosReturned.getLocationOffice().getAddress());
        assertEquals(newInfos.getLocationOffice().getLatitude(),infosReturned.getLocationOffice().getLatitude());
        assertEquals(newInfos.getLocationOffice().getLongitude(),infosReturned.getLocationOffice().getLongitude());
        assertEquals(newInfos.getLocationOffice().getLatitude(),infosReturned.getLocationOffice().getLatitude());

*/
    }
}
