package com.anbara.medicaloffice.utils;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDate;

import static com.anbara.medicaloffice.utils.Helpers.calculatePercentage;
import static com.anbara.medicaloffice.utils.Helpers.formatDate;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
 class HelpersTest {


    @Test
     void when_call_calcul_percentage(){
        BigDecimal per= calculatePercentage(200,25);
        BigDecimal expected_per=new BigDecimal(12.5);
        //assertTrue(per.(expected_per));
        assertEquals(0,per.compareTo(expected_per));

    }
    @Test
     void when_call_calcul_percentage_with_total_equal0(){
        BigDecimal per= calculatePercentage(0,7);
        BigDecimal expected_per=new BigDecimal(0);
        assertEquals(0,per.compareTo(expected_per));
    }

    @Test
     void when_call_formatDate(){
        String date= formatDate(LocalDate.of(2023,7,2));

        assertEquals("2023-07-02",date); // yyyy-MM-dd
    }

}





