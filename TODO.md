##Le reste a faire pour l'API Medical Office
**️✔** Done  
**x** Todo
###General:
---
   show error and messages to user- displaying loading until getting data
---
* https://github.com/search?q=spring+boot+best+practices
* logger file **x**
* voir le cahier de charge pfe
* apply recomandation youssfi ngnx
* use CI/CD to automatise le deployment **x**
* trim input   npm i ng2-trim-directive**x**
* change date format to dd/MM/yyyy **v**
* complete all todo in code and Logger **x**
* use ResponseEntity but we shouldn't overuse it**x**
* adding prescriptions  **x**
* complete medicaments **x**
* complete creation interfaces controller **x**
* UserModelRest **x**
* cacher les erreurs system ==> hackers **x**
* update libraries **x**
* use toDate sql function to format date in sql request statistics, because mysql 
used format yyyy-MM-dd **x**
### Traduction
* Traduction les erreurs **x**
* Traduction l'application front end **x**

#### Controleur Patient
* add allergies les médicaments et les anciennes/current maladies: https://www.youtube.com/watch?v=0t5f1e8O8Zg&t=109s&ab_channel=aminawahmane **x**
* supprission d'un patient ==> delete her consultations ==> delete certaficate ...

#### Controleur Payment
* list payment **️✔**
* historique payment pour chaq patient (credits) **️✔**

#### Controleur DatabaseManagement
* verification account admin when restore database **x**
    
#### User
* change password without change other user information **x**
* admin can edit roles from IHM **x**
* refresh token

#### Validators
* Finaliser les validateurs **x**


#### Les methodes de suppression
* Ajouter les verification pour chaque methode de suppression de l'API **x**

### Tests
* complete Unit tests **x**
* functional tests  **x**
* Integration tests **x**

### Docker
* Dockerizing l'app  **x**
* search intranet for deployment  **x**

### Sources:
https://www.youtube.com/watch?v=01sVw8BiFC0&list=LL&index=4&t=10456s&ab_channel=MohamedIDBRAHIM

### Sponsors:
* change documentation **x**
* create page facebook, channel youtube   **x**
* ads => facebook, adwords   **x**
